// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class UnboundEditorTarget : TargetRules
{
	public UnboundEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		ExtraModuleNames.Add("Unbound");
		ExtraModuleNames.Add("UnboundAbilities");
        ExtraModuleNames.Add("UnboundDungeons");
        ExtraModuleNames.Add("UnboundUI");
        ExtraModuleNames.Add("UnboundDialogue");
        ExtraModuleNames.Add("UnboundInteraction");

        ExtraModuleNames.Add("UnboundDialogueEditor");
    }
}
