// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "UnboundDungeons.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_GAME_MODULE( FDefaultGameModuleImpl, UnboundDungeons);

DEFINE_LOG_CATEGORY(LogUnboundDungeons);