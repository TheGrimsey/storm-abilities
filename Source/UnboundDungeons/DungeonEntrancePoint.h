// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "GameplayTagContainer.h"

#include "Components/SceneComponent.h"

#include "DungeonEntrancePoint.generated.h"


UCLASS( ClassGroup=(Dungeons), meta=(BlueprintSpawnableComponent) )
class UNBOUNDDUNGEONS_API UDungeonEntrancePoint : public USceneComponent
{
	GENERATED_BODY()

	/*
	*	Variables
	*/
public:
	UPROPERTY(AssetRegistrySearchable, EditAnywhere)
	FGameplayTag EntrancePointSize;

	UPROPERTY(VisibleAnywhere)
	uint16 Id;

	UPROPERTY(VisibleAnywhere)
	FTransform TransformRelativeToRoot;

#if WITH_EDITORONLY_DATA
	UPROPERTY(VisibleAnywhere)
	class UBillboardComponent* BillboardComponent;
#endif //WITH_EDITORONLY_DATA

	/*
	*	Methods
	*/
public:	
	// Sets default values for this component's properties
	UDungeonEntrancePoint();
};
