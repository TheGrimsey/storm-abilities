// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameplayTagContainer.h"
#include "DungeonRoom.generated.h"

UCLASS()
class UNBOUNDDUNGEONS_API ADungeonRoom : public AActor
{
	GENERATED_BODY()

	/*
	*	Variables
	*/
public:
	//The theme of this room.
	UPROPERTY(AssetRegistrySearchable, EditDefaultsOnly)
	FGameplayTag RoomTheme;

	//Tags that this room has.
	UPROPERTY(AssetRegistrySearchable, EditDefaultsOnly)
	FGameplayTagContainer RoomTags;

	//Tags that a room attempting to connect to this room must have.
	UPROPERTY(AssetRegistrySearchable, EditDefaultsOnly)
	FGameplayTagContainer ConnectionRequiredTags;

	//Tags that a room attempting to connect to this room must NOT have.
	UPROPERTY(AssetRegistrySearchable, EditDefaultsOnly)
	FGameplayTagContainer ConnectionDisallowedTags;

	UPROPERTY(VisibleAnywhere)
	FVector Center;

	UPROPERTY(VisibleAnywhere)
	FVector Bounds;

	UPROPERTY(AssetRegistrySearchable, VisibleAnywhere)
	uint16 EntrancePointCount;

	/*
	*	Methods
	*/
public:	
	// Sets default values for this actor's properties
	ADungeonRoom();

	virtual void BeginPlay() override;

	UFUNCTION()
	class UDungeonEntrancePoint* GetRandomEntrancePoint() const;

	UFUNCTION()
	TArray<class UDungeonEntrancePoint*> GetAllEntrancePoints() const;

    //Returns true if the rooms have no tags blocking each other from connecting.
    UFUNCTION()
    bool IsCompatibleWithRoom(class ADungeonRoom* OtherRoom);

#if WITH_EDITORONLY_DATA
    virtual void PreSave(const class ITargetPlatform* TargetPlatform) override;
#endif
};
