// Fill out your copyright notice in the Description page of Project Settings.


#include "DungeonEntrancePoint.h"

#if WITH_EDITORONLY_DATA
#include "UObject/ConstructorHelpers.h" 
#include "Components/BillboardComponent.h"
#endif

UDungeonEntrancePoint::UDungeonEntrancePoint()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

#if WITH_EDITORONLY_DATA
	BillboardComponent = CreateEditorOnlyDefaultSubobject<UBillboardComponent>(TEXT("Billboard"));
	if (BillboardComponent)
	{
		BillboardComponent->SetupAttachment(this);
		BillboardComponent->bIsScreenSizeScaled = true;

		ConstructorHelpers::FObjectFinderOptional<UTexture2D> MarkerTextureObject(TEXT("/Game/Editor/DungeonMarkers/EntrancePointMarker"));

		BillboardComponent->Sprite = MarkerTextureObject.Get();
	}
#endif
}
