// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class UnboundDungeons : ModuleRules
{
	public UnboundDungeons(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "GameplayTags" });

        if(Target.bBuildEditor)
        {
            PrivateDependencyModuleNames.Add("UnrealEd");
        }

    }
}
