// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameplayTagContainer.h"
#include "DungeonGate.generated.h"

UCLASS()
class UNBOUNDDUNGEONS_API ADungeonGate : public AActor
{
	GENERATED_BODY()
	
	/*
	*	Variables
	*/
protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class UDungeonEntrancePoint* EntrancePoint;

	//Dungeon rooms this gate has spawned.
	UPROPERTY()
	TArray<class ADungeonRoom*> SpawnedDungeonRooms;

	/*
	*	Dungeon Generating.
	*/

	//Dungeons rooms this gate can spawn.
	UPROPERTY(EditAnywhere)
	TArray<TSubclassOf<class ADungeonRoom>> SpawnableDungeonRooms;

	//How many rounds are we going to fill in entrance points before stopping?
	UPROPERTY(EditAnywhere)
	int SpawnIterations = 5;

	//How many times are we going to attempt to spawn a room before giving up?
	UPROPERTY(EditAnywhere)
	int MaxRoomSpawnAttempts = 5;

	UPROPERTY()
	TArray<class UDungeonEntrancePoint*> UnconnectedEntrancePoints;

	/*
	*	Methods
	*/
public:	
	// Sets default values for this actor's properties
	ADungeonGate();

	void BeginPlay() override;

	void EndPlay(EEndPlayReason::Type EndPlayReason) override;

	void GenerateDungeon();

protected:
    //Spawn a Dungeon room connected to SourceEntrancePoint with at maxmimum MaxEntrancePoints
	class ADungeonRoom* SpawnDungeonRoom(class UDungeonEntrancePoint* SourceEntrancePoint, uint16 MaxEntrancePoints = 0);

    //Calculate the transform for to connect the two entrancepoints.
    static void CalculateDungeonTransform(FTransform& TransformOut, class UDungeonEntrancePoint* SourceEntrancePoint, class UDungeonEntrancePoint* NewEntrancePoint);

	TSubclassOf<ADungeonRoom> FindDungeonToSpawn(class ADungeonRoom* SourceDungeonRoom = nullptr, uint16 MaxEntrancePoints = 0);

	class UDungeonEntrancePoint* GetRandomEntrancePointWithSize(const TSubclassOf<class ADungeonRoom>& DungeonRoomClass, const FGameplayTag& SizeTag) const;
};
