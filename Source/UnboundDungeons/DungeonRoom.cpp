// Fill out your copyright notice in the Description page of Project Settings.


#include "DungeonRoom.h"
#include "DungeonEntrancePoint.h"

#if WITH_EDITOR
#include "Engine/BlueprintGeneratedClass.h" 
#include "Engine/SimpleConstructionScript.h"
#include "Engine/SCS_Node.h" 

#include "Editor/UnrealEd/Public/Editor.h" 
#endif //WTIH_EDITOR

// Sets default values
ADungeonRoom::ADungeonRoom()
{
 	// Set this actor to NOT call Tick() every frame.
	PrimaryActorTick.bCanEverTick = false;

	bReplicates = true;
    SetReplicateMovement(false);
}

void ADungeonRoom::BeginPlay()
{
    Super::BeginPlay();

    SetNetDormancy(ENetDormancy::DORM_DormantAll);
}

UDungeonEntrancePoint* ADungeonRoom::GetRandomEntrancePoint() const
{
	TArray<UDungeonEntrancePoint*> EntrancePoints = GetAllEntrancePoints();

	if (EntrancePoints.Num() > 0)
	{
		int RandomIndex = FMath::RandRange(0, EntrancePoints.Num() - 1);

		return Cast<UDungeonEntrancePoint>(EntrancePoints[RandomIndex]);
	}

	return nullptr;
}

TArray<UDungeonEntrancePoint*> ADungeonRoom::GetAllEntrancePoints() const
{
	TArray<UDungeonEntrancePoint*> Result;

	GetComponents<UDungeonEntrancePoint>(Result, false);

	return Result;
}

bool ADungeonRoom::IsCompatibleWithRoom(ADungeonRoom* OtherRoom)
{
    /*
    *   Check so OtherRoom has all required tags and none of the disallowed tags.
    *   Then check so we have all of OtherRoom's required tags and none of OtherRoom's disallowed tags.
    */
    return OtherRoom->RoomTags.HasAll(ConnectionRequiredTags) && !OtherRoom->RoomTags.HasAny(ConnectionDisallowedTags) 
        && RoomTags.HasAll(OtherRoom->ConnectionRequiredTags) && !RoomTags.HasAny(OtherRoom->ConnectionDisallowedTags);
}

#if WITH_EDITORONLY_DATA
void ADungeonRoom::PreSave(const ITargetPlatform* TargetPlatform)
{
	Super::PreSave(TargetPlatform);

    /*
    *   Assign DungeonEntrancePoint Ids.
    */
    TArray<UDungeonEntrancePoint*> CDODungeonEntracePoints;

    UBlueprintGeneratedClass* ActorBlueprintGeneratedClass = Cast<UBlueprintGeneratedClass>(GetClass());
    if (ActorBlueprintGeneratedClass)
    {
        const TArray<const USCS_Node*>& ActorBlueprintNodes = ActorBlueprintGeneratedClass->SimpleConstructionScript->GetAllNodesConst();

        uint16 IdCount = 0;

        for (const USCS_Node* Node : ActorBlueprintNodes)
        {
            if (Node->ComponentClass->IsChildOf(UDungeonEntrancePoint::StaticClass()))
            {
                UDungeonEntrancePoint* DungeonEntrancePoint = Cast<UDungeonEntrancePoint>(Node->GetActualComponentTemplate(ActorBlueprintGeneratedClass));
                DungeonEntrancePoint->Id = IdCount++;

                CDODungeonEntracePoints.Add(DungeonEntrancePoint);
            }
        }

        EntrancePointCount = IdCount;
    }

    /*
    *   Create TempActor
    */
    UWorld* World = GEditor->GetEditorWorldContext().World();

    ADungeonRoom* TempActor = World->SpawnActor<ADungeonRoom>(GetClass(), FTransform());

    /*
    * Semi-hacky system to cache actor bounds.
    */
    FVector Origin;
    FVector Extents;

    TempActor->GetActorBounds(true, Origin, Extents);

	Center = Origin;
    Bounds = Extents;

    /*
    *   Assign TransformRelativeToRoot for DungeonEntrancePoints.
    */
    for (UDungeonEntrancePoint* CDODungeonEntrancePoint : CDODungeonEntracePoints)
    {
        for (UDungeonEntrancePoint* TempActorDungeonEntrancePoint : TempActor->GetAllEntrancePoints())
        {
            if (CDODungeonEntrancePoint->Id == TempActorDungeonEntrancePoint->Id)
            {
                CDODungeonEntrancePoint->TransformRelativeToRoot = TempActorDungeonEntrancePoint->GetComponentTransform().GetRelativeTransform(TempActor->GetTransform());
                break;
            }
        }
    }

    TempActor->Destroy();
}
#endif //WITH_EDITORONLY_DATA