// Fill out your copyright notice in the Description page of Project Settings.


#include "DungeonGate.h"
#include "DungeonRoom.h"
#include "DungeonEntrancePoint.h"
#include "UnboundDungeons.h"

#include "Engine/World.h"
#include "Engine/BlueprintGeneratedClass.h" 
#include "Engine/SCS_Node.h"
#include "DrawDebugHelpers.h"

// Sets default values
ADungeonGate::ADungeonGate()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	EntrancePoint = CreateDefaultSubobject<UDungeonEntrancePoint>(TEXT("DungeonEntrancePoint"));
	
	SetRootComponent(EntrancePoint);
}

// Called when the game starts or when spawned
void ADungeonGate::BeginPlay()
{
	Super::BeginPlay();

	SetNetDormancy(ENetDormancy::DORM_DormantAll);

	//Only generate dungeons on authority.
	if (HasAuthority())
	{
		UE_LOG(LogUnboundDungeons, Log, TEXT("Starting Dungeon Generation from %s"), *GetName());
		GenerateDungeon();
		UE_LOG(LogUnboundDungeons, Log, TEXT("Dungeon Generation finished from %s"), *GetName());
	}
}

void ADungeonGate::EndPlay(EEndPlayReason::Type EndPlayReason)
{
	if (HasAuthority())
	{
		//Destroy all the dungeon rooms spawned by this gate.
		for (ADungeonRoom* SpawnedDungeonRoom : SpawnedDungeonRooms)
		{
			SpawnedDungeonRoom->Destroy(true, true);
		}
		SpawnedDungeonRooms.Empty();
	}
}


void ADungeonGate::GenerateDungeon()
{
	UnconnectedEntrancePoints.Add(EntrancePoint);

	/*
	*	Spawn inital rooms.
	*/
	for (int i = 0; i < SpawnIterations; i++)
	{
		//Grab all UnconnectedEntrencePoints.
		TArray<UDungeonEntrancePoint*> PendingEntrancePoints = UnconnectedEntrancePoints;

		//Try to spawn a dungeon room from each pendingentrancepoint.
		for (UDungeonEntrancePoint* UnconnectedEntrancePoint : PendingEntrancePoints)
		{
			SpawnDungeonRoom(UnconnectedEntrancePoint);
		}
	}

	/*
	*	Fill in the last empty entrancepoints with rooms with only 1 entrance.
	*/
	{
		TArray<UDungeonEntrancePoint*> PendingEntrancePoints = UnconnectedEntrancePoints;

		for (UDungeonEntrancePoint* UnconnectedEntrancePoint : PendingEntrancePoints)
		{
			SpawnDungeonRoom(UnconnectedEntrancePoint, 1);
		}
	}
}

ADungeonRoom* ADungeonGate::SpawnDungeonRoom(UDungeonEntrancePoint* SourceEntrancePoint, uint16 MaxEntrancePoints)
{
    ADungeonRoom* SourceDungeonRoom = Cast<ADungeonRoom>(SourceEntrancePoint->GetOwner());

	for (int i = 0; i < MaxRoomSpawnAttempts; i++)
	{
		/*
		*	Select a dungeonroom to spawn from our options.
		*/
		TSubclassOf<ADungeonRoom> DungeonToSpawn = FindDungeonToSpawn(SourceDungeonRoom, MaxEntrancePoints);

		//If we can't find any dungeon to spawn then let's just return nullptr.
		if (DungeonToSpawn == nullptr)
		{
			return nullptr;
		}

		/*
		*	Select a random entrance point on the DungeonRoom that we will attach to. We take it here before the template so we don't need to spawn the template unnecessarily.
		*/
		UDungeonEntrancePoint* TemplateEntrancePoint = GetRandomEntrancePointWithSize(DungeonToSpawn, SourceEntrancePoint->EntrancePointSize);

		//Make sure we have an entrance point else let's skip this round.
		if (TemplateEntrancePoint == nullptr)
		{
			continue;
		}

		/*
		*	Make transform.
		*/
		//Calulate rotation by taking the opposite of the Source forward and removing our entrance's relative rotation.
		FRotator NewRotation = (-SourceEntrancePoint->GetForwardVector()).ToOrientationRotator() - TemplateEntrancePoint->GetRelativeRotation();
		//Calculate location by taking the SourceEntrancePoint's location and removing our entrance's relative location rotated to the new rotation.
		FVector NewLocation = SourceEntrancePoint->GetComponentLocation() - NewRotation.RotateVector(TemplateEntrancePoint->GetRelativeLocation());
        FTransform TestTransform = FTransform(NewRotation, NewLocation);

		FTransform TargetTransform = FTransform(); 
        CalculateDungeonTransform(TargetTransform, SourceEntrancePoint, TemplateEntrancePoint);
		
		/*
		*	COLLISION
		*/
		//Get the bounds of the DungeonRoom from the cached value.
		FVector Center = NewRotation.RotateVector(DungeonToSpawn->GetDefaultObject<ADungeonRoom>()->Center);
        FVector Bounds = DungeonToSpawn->GetDefaultObject<ADungeonRoom>()->Bounds;
		
		//Check if the room fits in the target location with the rotation.
		if (GetWorld()->OverlapBlockingTestByProfile(NewLocation + Center, NewRotation.Quaternion(), FName(TEXT("BlockAll")), FCollisionShape::MakeBox(Bounds * 0.95f)))
		{
#if ENABLE_DRAW_DEBUG
			DrawDebugBox(GetWorld(), NewLocation + Center, Bounds, NewRotation.Quaternion(), FColor::Red, false, 30.f, 0, 10.f);
#endif

			//If we collide we cant spawn this one.
			continue;
		}
#if ENABLE_DRAW_DEBUG
		else
		{
			DrawDebugBox(GetWorld(), NewLocation + Center, Bounds, NewRotation.Quaternion(), FColor::Blue, false, 30.f, 0, 10.f);
		}
#endif

		/*
		*	Set up SpawnParameters.
		*/
		FActorSpawnParameters SpawnParams = FActorSpawnParameters();
		SpawnParams.Owner = this;
		SpawnParams.Instigator = nullptr;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		/*
		*	Spawn new Dungeon Room.
		*/
        ADungeonRoom* SpawnedRoom = SpawnedRoom = GetWorld()->SpawnActor<ADungeonRoom>(DungeonToSpawn, TargetTransform, SpawnParams);

		if (SpawnedRoom)
		{
			//We built from the SourceEntrancePoint so we remove it from the list of unbuilt ones.
			UnconnectedEntrancePoints.Remove(SourceEntrancePoint);

			//Add the room we made to the list of rooms owned by this gate.
			SpawnedDungeonRooms.Add(SpawnedRoom);

			/*
			*	Loop through all entrance points in the room we created and add all except the one we connected to into the UnbuiltEntrancePoints list.
			*/
			for (UDungeonEntrancePoint* SpawnedEntrancePoint : SpawnedRoom->GetAllEntrancePoints())
			{
				//Check so the Ids dont match. If they dont then lets add it to the list of unbuiltentrances.
				if (SpawnedEntrancePoint->Id != TemplateEntrancePoint->Id)
				{
					UnconnectedEntrancePoints.Add(SpawnedEntrancePoint);
				}
				else
				{
					/*
					*	Log the Point we attached to and the point we created's paths for debug.
					*/
					UE_LOG(LogUnboundDungeons, Log, TEXT("Attached %s to %s"), *SpawnedEntrancePoint->GetPathName(), *SourceEntrancePoint->GetPathName());

				}
			}

			//Attach the room to this gate so we know who it belongs to and we can easily move it if needed.
			SpawnedRoom->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);

            return SpawnedRoom;
		}
	}

	return nullptr;
}

void ADungeonGate::CalculateDungeonTransform(FTransform& TransformOut, class UDungeonEntrancePoint* SourceEntrancePoint, class UDungeonEntrancePoint* NewEntrancePoint)
{
    //Calulate rotation by taking the opposite of the Source forward and removing our entrance's relative rotation.
    FRotator NewRotation = (-SourceEntrancePoint->GetForwardVector()).ToOrientationRotator() - NewEntrancePoint->TransformRelativeToRoot.GetRotation().Rotator();
    //Calculate location by taking the SourceEntrancePoint's location and removing our entrance's relative location rotated to the new rotation.
    FVector NewLocation = SourceEntrancePoint->GetComponentLocation() - NewRotation.RotateVector(NewEntrancePoint->TransformRelativeToRoot.GetLocation());

    TransformOut = FTransform(NewRotation, NewLocation);
}

TSubclassOf<ADungeonRoom> ADungeonGate::FindDungeonToSpawn(ADungeonRoom* SourceDungeonRoom, uint16 MaxEntrancePoints)
{
	TArray<TSubclassOf<ADungeonRoom>> ValidRooms;

	for (TSubclassOf<ADungeonRoom> DungeonRoom : SpawnableDungeonRooms)
	{
		ADungeonRoom* DungeonRoomCDO = DungeonRoom->GetDefaultObject<ADungeonRoom>();
		/*
		*   Check so we have an acceptable amount of entrancepoints & if the SourceRoom isn't nullptr that the rooms are compatible with each other.
		*/
		if ((MaxEntrancePoints != 0 ? DungeonRoomCDO->EntrancePointCount <= MaxEntrancePoints : true ) && (SourceDungeonRoom ? SourceDungeonRoom->IsCompatibleWithRoom(DungeonRoomCDO) : true))
		{
			ValidRooms.Add(DungeonRoom);
		}
	}

	if (ValidRooms.Num() > 0)
	{
		//Pick a randum index in the list.
		uint32 DungeonToSpawnIndex = FMath::RandRange(0, ValidRooms.Num() - 1);

		//Return the room at that random index.
		return ValidRooms[DungeonToSpawnIndex];
	}

	return nullptr;
}

UDungeonEntrancePoint* ADungeonGate::GetRandomEntrancePointWithSize(const TSubclassOf<ADungeonRoom>& DungeonRoomClass,const FGameplayTag& SizeTag) const
{
	TArray<UDungeonEntrancePoint*> DungeonEntrances;

	DungeonEntrances = DungeonRoomClass->GetDefaultObject<ADungeonRoom>()->GetAllEntrancePoints();

	UBlueprintGeneratedClass* ActorBlueprintGeneratedClass = Cast<UBlueprintGeneratedClass>(DungeonRoomClass);
	if (ActorBlueprintGeneratedClass)
	{
		const TArray<const USCS_Node*>& ActorBlueprintNodes = ActorBlueprintGeneratedClass->SimpleConstructionScript->GetAllNodesConst();

		for (const USCS_Node* Node : ActorBlueprintNodes)
		{
			if (Node->ComponentClass->IsChildOf(UDungeonEntrancePoint::StaticClass()))
			{
				UDungeonEntrancePoint* DungeonEntrancePoint = Cast<UDungeonEntrancePoint>(Node->GetActualComponentTemplate(ActorBlueprintGeneratedClass));

				/*
				*	Check so the size tags match.
				*/
				if (SizeTag.IsValid() && DungeonEntrancePoint->EntrancePointSize.IsValid() && SizeTag == DungeonEntrancePoint->EntrancePointSize)
				{
					DungeonEntrances.Add(DungeonEntrancePoint);
				}
			}
		}
	}

	if (DungeonEntrances.Num() == 0)
	{
		return nullptr;
	}

	int RandomizedEntranceIndex = FMath::RandRange(0, DungeonEntrances.Num() - 1);
	return DungeonEntrances[RandomizedEntranceIndex];
}
