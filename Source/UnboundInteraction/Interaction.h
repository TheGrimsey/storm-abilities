// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "InteractionTypes.h"
#include "Engine/EngineTypes.h"

#include "Interaction.generated.h"

/**
 *	An that requires action by the player beyond just one button press.
 *	For example: Dialogue, Trading, etc
 *
 *	Interactions have a Speaker (an actor we are interacting with) and are held in an InteractionComponent.
 *
 *	Each interaction needs it's own start function (See UDialogueInteraction::StartDialogue for an example of this) which needs to call Init() and take in a speaker & a UInteractionComponent.
 *	They also need an EInteractionType.
 *
 *	All the data needed for an interaction to work is put in through it's start function.
 */
UCLASS(Abstract, BlueprintType)
class UNBOUNDINTERACTION_API UInteraction : public UObject
{
	friend class UInteractionComponent;

	GENERATED_BODY()

	/*
	*	Variables
	*/
protected:
	//The type of this interaction. 
	UPROPERTY(VisibleAnywhere)
	EInteractionType InteractionType;

public:
	//The InteractionComponent that owns this interaction.
	UPROPERTY(VisibleAnywhere, Replicated)
	class UInteractionComponent* InteractionComponent;

	//The Actor we are interacting with.
	UPROPERTY(BlueprintReadOnly, Replicated)
	class AActor* Speaker;

	/*s
	*	Methods
	*/
public:
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	virtual bool IsSupportedForNetworking() const final { return true; }

	virtual bool CallRemoteFunction(UFunction* Function, void* Parms, struct FOutParmRec* OutParms, FFrame* Stack);

	virtual int32 GetFunctionCallspace(UFunction* Function, FFrame* Stack);

	virtual void PreDestroyFromReplication() override;
	
	/*
	*	Returns this Interaction's type.
	*/
	UFUNCTION(BlueprintPure)
	FORCEINLINE EInteractionType GetInteractionType() const { return InteractionType; }

	/*
	*	OnStartInteraction. Called when the interaction has been added to the interactioncomponent
	*	Called on both server & client.
	*/
	UFUNCTION()
	virtual void OnStartInteraction();

	/*
	*	OnStopInteraction. Called when the interaction is being removed from the interactioncomponent / when it has ended.
	*	Called on both server & client.
	*/
	UFUNCTION()
	virtual void OnStopInteraction();
	
protected:
	/*
	*	Initializes the Interaction and adds it to the InteractionComponent. Needs to be called by all Interactions after they are created.
	*	@Param InInteractionComponent InteractionComponent to register this interaction to.
	*	@Param InSpeaker Actor we are interacting with.
	*/
	void Init(class UInteractionComponent* InInteractionComponent, class AActor* InSpeaker);

	/*
	*	Returns our owning actor (the owner of the interaction component)
	*/
	class AActor* GetOwningActor();

	/*
	*	Returns the actor we are interacting with.
	*/
	class AActor* GetSpeaker() { return Speaker; }

	/*
	*	Returns the owning player controller.
	*	Simply refers to UInteractionComponent::GetPlayerController()
	*/
	class AUnboundPlayerController* GetPlayerController() const;
};
