// Fill out your copyright notice in the Description page of Project Settings.


#include "Interaction.h"
#include "InteractionComponent.h"

#include "GameFramework/Actor.h"
#include "Engine/NetDriver.h"
#include "Engine/ActorChannel.h"
#include "UnrealNetwork.h"

void UInteraction::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UInteraction, InteractionComponent);
	DOREPLIFETIME(UInteraction, Speaker);
}

bool UInteraction::CallRemoteFunction(UFunction* Function, void* Parms, FOutParmRec* OutParms, FFrame* Stack)
{
	AActor* Owner = GetOwningActor();
	if (Owner == nullptr) return false;

	UNetDriver* NetDriver = Owner->GetNetDriver();
	if (NetDriver == nullptr) return false;

	NetDriver->ProcessRemoteFunction(Owner, Function, Parms, OutParms, Stack, this);

	return true;
}

int32 UInteraction::GetFunctionCallspace(UFunction* Function, FFrame* Stack)
{
	AActor* Owner = GetOwningActor();
	if (Owner == nullptr) return FunctionCallspace::Local;

	return Owner->GetFunctionCallspace(Function, Stack);
}

void UInteraction::PreDestroyFromReplication()
{
	Super::PreDestroyFromReplication();

	OnStopInteraction();
}

void UInteraction::Init(UInteractionComponent* InInteractionComponent, AActor* InSpeaker)
{
	Speaker = InSpeaker;
	InteractionComponent = InInteractionComponent;

	InteractionComponent->StartInteraction(this);
}

void UInteraction::OnStartInteraction()
{

}

void UInteraction::OnStopInteraction()
{

}

AActor* UInteraction::GetOwningActor()
{
	if (InteractionComponent)
		return InteractionComponent->GetOwner();

	return nullptr;
}

AUnboundPlayerController* UInteraction::GetPlayerController() const
{
	return InteractionComponent->GetPlayerController();
}
