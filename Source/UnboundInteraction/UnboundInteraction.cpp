
#include "UnboundInteraction.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_GAME_MODULE( FDefaultGameModuleImpl, UnboundInteraction );

DEFINE_LOG_CATEGORY(LogUnboundInteraction);
