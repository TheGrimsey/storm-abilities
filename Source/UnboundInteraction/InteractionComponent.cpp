// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractionComponent.h"
#include "Interaction.h"

#include "UnrealNetwork.h"
#include "Engine/ActorChannel.h"

#include "UnboundPlayerController.h"

// Sets default values for this component's properties
UInteractionComponent::UInteractionComponent()
{
	PrimaryComponentTick.bCanEverTick = false;

	bReplicates = true;

	InteractionStatus = EInteractionType::None;
}

void UInteractionComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION_NOTIFY(UInteractionComponent, Interaction, COND_None, REPNOTIFY_OnChanged);
}

bool UInteractionComponent::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool WroteSomething = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	if (Interaction)
	{
		WroteSomething |= Channel->ReplicateSubobject(Interaction, *Bunch, *RepFlags);
	}

	return WroteSomething;
}

UInteraction* UInteractionComponent::BP_GetInteractionAs(TSubclassOf<UInteraction> Class, bool& IsValid)
{
	//The interaction is valid if we have an interaction and it is of the class requested.
	IsValid = Interaction && Interaction->IsA(Class);

	return Interaction;
}

void UInteractionComponent::StartInteraction(UInteraction* NewInteraction)
{
	//Only authority can start interactions.
	if (HasAuthority())
	{
		//If an interaction is already in progress then let's stop it.
		if (IsInteracting())
		{
			StopInteraction();
		}

		//Set interaction status as the new interaction's type.
		InteractionStatus = NewInteraction->GetInteractionType();
		Interaction = NewInteraction;
		
		Interaction->OnStartInteraction();
		
		//Broadcast that an interaction has started.
		OnInteractionStarted.Broadcast(InteractionStatus);
	}
}

void UInteractionComponent::StopInteraction()
{
	//Only authority can stop interactions and we need an interaction to be able to stop one.
	if (HasAuthority() && IsInteracting())
	{
		//Save old interaction type for event.
		EInteractionType OldInteractionType = InteractionStatus;

		Interaction->OnStopInteraction();
		Interaction->MarkPendingKill();

		Interaction = nullptr;
		InteractionStatus = EInteractionType::None;

		//Broadcast that an interaction has ended. Done after removing interaction to match up with clientside.
		OnInteractionEnded.Broadcast(OldInteractionType);
	}
}

AUnboundPlayerController* UInteractionComponent::GetPlayerController() const
{
	return Cast<AUnboundPlayerController>(GetOwner());
}

void UInteractionComponent::OnRep_Interaction()
{
	if (Interaction != nullptr)
	{
		//If we already were in an interaction before this let's broadcast that it ended.
		if (IsInteracting())
		{
			OnInteractionEnded.Broadcast(InteractionStatus);
		}

		InteractionStatus = Interaction->GetInteractionType();

		Interaction->OnStartInteraction();

		OnInteractionStarted.Broadcast(InteractionStatus);
	}
	else
	{
		OnInteractionEnded.Broadcast(InteractionStatus);

		InteractionStatus = EInteractionType::None;
	}
}
