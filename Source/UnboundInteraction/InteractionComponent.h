// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Components/ActorComponent.h"

#include "InteractionTypes.h"

#include "InteractionComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnInteractionX, EInteractionType, InteractionType );

UCLASS( ClassGroup=(Custom))
class UNBOUNDINTERACTION_API UInteractionComponent : public UActorComponent
{
	friend class UInteraction;

	GENERATED_BODY()

	/*
	*	Variables
	*/
public:
	UPROPERTY(BlueprintAssignable)
	FOnInteractionX OnInteractionStarted;

	UPROPERTY(BlueprintAssignable)
	FOnInteractionX OnInteractionEnded;

protected:
	//Our current interaction status.
	UPROPERTY()
	EInteractionType InteractionStatus;

	//The current interaction (if any)
	UPROPERTY(ReplicatedUsing=OnRep_Interaction, VisibleAnywhere)
	class UInteraction* Interaction;

	/*
	*	Methods
	*/
public:	
	UInteractionComponent();

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	virtual bool ReplicateSubobjects(class UActorChannel* Channel, class FOutBunch* Bunch, FReplicationFlags* RepFlags);

	UFUNCTION(BlueprintPure)
	EInteractionType GetInteractionType() const { return InteractionStatus; }

	UFUNCTION(BlueprintPure)
	bool IsInteracting() const { return InteractionStatus != EInteractionType::None; }

	/*
	*	Returns Interaction.
	*/
	class UInteraction* GetInteraction()
	{
		return Interaction;
	}

	/*
	*	Returns Interaction as templated type. Does not guarantee that the Interaction is valid.
	*/
	template<typename T>
	T* GetInteractionAs()
	{
		return Cast<T>(Interaction);
	}

	/*
	*	Get the current interaction as Class.
	*	@Param Class The class to get the interaction as.
	*	@Param IsValid Whether or not the current interaction is of Class.
	*/
	UFUNCTION(BlueprintCallable, meta = (DisplayName="GetInteractionAs", DeterminesOutputType = "Class"))
	class UInteraction* BP_GetInteractionAs(TSubclassOf<class UInteraction> Class, bool& IsValid);

protected:
	/*
	*	Starts an interaction.
	*	Only accessable from UInteraction.
	*	@Param NewInteraction Interaction to start.
	*/
	void StartInteraction(class UInteraction* NewInteraction);

public:
	/*
	*	Stops the current interaction.
	*/
	UFUNCTION(BlueprintCallable)
	void StopInteraction();

	/*
	*	Returns true if we have authority over this component.
	*/
	bool HasAuthority() { return GetOwnerRole() == ENetRole::ROLE_Authority; }

	/*
	*	Returns the owning player controller.
	*/
	class AUnboundPlayerController* GetPlayerController() const;

private:
	UFUNCTION()
	void OnRep_Interaction();
};
