// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

UENUM(BlueprintType)
enum class EInteractionType : uint8
{
	None,
	Dialogue,
	Trading,
	Banking,
	Mail,

	PlayerInspect,

};
