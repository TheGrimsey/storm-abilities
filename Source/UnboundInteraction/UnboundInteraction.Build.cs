
using UnrealBuildTool;

public class UnboundInteraction : ModuleRules
{
	public UnboundInteraction(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine"});

        PublicDependencyModuleNames.AddRange(new string[] { "Unbound" });
    }
}
