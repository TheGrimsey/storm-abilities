// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class UnboundTarget : TargetRules
{
	public UnboundTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.Add("Unbound");
        ExtraModuleNames.Add("UnboundAbilities");
        ExtraModuleNames.Add("UnboundDungeons");
        ExtraModuleNames.Add("UnboundUI");
        ExtraModuleNames.Add("UnboundDialogue");
        ExtraModuleNames.Add("UnboundInteraction");
    }
}
