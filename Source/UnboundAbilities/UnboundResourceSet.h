// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AttributeSet.h"
#include "UnboundResourceSet.generated.h"

/**
 * 
 */
UCLASS()
class UNBOUNDABILITIES_API UUnboundResourceSet : public UAttributeSet
{
	GENERATED_BODY()
	
};
