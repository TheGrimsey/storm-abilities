// Fill out your copyright notice in the Description page of Project Settings.

#include "ProjectileManagerSubsystem.h"
#include "Projectile.h"

#include "Engine/GameInstance.h"
#include "Engine/World.h"
#include "Runtime/Core/Public/Async/ParallelFor.h"

//Max projectiles we expect to have. USed for reserving size of arrays.
#define EXPECTED_MAX_PROJECTILES 5000

void FProjectileManagerTickFunction::ExecuteTick(float DeltaTime, ELevelTick TickType, ENamedThreads::Type CurrentThread, const FGraphEventRef& MyCompletionGraphEvent)
{
	UE_LOG(LogTemp, Log, TEXT("EXECUTE TICK CALLED. ProjectileManagerSystem"));
	ProjectileManagerSubSystem->TickProjectiles(DeltaTime);
}

AProjectile* UProjectileManagerSubsystem::SpawnProjectile(AActor* Target, UProjectileDataAsset* InProjectileData, const FVector& InSpawnPosition, const FRotator& InSpawnRotation, const FGameplayEffectSpecHandle& EffectHandle, bool bCaptureAttributesDirectly)
{
	TRACE_CPUPROFILER_EVENT_SCOPE("UProjectileManagerSubsystem::SpawnProjectile")

	check(EffectHandle.Data.IsValid());

	//Capture attributes.
	if (bCaptureAttributesDirectly)
	{
		EffectHandle.Data.Get()->SetupAttributeCaptureDefinitions();
	}

	//TODO Pool projectiles.
	AProjectile* SpawnedProjectile = Target->GetWorld()->SpawnActorDeferred<AProjectile>(AProjectile::StaticClass(), FTransform::Identity, NULL, NULL, ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
	SpawnedProjectile->Init(InProjectileData, Target, EffectHandle);
	SpawnedProjectile->FinishSpawning(FTransform(InSpawnRotation, InSpawnPosition));

	return SpawnedProjectile;
}

void UProjectileManagerSubsystem::SpawnProjectiles(const FGameplayAbilityTargetDataHandle& Targets, UProjectileDataAsset* InProjectileData, const FVector& InSpawnPosition, const FRotator& InSpawnRotation, const FGameplayEffectSpecHandle& EffectHandle, TArray<class AProjectile*>& OutProjectiles, bool bCaptureAttributesDirectly)
{
	TRACE_CPUPROFILER_EVENT_SCOPE("UProjectileManagerSubsystem::SpawnProjectiles")
	
	//Capture attributes.
	if (bCaptureAttributesDirectly)
	{
		EffectHandle.Data.Get()->SetupAttributeCaptureDefinitions();
	}
	
	OutProjectiles.Reset();

	for (const TSharedPtr<FGameplayAbilityTargetData>& SharedTargetDataPtr : Targets.Data)
	{
		if (SharedTargetDataPtr.IsValid())
		{
			for (TWeakObjectPtr<AActor> targetActor : SharedTargetDataPtr->GetActors())
			{
				if (!targetActor.IsValid())
					continue;

				AProjectile* SpawnedProjectile = targetActor->GetWorld()->SpawnActorDeferred<AProjectile>(AProjectile::StaticClass(), FTransform::Identity, NULL, NULL, ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
				SpawnedProjectile->Init(InProjectileData, targetActor.Get(), EffectHandle);
				SpawnedProjectile->FinishSpawning(FTransform(InSpawnRotation, InSpawnPosition));

				OutProjectiles.Add(SpawnedProjectile);
			}
		}
	}
}

void UProjectileManagerSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	TickFunction.ProjectileManagerSubSystem = this;

	TickingProjectiles.Reserve(EXPECTED_MAX_PROJECTILES);
	PendingDestroyProjectiles.Reserve(EXPECTED_MAX_PROJECTILES);
}

void UProjectileManagerSubsystem::Deinitialize()
{
	TickFunction.UnRegisterTickFunction();
}

void UProjectileManagerSubsystem::TickProjectiles(float DeltaTime)
{
	TRACE_CPUPROFILER_EVENT_SCOPE("UProjectileManagerSubsystem::TickProjectiles")

	/*
	*	Check our last async traces.
	*/
	{
		TRACE_CPUPROFILER_EVENT_SCOPE("UProjectileManagerSubsystem::TickProjectiles - CheckTraces")
		for (AProjectile* Projectile : TickingProjectiles)
		{
			Projectile->CheckTraces();
		}
	}

	/*
	*	Destroy all pending destroy projectiles.
	*/
	{
		TRACE_CPUPROFILER_EVENT_SCOPE("UProjectileManagerSubsystem::TickProjectiles - Destroy projectiles")
		for (AProjectile* Projectile : PendingDestroyProjectiles)
		{
			TickingProjectiles.RemoveSwap(Projectile);
			Projectile->Destroy();
		}
		PendingDestroyProjectiles.Reset();
	}

	/*
	*	Do tick in parallel.
	*/
	{
		TRACE_CPUPROFILER_EVENT_SCOPE("UProjectileManagerSubsystem::TickProjectiles - Tick")
		ParallelFor(TickingProjectiles.Num(), [&](int32 Idx) 
		{
			TickingProjectiles[Idx]->ProjectileTick(DeltaTime);
		});
	}

	/*
	*	Queue async trace in game thread.
	*/
	{
		TRACE_CPUPROFILER_EVENT_SCOPE("UProjectileManagerSubsystem::TickProjectiles - QueueAsyncTrace")
			for (AProjectile* Projectile : TickingProjectiles)
			{
				Projectile->QueueAsyncTrace();
			}
	}

	/*
	*	MarkRenderTransformDirty. (Client only)
	*/
#if !UE_SERVER || UE_BUILD_DEBUG
#if UE_BUILD_DEBUG
	if(!GetGameInstance()->GetNetMode() != ENetMode::DedicatedServer)
#endif
	{
		TRACE_CPUPROFILER_EVENT_SCOPE("UProjectileManagerSubsystem::TickProjectiles - MarkRenderTransformDirty")
		for (AProjectile* Projectile : TickingProjectiles)
		{
			Projectile->MarkRenderTransformsDirty();
		}
	}
#endif
}

void UProjectileManagerSubsystem::RegisterProjectile(AProjectile* Projectile)
{
	TickingProjectiles.Add(Projectile);

	if (!TickFunction.IsTickFunctionRegistered())
	{
		TickFunction.RegisterTickFunction(GetGameInstance()->GetWorld()->GetCurrentLevel());
	}
}

void UProjectileManagerSubsystem::UnRegisterProjectile(AProjectile* Projectile)
{
	//Remove without shrinking if we are below expected amount of projectiles.
	const bool Shrink = TickingProjectiles.Num() > EXPECTED_MAX_PROJECTILES;
	TickingProjectiles.RemoveSingleSwap(Projectile, Shrink);

	if (TickingProjectiles.Num() == 0)
	{
		TickFunction.UnRegisterTickFunction();
	}
}

void UProjectileManagerSubsystem::QueueProjectileDestruction(AProjectile* Projectile)
{
	PendingDestroyProjectiles.Add(Projectile);
}
