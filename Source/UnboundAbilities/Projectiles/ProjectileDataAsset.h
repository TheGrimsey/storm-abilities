// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "ProjectileDataAsset.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class UNBOUNDABILITIES_API UProjectileDataAsset : public UDataAsset
{
	friend class AProjectile;

	GENERATED_BODY()

protected:
	//Projectile Visual Data (Mesh, Particles, etc)
	UPROPERTY(EditAnywhere, Category = Visuals)
	TSoftObjectPtr<class UProjectileVisualsAsset> Visuals;

	/*
	*	Data
	*/
	//Movement Speed of the projectile.
	UPROPERTY(EditAnywhere, Category = Data, meta = (ClampMin = 0.f))
	float Speed = 50.f;

	//How much the projectile can turn in a second.
	UPROPERTY(EditAnywhere, Category = Data, meta = (ClampMin = 0.f))
	float TurnSpeed = 45.f;

	//How big the projectile's collision sphere is.
	UPROPERTY(EditAnywhere, Category = Data, meta = (ClampMin = 0.f))
	float ProjectileRadius = 32.f;
};
