// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "ProjectileVisualsAsset.generated.h"

/**
 * 
 */
UCLASS()
class UNBOUNDABILITIES_API UProjectileVisualsAsset : public UDataAsset
{
	friend class AProjectile;

	GENERATED_BODY()
	
protected:
	/*
	*	Visuals
	*/

	//Projectile Mesh
	UPROPERTY(EditAnywhere, Category = Visuals)
	class UStaticMesh* Mesh;

	//Materials to be applied to the projectile mesh.
	UPROPERTY(EditAnywhere, Category = Visuals)
	TArray<class UMaterialInterface*> Materials;

	//Scale of the mesh. Useful for meshes that are larger than intended for the projectile
	UPROPERTY(EditAnywhere, Category = Visuals, meta = (ClampMin = 0.01f))
	float MeshScale = 1.f;

	//Niagara Particle System used as the particle effect for the projectile.
	UPROPERTY(EditAnywhere, Category = Visuals)
	class UNiagaraSystem* ParticleSystem;
};
