// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "GameFramework/Actor.h"
#include "WorldCollision.h"

#include "GameplayEffect.h"

#include "UnboundAbilities.h"

#include "Projectile.generated.h"

UCLASS(NotBlueprintable)
class UNBOUNDABILITIES_API AProjectile : public AActor
{
	GENERATED_BODY()

	/*
	*	Variables
	*/
protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* Mesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UNiagaraComponent* ParticleSystem;

	//The delta between our new location and current location calculated in PreTick.
	FVector DeltaLocation;

	//Our new rotation calculated in PreTick
	FRotator NewRotation;

	//The handle for our asynctrace.
	FTraceHandle TraceHandle;

	//The ProjectileDataAsset which we get all our data from.
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Replicated, ReplicatedUsing=OnRep_ProjectileData, meta = (ExposeOnSpawn))
	class UProjectileDataAsset* ProjectileData;

	//Our target.
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Replicated, meta = (ExposeOnSpawn))
	TSoftObjectPtr<AActor> TargetActor;

	//Effect to apply upon projectile hit.
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly)
	FGameplayEffectSpecHandle EffectToApply;

	/*
	*	Methods
	*/
public:	
	// Sets default values for this actor's properties
	AProjectile();

	// Handle replicated properties lifetimes.
	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const override;

	// Called when this actor is started.
	virtual void BeginPlay();

	void Init(class UProjectileDataAsset* InProjectileData, TSoftObjectPtr<AActor> InTargetActor, const FGameplayEffectSpecHandle& InEffectToApply);

	//Checks our async trace and handles what we do to the hit actors.
	void CheckTraces();

	//"Pre"-tick. Could be called on threads other than game thread so don't modify any actor variables here.
	void ProjectileTick(float DeltaTime);

	// Tick. Called only on game thread.
	void QueueAsyncTrace();

	//Override blueprint destroy to not break ticks.
	virtual void K2_DestroyActor();

	//Call to destroy a projectile.
	UFUNCTION(BlueprintCallable)
	void DestroyProjectile();

	// Called whenever this actor is being removed from a level.
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason);

	//Called on recieving client when ProjectileData is replicated.
	UFUNCTION()
	void OnRep_ProjectileData();

	// Called when the ProjectileVisuals have been loaded.
	UFUNCTION()
	void OnVisualsLoaded();

	//Mark render transforms of components dirty to update position.
	void MarkRenderTransformsDirty();
};
