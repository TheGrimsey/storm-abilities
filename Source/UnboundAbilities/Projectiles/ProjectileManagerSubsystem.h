// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Subsystems/GameInstanceSubsystem.h"

#include "UnboundAbilities.h"
#include "GameplayAbilities/Public/GameplayEffect.h"
#include "GameplayAbilities\Public\Abilities\GameplayAbilityTargetTypes.h"

#include "ProjectileManagerSubsystem.generated.h"

USTRUCT()
struct FProjectileManagerTickFunction : public FTickFunction
{
	GENERATED_BODY()

	FProjectileManagerTickFunction() : FTickFunction()
	{
		TickGroup = ETickingGroup::TG_PrePhysics;
		bCanEverTick = true;
		bStartWithTickEnabled = true;
		bAllowTickOnDedicatedServer = true;
		bRunOnAnyThread = false;
	}
	
	class UProjectileManagerSubsystem* ProjectileManagerSubSystem;

	virtual void ExecuteTick(float DeltaTime, ELevelTick TickType, ENamedThreads::Type CurrentThread, const FGraphEventRef& MyCompletionGraphEvent) override;

	/** Abstract function to describe this tick. Used to print messages about illegal cycles in the dependency graph **/
	virtual FString DiagnosticMessage()
	{
		return FString(TEXT("ProjectileManagerSubSystem"));
	}
	/** Function to give a 'context' for this tick, used for grouped active tick reporting */
	virtual FName DiagnosticContext(bool bDetailed)
	{
		return FName(TEXT("ProjectileManagerSubSystem"));
	}
};

template<>
struct TStructOpsTypeTraits<FProjectileManagerTickFunction> : public TStructOpsTypeTraitsBase2<FProjectileManagerTickFunction>
{
	enum
	{
		WithCopy = false
	};
};

/**
 *	SubSystem that handles ticking Projectile actors.
 */
UCLASS()
class UNBOUNDABILITIES_API UProjectileManagerSubsystem : public UGameInstanceSubsystem
{
	friend class AProjectile;

	GENERATED_BODY()

	/*
	*	Variables
	*/
protected:
	UPROPERTY()
	FProjectileManagerTickFunction TickFunction;

	UPROPERTY()
	TArray<class AProjectile*> TickingProjectiles;

	UPROPERTY()
	TArray<class AProjectile*> PendingDestroyProjectiles;

	/*
	*	Methods
	*/
public:
	UFUNCTION(BlueprintCallable)
	class AProjectile* SpawnProjectile(class AActor* Target, class UProjectileDataAsset* InProjectileData, const FVector& InSpawnPosition, const FRotator& InSpawnRotation, const FGameplayEffectSpecHandle& EffectSpec, bool bCaptureAttributesDirectly = true);

	UFUNCTION(BlueprintCallable)
	void SpawnProjectiles(const FGameplayAbilityTargetDataHandle& Targets, class UProjectileDataAsset* InProjectileData, const FVector& InSpawnPosition, const FRotator& InSpawnRotation, const FGameplayEffectSpecHandle& EffectHandle, TArray<class AProjectile*>& OutProjectiles, bool bCaptureAttributesDirectly = true);

	/** Called when system initializes. */
	virtual void Initialize(FSubsystemCollectionBase& Collection) override;

	/** Called when system is deactivated. */
	virtual void Deinitialize() override;

	UFUNCTION()
	void TickProjectiles(float DeltaTime);

private:
	void RegisterProjectile(AProjectile* Projectile);

	void UnRegisterProjectile(AProjectile* Projectile);

	void QueueProjectileDestruction(AProjectile* Projectile);
};
