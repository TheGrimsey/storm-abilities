// Fill out your copyright notice in the Description page of Project Settings.


#include "Projectile.h"

#include "Engine/GameInstance.h"
#include "Components/StaticMeshComponent.h"

#include "Niagara/Public/NiagaraFunctionLibrary.h"
#include "Niagara/Public/NiagaraComponent.h"

#include "ProjectileDataAsset.h"
#include "ProjectileVisualsAsset.h"
#include "Engine/AssetManager.h"

#include "UnrealNetwork.h"

#include "AbilitySystemInterface.h"
#include "AbilitySystemComponent.h"

#include "ProjectileManagerSubsystem.h"

// Sets default values
AProjectile::AProjectile()
{
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetCastShadow(false);
	SetRootComponent(Mesh);

	/*
	*	Set up replication properties.
	*/
	bReplicates = true;
	SetReplicatingMovement(true);

	//100m radius.
	NetCullDistanceSquared = 100000000.f;

	//Try to update this 5 times per second or about every 6th tick on the server (if at 30 tps).
	NetUpdateFrequency = 5.f;
	MinNetUpdateFrequency = 1.f;

	//Set ReplicatedMovement compression.
	FRepMovement& MutRepMovement = GetReplicatedMovement_Mutable();
	MutRepMovement.LocationQuantizationLevel = EVectorQuantization::RoundWholeNumber;
	MutRepMovement.VelocityQuantizationLevel = EVectorQuantization::RoundWholeNumber;
	MutRepMovement.RotationQuantizationLevel = ERotatorQuantization::ByteComponents;

	/*
	 *	Disable collision for optimization.
	 */
	SetActorEnableCollision(false);
}

void AProjectile::GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AProjectile, ProjectileData);

	DOREPLIFETIME(AProjectile, TargetActor);
}


void AProjectile::BeginPlay()
{
	Super::BeginPlay();

	// Unregister tick function. We handle this in the UProjectileManagerSubSystem.
	PrimaryActorTick.UnRegisterTickFunction();

	//Register this projectile with the ProjectileManager so we can tick.
	UProjectileManagerSubsystem* ProjectileManager = GetGameInstance()->GetSubsystem<UProjectileManagerSubsystem>();
	if (ProjectileManager)
	{
		ProjectileManager->RegisterProjectile(this);
	}

	//We need this for testing in single player.
	if (HasAuthority() && GetNetMode() != ENetMode::NM_DedicatedServer && ProjectileData)
	{
		OnRep_ProjectileData();
	}
}

void AProjectile::Init(UProjectileDataAsset* InProjectileData, TSoftObjectPtr<AActor> InTargetActor, const FGameplayEffectSpecHandle& InEffectToApply)
{
	ProjectileData = InProjectileData;
	TargetActor = InTargetActor;
	EffectToApply = InEffectToApply;

	ForceNetUpdate();
}

void AProjectile::CheckTraces()
{
	TRACE_CPUPROFILER_EVENT_SCOPE("AProjectile::CheckTraces")

	//Grab our tracedata.
	FOverlapDatum AsyncTraceResult;
	GetWorld()->QueryOverlapData(TraceHandle, AsyncTraceResult);

	//Check if we overlapped something.
	if (AsyncTraceResult.OutOverlaps.Num() > 0)
	{
		for (const FOverlapResult& OverlapResult : AsyncTraceResult.OutOverlaps)
		{
			if (AActor* OverlapActor = OverlapResult.GetActor())
			{
				UE_LOG(LogTemp, Log, TEXT("%s overlapped with %s"), *GetName(), *OverlapActor->GetName());

				if (IAbilitySystemInterface* AbilitySysInterface = Cast<IAbilitySystemInterface>(OverlapActor))
				{
					AbilitySysInterface->GetAbilitySystemComponent()->ApplyGameplayEffectSpecToSelf(*EffectToApply.Data.Get());
				}
			}
		}

		//If we hit something we should destroy this projectile.
		DestroyProjectile();
	}
}

void AProjectile::ProjectileTick(float DeltaTime)
{
	TRACE_CPUPROFILER_EVENT_SCOPE("AProjectile::ProjectileTick")

	/*
	*	Calculate new transform.
	*/

	if (TargetActor.IsValid())
	{
		//Calculate our new position.
		FRotator TargetRotation = FRotationMatrix::MakeFromX(TargetActor.Get()->GetRootComponent()->GetComponentLocation() - RootComponent->GetComponentLocation()).Rotator();

		NewRotation = FMath::RInterpTo(RootComponent->GetComponentRotation(), TargetRotation, DeltaTime, ProjectileData->TurnSpeed);
		DeltaLocation = NewRotation.Vector() * ProjectileData->Speed;
	}
	
	/*
	*	Update component positions.
	*/

	RootComponent->SetComponentToWorld(FTransform(NewRotation, RootComponent->GetComponentLocation() + DeltaLocation, RootComponent->GetComponentScale()));

	if (ParticleSystem)
	{
		ParticleSystem->SetComponentToWorld(RootComponent->GetComponentToWorld());
	}

	RootComponent->UpdateBounds();
}

void AProjectile::QueueAsyncTrace()
{
	TRACE_CPUPROFILER_EVENT_SCOPE("AProjectile::QueueAsyncTrace")

	//Queue async trace.
	TraceHandle = GetWorld()->AsyncOverlapByChannel(RootComponent->GetComponentLocation(), RootComponent->GetComponentTransform().GetRotation(), ECollisionChannel::ECC_GameTraceChannel1, FCollisionShape::MakeSphere(ProjectileData->ProjectileRadius));
}

void AProjectile::K2_DestroyActor()
{
	//NOPE NOPE NOPE NOPE. This should not be used. Instead DestroyProjectile should be used.
	DestroyProjectile();
}

void AProjectile::DestroyProjectile()
{
	//Queue the destruction of this projectile. This is just to make sure we don't delete in the middle of a tick.
	UProjectileManagerSubsystem* ProjectileManager = GetGameInstance()->GetSubsystem<UProjectileManagerSubsystem>();
	if (ProjectileManager)
	{
		ProjectileManager->QueueProjectileDestruction(this);
	}
}

void AProjectile::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	//UnRegister this projectile from the ProjectileManager so it doesn't try to tick us.
	UProjectileManagerSubsystem* ProjectileManager = GetGameInstance()->GetSubsystem<UProjectileManagerSubsystem>();
	if (ProjectileManager)
	{
		ProjectileManager->UnRegisterProjectile(this);
	}

	if (ParticleSystem)
	{
		ParticleSystem->ReleaseToPool();
	}

	Super::EndPlay(EndPlayReason);
}

void AProjectile::OnRep_ProjectileData()
{
	//Check if we have the visuals already loaded then let's just apply them ELSE load them in async.
	if (ProjectileData->Visuals.IsValid())
	{
		OnVisualsLoaded();
	}
	else
	{
		/*
		*	Async load the Visuals.
		*/
		FStreamableDelegate Delegate = FStreamableDelegate();
		Delegate.BindUObject(this, &AProjectile::OnVisualsLoaded);

		UAssetManager::GetStreamableManager().RequestAsyncLoad(ProjectileData->Visuals.ToSoftObjectPath(), Delegate);
	}
}

void AProjectile::OnVisualsLoaded()
{
	TRACE_CPUPROFILER_EVENT_SCOPE("AProjectile::OnVisualsLoaded")

	UProjectileVisualsAsset* LoadedVisuals = ProjectileData->Visuals.Get();

	/*
	*	Set up Mesh.
	*/
	Mesh->SetStaticMesh(LoadedVisuals->Mesh);

	for (int i = 0; i < LoadedVisuals->Materials.Num(); i++)
	{
		Mesh->SetMaterial(i, LoadedVisuals->Materials[i]);
	}

	Mesh->SetRelativeScale3D(FVector(LoadedVisuals->MeshScale));

	/*
	*	Create particle system.
	*/
	if (LoadedVisuals->ParticleSystem)
	{
		//TODO Object pooling.
		ParticleSystem = UNiagaraFunctionLibrary::SpawnSystemAttached(LoadedVisuals->ParticleSystem, GetRootComponent(), FName(), 
			FVector::ZeroVector, FRotator::ZeroRotator, 
			EAttachLocation::SnapToTarget, false, true, ENCPoolMethod::ManualRelease);
	}
}

void AProjectile::MarkRenderTransformsDirty()
{
	TRACE_CPUPROFILER_EVENT_SCOPE("AProjectile::MarkRenderTransformsDirty")

	Mesh->MarkRenderTransformDirty();

	if (ParticleSystem)
	{
		ParticleSystem->MarkRenderTransformDirty();
	}
}

