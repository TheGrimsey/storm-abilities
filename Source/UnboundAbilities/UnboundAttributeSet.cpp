// Fill out your copyright notice in the Description page of Project Settings.


#include "UnboundAttributeSet.h"

#include "GameplayEffectExtension.h"

#include "UnrealNetwork.h"

UUnboundAttributeSet::UUnboundAttributeSet()
{
	/*
	*	Attribute defaults.
	*/

	InitStrength(0.f);
	InitIntellect(0.f);
	InitEndurance(0.f);
	InitVitality(0.f);

	/*
	*	Resistance defaults.
	*/

	InitPhysicalResistance(0.f);
	InitMagicResistance(0.f);
	InitNatureResistance(0.f);
	InitElementalResistance(0.f);

	/*
	*	Resource defaults.
	*/

	InitHealth(0.f);
	InitMaxHealth(0.f);

	InitStamina(0.f);
	InitMaxStamina(0.f);

	InitMana(0.f);
	InitMaxMana(0.f);
}

void UUnboundAttributeSet::GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	/*
	*	Attributes
	*/

	DOREPLIFETIME_CONDITION(UUnboundAttributeSet, Strength, COND_OwnerOnly);

	DOREPLIFETIME_CONDITION(UUnboundAttributeSet, Intellect, COND_OwnerOnly);

	DOREPLIFETIME_CONDITION(UUnboundAttributeSet, Endurance, COND_OwnerOnly);

	DOREPLIFETIME_CONDITION(UUnboundAttributeSet, Vitality, COND_OwnerOnly);

	/*
	*	Resistances
	*/

	DOREPLIFETIME(UUnboundAttributeSet, PhysicalResistance);

	DOREPLIFETIME(UUnboundAttributeSet, MagicResistance);
	
	DOREPLIFETIME(UUnboundAttributeSet, NatureResistance);
	
	DOREPLIFETIME(UUnboundAttributeSet, ElementalResistance);

	/*
	*	Resources
	*/

	DOREPLIFETIME(UUnboundAttributeSet, MaxHealth);
	DOREPLIFETIME(UUnboundAttributeSet, Health);

	DOREPLIFETIME_CONDITION(UUnboundAttributeSet, MaxStamina, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(UUnboundAttributeSet, Stamina, COND_OwnerOnly);

	DOREPLIFETIME_CONDITION(UUnboundAttributeSet, MaxMana, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(UUnboundAttributeSet, Mana, COND_OwnerOnly);

}

void UUnboundAttributeSet::PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue)
{
	//All Attributes should always be 0 or higher.
	if (NewValue < 0.f)
	{
		NewValue = 0.f;
	}

	//Pick the lower value to make sure we are always below or at Max. (Since we already clamp it to be >=0 above we only need to worry about if it is above Max)
	if (Attribute == GetHealthAttribute())
	{
		NewValue = FGenericPlatformMath::Min<float>(NewValue, MaxHealth.GetCurrentValue());
	}
	else if (Attribute == GetStaminaAttribute())
	{
		NewValue = FGenericPlatformMath::Min<float>(NewValue, MaxStamina.GetCurrentValue());
	}
	else if (Attribute == GetManaAttribute())
	{
		NewValue = FGenericPlatformMath::Min<float>(NewValue, MaxMana.GetCurrentValue());
	}
}

void UUnboundAttributeSet::PreAttributeBaseChange(const FGameplayAttribute& Attribute, float& NewValue) const
{
	//All Attributes should always be 0 or higher.
	if (NewValue < 0.f)
	{
		NewValue = 0.f;
	}

	//Clamp Health, Stamina & Mana between 0.f and their MaxValue.
	if (Attribute == GetHealthAttribute())
	{
		NewValue = FGenericPlatformMath::Min<float>(NewValue, MaxHealth.GetCurrentValue());
	}
	else if (Attribute == GetStaminaAttribute())
	{
		NewValue = FGenericPlatformMath::Min<float>(NewValue, MaxStamina.GetCurrentValue());
	}
	else if (Attribute == GetManaAttribute())
	{
		NewValue = FGenericPlatformMath::Min<float>(NewValue, MaxMana.GetCurrentValue());
	}
}

/*
*	Attributes OnRep functions definitions.
*/

void UUnboundAttributeSet::OnRep_Strength()
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UUnboundAttributeSet, Strength);
}

void UUnboundAttributeSet::OnRep_Intellect()
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UUnboundAttributeSet, Intellect);
}

void UUnboundAttributeSet::OnRep_Endurance()
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UUnboundAttributeSet, Endurance);
}

void UUnboundAttributeSet::OnRep_Vitality()
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UUnboundAttributeSet, Vitality);
}

/*
*	Resistances OnRep functions definitions.
*/

void UUnboundAttributeSet::OnRep_PhysicalResistance()
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UUnboundAttributeSet, PhysicalResistance);
}

void UUnboundAttributeSet::OnRep_MagicResistance()
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UUnboundAttributeSet, MagicResistance);
}

void UUnboundAttributeSet::OnRep_NatureResistance()
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UUnboundAttributeSet, NatureResistance);
}

void UUnboundAttributeSet::OnRep_ElementalResistance()
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UUnboundAttributeSet, ElementalResistance);
}

/*
*	Resources OnRep functions definitions.
*/

void UUnboundAttributeSet::OnRep_Health()
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UUnboundAttributeSet, Health);
}

void UUnboundAttributeSet::OnRep_MaxHealth()
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UUnboundAttributeSet, MaxHealth);
}

void UUnboundAttributeSet::OnRep_Stamina()
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UUnboundAttributeSet, Stamina);
}

void UUnboundAttributeSet::OnRep_MaxStamina()
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UUnboundAttributeSet, MaxStamina);
}

void UUnboundAttributeSet::OnRep_Mana()
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UUnboundAttributeSet, Mana);
}

void UUnboundAttributeSet::OnRep_MaxMana()
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UUnboundAttributeSet, MaxMana);
}
