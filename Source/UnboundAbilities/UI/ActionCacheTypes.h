// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "ActionCacheTypes.generated.h"

/**
 * 
 */
UENUM(Blueprintable)
enum class EActionCacheType : uint8
{
	None,
	Ability
};

USTRUCT(Blueprintable)
struct FActionBarCacheData
{
	GENERATED_BODY()

	//The Id of the ActionBar this Data is related to.
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	uint8 ActionBarId;

	//The slot in the actionbar that this Data is related to.
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	uint8 Slot;
	
	//The type of this data.
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EActionCacheType ActionType;

	//The AbilityClass this data has.
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<class UUnboundGameplayAbility> AbilityClass;

	//Returns true if Other points to the same slot and actionbar.
	FORCEINLINE bool HasSameSlot(const FActionBarCacheData& Other)
	{
		return Slot == Other.Slot && ActionBarId == Other.ActionBarId;
	}

	bool operator==(const FActionBarCacheData& Other)
	{
		return Slot == Other.Slot && ActionBarId == Other.ActionBarId && ActionType == Other.ActionType;
	}
};