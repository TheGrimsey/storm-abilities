// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayEffectUIData.h"
#include "UnboundGASUIData.generated.h"

/**
 * 
 */
UCLASS()
class UNBOUNDABILITIES_API UUnboundGASUIData : public UGameplayEffectUIData
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditAnywhere)
	FText Name;

	UPROPERTY(EditAnywhere, meta = (MultiLine = true))
	FText Description;

	UPROPERTY(EditAnywhere)
	class UTexture2D* Icon;

public:
	UFUNCTION(BlueprintPure, Category = "UIData")
	FORCEINLINE FText GetName()
	{
		return Name;
	}

	UFUNCTION(BlueprintPure, Category = "UIData")
	FORCEINLINE class UTexture2D* GetIcon()
	{
		return Icon;
	}

	UFUNCTION(BlueprintPure, Category = "UIData")
	const FText GetDescription()
	{
		return Description;
	}
};
