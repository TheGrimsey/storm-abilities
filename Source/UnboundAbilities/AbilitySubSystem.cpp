// Fill out your copyright notice in the Description page of Project Settings.

#include "AbilitySubSystem.h"
#include "AbilitySystemGlobals.h"

void UAbilitySubSystem::Initialize(FSubsystemCollectionBase& Collection)
{
	Super::Initialize(Collection);

	UAbilitySystemGlobals::Get().InitGlobalData();
}