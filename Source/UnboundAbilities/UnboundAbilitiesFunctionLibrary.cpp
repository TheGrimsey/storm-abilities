// Fill out your copyright notice in the Description page of Project Settings.


#include "UnboundAbilitiesFunctionLibrary.h"
#include "AbilitySystemComponent.h"

#include "Effect/GameplayEffectHandler.h"

#include "Ability/UnboundGameplayAbility.h"

TArray<FActiveGameplayEffectHandle> UAbilitiesUtilityFunctionLibrary::GetActiveEffectsWithoutTag(const UAbilitySystemComponent* AbilitySystem, const FGameplayTag GameplayTag)
{
	return AbilitySystem->GetActiveEffects(FGameplayEffectQuery::MakeQuery_MatchNoEffectTags(FGameplayTagContainer(GameplayTag)));
}

bool UAbilitiesUtilityFunctionLibrary::DoesEffectHaveTag(const FActiveGameplayEffectHandle Handle, const FGameplayTag GameplayTag, bool ExactTag)
{
	const FActiveGameplayEffect* ActiveGE = Handle.GetOwningAbilitySystemComponent()->GetActiveGameplayEffect(Handle);

	if (ActiveGE)
	{
		FGameplayTagContainer Tags;
		ActiveGE->Spec.GetAllAssetTags(Tags);

		return ExactTag ? Tags.HasTagExact(GameplayTag) : Tags.HasTag(GameplayTag);
	}

	return false;
}

void UAbilitiesUtilityFunctionLibrary::BindStackChangeEvent(struct FActiveGameplayEffectHandle Handle, UObject* Object, const FName FuncName)
{
	Handle.GetOwningAbilitySystemComponent()->OnGameplayEffectStackChangeDelegate(Handle)->AddUFunction(Object, FuncName);
}

void UAbilitiesUtilityFunctionLibrary::BindEffectRemovedEvent(struct FActiveGameplayEffectHandle Handle, UObject* Object, const FName FuncName)
{
	Handle.GetOwningAbilitySystemComponent()->OnGameplayEffectRemoved_InfoDelegate(Handle)->AddUFunction(Object, FuncName);
}

void UAbilitiesUtilityFunctionLibrary::BindEffectAddedEvent(UAbilitySystemComponent* AbilitySystem, TScriptInterface<IGameplayEffectHandler> GameplayEffectHandler)
{
	AbilitySystem->OnActiveGameplayEffectAddedDelegateToSelf.AddWeakLambda(GameplayEffectHandler.GetObject(), [](UAbilitySystemComponent* AbilitySystem, const FGameplayEffectSpec& GESpec, FActiveGameplayEffectHandle ActiveGEHandle, TScriptInterface<IGameplayEffectHandler> InGEHandler)
		{
			IGameplayEffectHandler* GEHandlerInterface = Cast<IGameplayEffectHandler>(InGEHandler.GetObject());

			GEHandlerInterface->Execute_OnEffectAdded(InGEHandler.GetObject(), ActiveGEHandle);
		}, GameplayEffectHandler);
}

const TArray<struct FGameplayAbilitySpec>& UAbilitiesUtilityFunctionLibrary::GetAllActivatableAbilities(const UAbilitySystemComponent* AbilitySystem)
{
	check(AbilitySystem != nullptr);

	return AbilitySystem->GetActivatableAbilities();
}

const struct FGameplayAbilitySpecHandle UAbilitiesUtilityFunctionLibrary::GetAbilityHandleFromClass(UAbilitySystemComponent* AbilitySystem, const TSubclassOf<class UGameplayAbility>& AbilityClass)
{
	if (AbilitySystem)
	{
		const FGameplayAbilitySpec* Spec = AbilitySystem->FindAbilitySpecFromClass(AbilityClass);

		if (Spec)
		{
			return Spec->Handle;
		}
	}

	return FGameplayAbilitySpecHandle();
}

bool UAbilitiesUtilityFunctionLibrary::HasAbilityByClass(UAbilitySystemComponent* AbilitySystem, const TSubclassOf<UGameplayAbility>& AbilityClass)
{
	return AbilitySystem && AbilitySystem->FindAbilitySpecFromClass(AbilityClass) != nullptr;
}

const TSubclassOf<UGameplayAbility> UAbilitiesUtilityFunctionLibrary::GetAbilityClassFromSpec(const FGameplayAbilitySpec& AbilitySpec)
{
	return AbilitySpec.Ability->GetClass();
}

bool UAbilitiesUtilityFunctionLibrary::DoesAbilityHaveTag(const TSubclassOf<UGameplayAbility> Ability, const FGameplayTag GameplayTag, bool ExactTag)
{
	return Ability && (ExactTag ? Ability.GetDefaultObject()->AbilityTags.HasTagExact(GameplayTag) : Ability.GetDefaultObject()->AbilityTags.HasTag(GameplayTag));
}

void UAbilitiesUtilityFunctionLibrary::GetAbilityCooldownTimeFromSpec(const UAbilitySystemComponent* AbilitySystem, const FGameplayAbilitySpec& Spec, float& TimeLeft, float& CooldownDuration)
{
	Spec.Ability->GetCooldownTimeRemainingAndDuration(Spec.Handle, AbilitySystem->AbilityActorInfo.Get(), TimeLeft, CooldownDuration);
}

void UAbilitiesUtilityFunctionLibrary::GetAbilityCooldownTimeFromHandle(UAbilitySystemComponent* AbilitySystem, const FGameplayAbilitySpecHandle& Handle, float& TimeLeft, float& CooldownDuration)
{
	if (AbilitySystem)
	{
		const FGameplayAbilitySpec* AbilitySpec = AbilitySystem->FindAbilitySpecFromHandle(Handle);

		if (AbilitySpec)
		{
			GetAbilityCooldownTimeFromSpec(AbilitySystem, *AbilitySpec, TimeLeft, CooldownDuration);
		}
	}
}
