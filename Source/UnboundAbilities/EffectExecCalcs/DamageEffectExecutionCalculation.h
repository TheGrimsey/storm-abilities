// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "UnboundAbilities.h"

#include "GameplayEffectExecutionCalculation.h"

#include "DamageEffectExecutionCalculation.generated.h"

/**
 * 
 */
UCLASS()
class UNBOUNDABILITIES_API UDamageEffectExecutionCalculation : public UGameplayEffectExecutionCalculation
{
	GENERATED_BODY()

	/*
	*	Variables
	*/
protected:
	UPROPERTY(EditAnywhere, meta=( ClampMin = 0.f))
	//The minimum damage to deal (before BonusDamage and reduction by resistance)
	float MinDamage = 0.f;

	UPROPERTY(EditAnywhere, meta = (ClampMin = 0.f))
	//The maximum damage to deal (before BonusDamage and reduction by resistance)
	float MaxDamage = 0.f;

	UPROPERTY(EditAnywhere, meta = (FilterMetaTag = "NotAttributeStat"))
	//Attribute used to calculate the bonus damage. Normally you'd probably want to leave this at Strength or Intellect or none.
	FGameplayAttribute BonusDamageAttribute;

	UPROPERTY(EditAnywhere, meta=(FilterMetaTag="NotAttributeResistance"))
	//Attribute used to calculate the damage reduction. Normally you'd want this to be set to one of the resistance attributes (PhysicalResistance, MagicResistance, NatureResistance, ElementalResistance, etc).
	FGameplayAttribute ResistanceAttribute;

private:
	UPROPERTY()
	//Capture definition used for the BonusDamageAttribute.
	FGameplayEffectAttributeCaptureDefinition BonusDamageCaptureDefinition;

	UPROPERTY()
	//Capture definition used for the ResistanceAttribute.
	FGameplayEffectAttributeCaptureDefinition ResistanceCaptureDefiniton;
	
	/*
	*	Methods
	*/
public:
	UDamageEffectExecutionCalculation();

	//Updates attributes to capture based on our properties.
	void UpdateRelevantCaptureAttributes();

	void Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const override;

#if WITH_EDITORONLY_DATA
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;

	virtual void PostEditChangeChainProperty(struct FPropertyChangedChainEvent& PropertyChangedEvent) override;
#endif
};
