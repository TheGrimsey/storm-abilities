#pragma once

#include "GameplayEffectExecutionCalculation.h"
#include "UnboundAttributeSet.h"

struct UnboundAttributes
{
	/*
	*	Source Attributes
	*/
	//Attributes.
	DECLARE_ATTRIBUTE_CAPTUREDEF(Strength);

	DECLARE_ATTRIBUTE_CAPTUREDEF(Intellect);

	DECLARE_ATTRIBUTE_CAPTUREDEF(Endurance);

	DECLARE_ATTRIBUTE_CAPTUREDEF(Vitality);

	/*
	*	Target Attributes
	*/
	//Resources.
	DECLARE_ATTRIBUTE_CAPTUREDEF(Health);

	//Resistances.
	DECLARE_ATTRIBUTE_CAPTUREDEF(PhysicalResistance);

	DECLARE_ATTRIBUTE_CAPTUREDEF(MagicResistance);

	DECLARE_ATTRIBUTE_CAPTUREDEF(NatureResistance);

	DECLARE_ATTRIBUTE_CAPTUREDEF(ElementalResistance);

	UnboundAttributes()
	{
		/*
		*	Source Attributes
		*/
		//Attributes.
		DEFINE_ATTRIBUTE_CAPTUREDEF(UUnboundAttributeSet, Strength, Source, true);

		DEFINE_ATTRIBUTE_CAPTUREDEF(UUnboundAttributeSet, Intellect, Source, true);

		DEFINE_ATTRIBUTE_CAPTUREDEF(UUnboundAttributeSet, Endurance, Source, true);

		DEFINE_ATTRIBUTE_CAPTUREDEF(UUnboundAttributeSet, Vitality, Source, true);
		/*
		*	Target Attributes
		*/
		//Resources.
		DEFINE_ATTRIBUTE_CAPTUREDEF(UUnboundAttributeSet, Health, Target, false);

		//Resistances.
		DEFINE_ATTRIBUTE_CAPTUREDEF(UUnboundAttributeSet, PhysicalResistance, Target, false);

		DEFINE_ATTRIBUTE_CAPTUREDEF(UUnboundAttributeSet, MagicResistance, Target, false);

		DEFINE_ATTRIBUTE_CAPTUREDEF(UUnboundAttributeSet, NatureResistance, Target, false);

		DEFINE_ATTRIBUTE_CAPTUREDEF(UUnboundAttributeSet, ElementalResistance, Target, false);

	}
};

static const UnboundAttributes& GetAttributes()
{
	static UnboundAttributes UnboundAttribs;
	return UnboundAttribs;
}
