// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayEffectExecutionCalculation.h"
#include "EventExecCalculation.generated.h"

/**
 * 
 */
UCLASS()
class UNBOUNDABILITIES_API UEventExecCalculation : public UGameplayEffectExecutionCalculation
{
    GENERATED_BODY()

    /*
    *   Variables
    */
protected:
    UPROPERTY(EditDefaultsOnly)
    FGameplayTag EventTag;

    /*
    *   Methods
    */
public:
    void Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const override;
};
