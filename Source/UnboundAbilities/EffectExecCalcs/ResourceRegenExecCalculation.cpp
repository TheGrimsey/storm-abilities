// Fill out your copyright notice in the Description page of Project Settings.


#include "ResourceRegenExecCalculation.h"

#include "UnboundAttributeSet.h"

#include "UnboundEffectExecCalcTypes.h"

UResourceRegenExecCalculation::UResourceRegenExecCalculation()
{
	UpdateRelevantCaptureAttributes();
}

void UResourceRegenExecCalculation::UpdateRelevantCaptureAttributes()
{
	RelevantAttributesToCapture.Empty();

	//Target Resource
	RegenTargetResourceCaptureDefinition = FGameplayEffectAttributeCaptureDefinition(RegenTargetResource, EGameplayEffectAttributeCaptureSource::Target, false);
	RelevantAttributesToCapture.Add(RegenTargetResourceCaptureDefinition);

	//Vitality (Creating a capture definition here so it is always the right thing).
	VitalityCaptureDefinition = FGameplayEffectAttributeCaptureDefinition(GetAttributes().VitalityProperty, EGameplayEffectAttributeCaptureSource::Target, false);
	RelevantAttributesToCapture.Add(VitalityCaptureDefinition);

	//Health
	RelevantAttributesToCapture.Add(GetAttributes().HealthDef);
}

void UResourceRegenExecCalculation::Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const
{
	TRACE_CPUPROFILER_EVENT_SCOPE("UResourceRegenExecCalculation::Execute_Implementation")

	const FGameplayEffectSpec& Spec = ExecutionParams.GetOwningSpec();

	FAggregatorEvaluateParameters EvaluationParameters;
	EvaluationParameters.SourceTags = Spec.CapturedSourceTags.GetAggregatedTags();
	EvaluationParameters.TargetTags = Spec.CapturedTargetTags.GetAggregatedTags();

	/*
	*	Grab Vitality value.
	*/
	float VitalityValue = 0.f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(VitalityCaptureDefinition, EvaluationParameters, VitalityValue);

	/*
	*	Calculate final RegenAmount (per second).
	*/
	float RegenAmount = VitalityValue * VitalityCoefficient;

	OutExecutionOutput.AddOutputModifier(FGameplayModifierEvaluatedData(RegenTargetResource, EGameplayModOp::Additive, RegenAmount * Spec.GetPeriod()));
}


#if WITH_EDITORONLY_DATA
void UResourceRegenExecCalculation::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	//Update relevant attributes just incase something changed.
	UpdateRelevantCaptureAttributes();
}

void UResourceRegenExecCalculation::PostEditChangeChainProperty(FPropertyChangedChainEvent& PropertyChangedEvent)
{
	//Update relevant attributes just incase something changed.
	UpdateRelevantCaptureAttributes();
}
#endif