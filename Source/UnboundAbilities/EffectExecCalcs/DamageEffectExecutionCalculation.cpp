// Fill out your copyright notice in the Description page of Project Settings.


#include "DamageEffectExecutionCalculation.h"
#include "UnboundEffectExecCalcTypes.h"

UDamageEffectExecutionCalculation::UDamageEffectExecutionCalculation()
{
	UpdateRelevantCaptureAttributes();
}

void UDamageEffectExecutionCalculation::UpdateRelevantCaptureAttributes()
{
	RelevantAttributesToCapture.Empty();
	
	//Health
	RelevantAttributesToCapture.Add(GetAttributes().HealthDef);

	//Bonus Damage Attribute
	BonusDamageCaptureDefinition = FGameplayEffectAttributeCaptureDefinition(BonusDamageAttribute, EGameplayEffectAttributeCaptureSource::Source, true);
	RelevantAttributesToCapture.Add(BonusDamageCaptureDefinition);

	//Resistance Attribute
	ResistanceCaptureDefiniton = FGameplayEffectAttributeCaptureDefinition(ResistanceAttribute, EGameplayEffectAttributeCaptureSource::Target, false);
	RelevantAttributesToCapture.Add(ResistanceCaptureDefiniton);
}

void UDamageEffectExecutionCalculation::Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const
{
	TRACE_CPUPROFILER_EVENT_SCOPE("UDamageEffectExecutionCalculation::Execute_Implementation")

	const FGameplayEffectSpec& Spec = ExecutionParams.GetOwningSpec();

	FAggregatorEvaluateParameters EvaluationParameters;
	EvaluationParameters.SourceTags = Spec.CapturedSourceTags.GetAggregatedTags();
	EvaluationParameters.TargetTags = Spec.CapturedTargetTags.GetAggregatedTags();

	/*
	*   Randomise the base damage between Min and Max.
	*/
	float BaseDamage = FMath::FRandRange(MinDamage, MaxDamage);

	/*
	*	Grab the bonus damage.
	*/
	float BonusDamage = 0.f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(BonusDamageCaptureDefinition, EvaluationParameters, BonusDamage);

	/*
	*   Calculate resistance.
	*/
	float ResistanceValue = 0.f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(ResistanceCaptureDefiniton, EvaluationParameters, ResistanceValue);

	//Calculate the resistance multiplier. 1 point of resistance == -0.5% damage .
	float ResistanceMultiplier = (1 - ResistanceValue / 100 * 0.5f);

	/*
	*	Calculate the real damage by adding BaseDamage and BonusDamage and then multiplying it by the reduction caused by the armor.
	*/
	float FinalDamage = (BaseDamage + BonusDamage) * ResistanceMultiplier;

	/*
	*	Apply modifier to health.
	*/
	OutExecutionOutput.AddOutputModifier(FGameplayModifierEvaluatedData(GetAttributes().HealthProperty, EGameplayModOp::Additive, -FinalDamage));
}

#if WITH_EDITORONLY_DATA
void UDamageEffectExecutionCalculation::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	//Update relevant attributes just incase something changed.
	UpdateRelevantCaptureAttributes();
}

void UDamageEffectExecutionCalculation::PostEditChangeChainProperty(FPropertyChangedChainEvent& PropertyChangedEvent)
{
	//Update relevant attributes just incase something changed.
	UpdateRelevantCaptureAttributes();
}
#endif