// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "UnboundAbilities.h"

#include "GameplayEffectExecutionCalculation.h"

#include "HealingEffectExecutionCalculation.generated.h"

/**
 * 
 */
UCLASS()
class UNBOUNDABILITIES_API UHealingEffectExecutionCalculation : public UGameplayEffectExecutionCalculation
{
	GENERATED_BODY()

	/*
	*	Variables
	*/
protected:
	UPROPERTY(EditAnywhere, meta = (ClampMin = 0.f))
	//The minimum damage to deal (before BonusHealing)
	float MinHealing = 0.f;

	UPROPERTY(EditAnywhere, meta = (ClampMin = 0.f))
	//The maximum damage to deal (before BonusHealing)
	float MaxHealing = 0.f;

	UPROPERTY(EditAnywhere, meta = (FilterMetaTag = "NotAttributeStat"))
	//Attribute used to calculate the bonus healing.
	FGameplayAttribute BonusHealingAttribute;

private:
	UPROPERTY()
	//Capture definition used for the BonusHealingAttribute.
	FGameplayEffectAttributeCaptureDefinition BonusHealingCaptureDefinition;

	/*
	*	Methods
	*/
public:
	UHealingEffectExecutionCalculation();

	//Updates attributes to capture based on our properties.
	void UpdateRelevantCaptureAttributes();

	void Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const override;

#if WITH_EDITORONLY_DATA
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;

	virtual void PostEditChangeChainProperty(struct FPropertyChangedChainEvent& PropertyChangedEvent) override;
#endif
};
