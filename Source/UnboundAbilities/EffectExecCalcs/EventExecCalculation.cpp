// Fill out your copyright notice in the Description page of Project Settings.


#include "EventExecCalculation.h"
#include "AbilitySystemComponent.h"

void UEventExecCalculation::Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const
{
    const FGameplayEffectSpec& Spec = ExecutionParams.GetOwningSpec();

    FGameplayEventData Payload;
    Payload.ContextHandle = Spec.GetContext();
    Payload.EventMagnitude = Spec.GetLevel();
    Payload.Instigator = Spec.GetContext().GetInstigator();
    Payload.InstigatorTags = *Spec.CapturedSourceTags.GetAggregatedTags();

    int32 triggered = ExecutionParams.GetTargetAbilitySystemComponent()->HandleGameplayEvent(EventTag, &Payload);
    UE_LOG(LogTemp, Log, TEXT("Abilities triggered: %d"), triggered)
}
