// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "UnboundAbilities.h"

#include "GameplayEffectExecutionCalculation.h"

#include "ResourceRegenExecCalculation.generated.h"

/**
 *	Effect Execution Calculation that modifies attribute based on a multiple of vitality.
 */
UCLASS()
class UNBOUNDABILITIES_API UResourceRegenExecCalculation : public UGameplayEffectExecutionCalculation
{
	GENERATED_BODY()

	/*
	*	Variables
	*/
protected:
	//The resource to regenerate.
	UPROPERTY(EditAnywhere, meta = (FilterMetaTag = "NotAttributeResource"))
	FGameplayAttribute RegenTargetResource;

	//The coefficient for the multiplication of vitality.
	UPROPERTY(EditAnywhere)
	float VitalityCoefficient;

private:
	UPROPERTY()
	//Capture definition used for the BonusDamageAttribute.
	FGameplayEffectAttributeCaptureDefinition RegenTargetResourceCaptureDefinition;
	
	UPROPERTY()
	//Capture definition used for the BonusDamageAttribute.
	FGameplayEffectAttributeCaptureDefinition VitalityCaptureDefinition;

	/*
	*	Methods
	*/

public:
	UResourceRegenExecCalculation();

	//Updates attributes to capture based on our properties.
	void UpdateRelevantCaptureAttributes();

	void Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const override;

#if WITH_EDITORONLY_DATA
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;

	virtual void PostEditChangeChainProperty(struct FPropertyChangedChainEvent& PropertyChangedEvent) override;
#endif
};
