// Fill out your copyright notice in the Description page of Project Settings.


#include "HealingEffectExecutionCalculation.h"
#include "UnboundEffectExecCalcTypes.h"

UHealingEffectExecutionCalculation::UHealingEffectExecutionCalculation()
{
	UpdateRelevantCaptureAttributes();
}

void UHealingEffectExecutionCalculation::UpdateRelevantCaptureAttributes()
{
	RelevantAttributesToCapture.Empty();

	//Health
	RelevantAttributesToCapture.Add(GetAttributes().HealthDef);

	//Bonus Damage Attribute
	BonusHealingCaptureDefinition = FGameplayEffectAttributeCaptureDefinition(BonusHealingAttribute, EGameplayEffectAttributeCaptureSource::Source, true);
	RelevantAttributesToCapture.Add(BonusHealingCaptureDefinition);
}

void UHealingEffectExecutionCalculation::Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const
{
	TRACE_CPUPROFILER_EVENT_SCOPE("UHealingEffectExecutionCalculation::Execute_Implementation")

	const FGameplayEffectSpec& Spec = ExecutionParams.GetOwningSpec();

	FAggregatorEvaluateParameters EvaluationParameters;
	EvaluationParameters.SourceTags = Spec.CapturedSourceTags.GetAggregatedTags();
	EvaluationParameters.TargetTags = Spec.CapturedTargetTags.GetAggregatedTags();

	/*
	*   Randomise the base healing between Min and Max.
	*/
	float BaseHealing = FMath::FRandRange(MinHealing, MaxHealing);

	/*
	*	Grab the bonus healing.
	*/
	float BonusHealing = 0.f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(BonusHealingCaptureDefinition, EvaluationParameters, BonusHealing);

	/*
	*	Calculate the real damage by adding BaseDamage and BonusDamage and then multiplying it by the reduction caused by the armor.
	*/
	float FinalHealing = (BaseHealing + BonusHealing);

	/*
	*	Apply modifier to health.
	*/
	OutExecutionOutput.AddOutputModifier(FGameplayModifierEvaluatedData(GetAttributes().HealthProperty, EGameplayModOp::Additive, FinalHealing));
}


#if WITH_EDITORONLY_DATA
void UHealingEffectExecutionCalculation::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	//Update relevant attributes just incase something changed.
	UpdateRelevantCaptureAttributes();
}

void UHealingEffectExecutionCalculation::PostEditChangeChainProperty(FPropertyChangedChainEvent& PropertyChangedEvent)
{
	//Update relevant attributes just incase something changed.
	UpdateRelevantCaptureAttributes();
}
#endif