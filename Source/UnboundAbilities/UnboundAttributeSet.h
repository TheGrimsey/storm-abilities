// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AttributeSet.h"
#include "AbilitySystemComponent.h"

#include "UnboundAttributeSet.generated.h"

/*
*	Accessors Macro
*/
#define ATTRIBUTE_ACCESSORS(ClassName, PropertyName) \
	GAMEPLAYATTRIBUTE_PROPERTY_GETTER(ClassName, PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_GETTER(PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_SETTER(PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_INITTER(PropertyName)

/**
 * 
 */
UCLASS()
class UNBOUNDABILITIES_API UUnboundAttributeSet : public UAttributeSet
{
	friend struct UnboundAttributes;

	GENERATED_BODY()

	/*
	*	Variables
	*/
private:
	/*
	*	Attributes
	*/

	/** This measures how strong you are. Affects Melee power and MaxStamina. */
	UPROPERTY(EditAnywhere, ReplicatedUsing = OnRep_Strength, Category = "Attributes", meta=(AttributeStat, NotAttributeResource, NotAttributeResistance))
	FGameplayAttributeData Strength;
	
	/** This measures how intelligent you are. Affects spell power and MaxMana. */
	UPROPERTY(EditAnywhere, ReplicatedUsing = OnRep_Intellect, Category = "Attributes", meta = (AttributeStat, NotAttributeResource, NotAttributeResistance))
	FGameplayAttributeData Intellect;

	/** This measures how hardy you are. Increases health. */
	UPROPERTY(EditAnywhere, ReplicatedUsing = OnRep_Endurance, Category = "Attributes", meta = (AttributeStat, NotAttributeResource, NotAttributeResistance))
	FGameplayAttributeData Endurance;

	/** This measures how regenerative(???) you are. Increases health, mana and stamina regen. */
	UPROPERTY(EditAnywhere, ReplicatedUsing = OnRep_Vitality, Category = "Attributes", meta = (AttributeStat, NotAttributeResource, NotAttributeResistance))
	FGameplayAttributeData Vitality;

	/*
	*	Resistances
	*/
	
	UPROPERTY(EditAnywhere, ReplicatedUsing = OnRep_PhysicalResistance, Category = "Attributes", meta = (AttributeResistance, NotAttributeResource, NotAttributeStat))
	FGameplayAttributeData PhysicalResistance;
	
	UPROPERTY(EditAnywhere, ReplicatedUsing = OnRep_MagicResistance, Category = "Attributes", meta = (AttributeResistance, NotAttributeResource, NotAttributeStat))
	FGameplayAttributeData MagicResistance;

	UPROPERTY(EditAnywhere, ReplicatedUsing = OnRep_NatureResistance, Category = "Attributes", meta = (AttributeResistance, NotAttributeResource, NotAttributeStat))
	FGameplayAttributeData NatureResistance;

	UPROPERTY(EditAnywhere, ReplicatedUsing = OnRep_ElementalResistance, Category = "Attributes", meta = (AttributeResistance, NotAttributeResource, NotAttributeStat))
	FGameplayAttributeData ElementalResistance;

	/*
	*	Resources
	*/

	/** This measures the amount of damage that can be absorbed before dying. */
	UPROPERTY(EditAnywhere, ReplicatedUsing = OnRep_Health, Category = "Attributes", meta = (AttributeResource, NotAttributeStat, NotAttributeResistance))
	FGameplayAttributeData Health;
	
	/** This measures the max amount of damage that can be absorbed before dying. */
	UPROPERTY(EditAnywhere, ReplicatedUsing = OnRep_MaxHealth, Category = "Attributes", meta = (AttributeResource, NotAttributeStat, NotAttributeResistance))
	FGameplayAttributeData MaxHealth;

	/** This measures the stamina you have. */
	UPROPERTY(EditAnywhere, ReplicatedUsing = OnRep_Stamina, Category = "Attributes", meta = (AttributeResource, NotAttributeStat, NotAttributeResistance))
	FGameplayAttributeData Stamina;

	/** This measures the max amount of stamina that you can have. */
	UPROPERTY(EditAnywhere, ReplicatedUsing = OnRep_MaxStamina, Category = "Attributes", meta = (AttributeResource, NotAttributeStat, NotAttributeResistance))
	FGameplayAttributeData MaxStamina;

	/** This measures the mana you have. */
	UPROPERTY(EditAnywhere, ReplicatedUsing = OnRep_Mana, Category = "Attributes", meta = (AttributeResource, NotAttributeStat, NotAttributeResistance))
	FGameplayAttributeData Mana;

	/** This measures the max amount of mana that you can have. */
	UPROPERTY(EditAnywhere, ReplicatedUsing = OnRep_MaxMana, Category = "Attributes", meta = (AttributeResource, NotAttributeStat, NotAttributeResistance))
	FGameplayAttributeData MaxMana;

	/*
	*	Methods
	*/
public:
	UUnboundAttributeSet();

	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const override;

	virtual void PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue) override;

	virtual void PreAttributeBaseChange(const FGameplayAttribute& Attribute, float& NewValue) const override;

public:

	/*
	*	Attribute Accessor functions.
	*/

	ATTRIBUTE_ACCESSORS(UUnboundAttributeSet, Strength);

	ATTRIBUTE_ACCESSORS(UUnboundAttributeSet, Intellect);

	ATTRIBUTE_ACCESSORS(UUnboundAttributeSet, Endurance);

	ATTRIBUTE_ACCESSORS(UUnboundAttributeSet, Vitality);

	/*
	*	Resistance Accessor functions.
	*/

	ATTRIBUTE_ACCESSORS(UUnboundAttributeSet, PhysicalResistance);

	ATTRIBUTE_ACCESSORS(UUnboundAttributeSet, MagicResistance);

	ATTRIBUTE_ACCESSORS(UUnboundAttributeSet, NatureResistance);

	ATTRIBUTE_ACCESSORS(UUnboundAttributeSet, ElementalResistance);

	/*
	*	Resource Accessor functions.
	*/

	ATTRIBUTE_ACCESSORS(UUnboundAttributeSet, Health);
	ATTRIBUTE_ACCESSORS(UUnboundAttributeSet, MaxHealth);

	ATTRIBUTE_ACCESSORS(UUnboundAttributeSet, Stamina);
	ATTRIBUTE_ACCESSORS(UUnboundAttributeSet, MaxStamina);

	ATTRIBUTE_ACCESSORS(UUnboundAttributeSet, Mana);
	ATTRIBUTE_ACCESSORS(UUnboundAttributeSet, MaxMana);

private:

	/*
	*	Attributes OnRep functions.
	*/

	UFUNCTION()
	void OnRep_Strength();

	UFUNCTION()
	void OnRep_Intellect();

	UFUNCTION()
	void OnRep_Endurance();

	UFUNCTION()
	void OnRep_Vitality();

	/*
	*	Resistance OnRep functions.
	*/

	UFUNCTION()
	void OnRep_PhysicalResistance();

	UFUNCTION()
	void OnRep_MagicResistance();

	UFUNCTION()
	void OnRep_NatureResistance();

	UFUNCTION()
	void OnRep_ElementalResistance();

	/*
	*	Resources OnRep functions.
	*/

	UFUNCTION()
	void OnRep_Health();

	UFUNCTION()
	void OnRep_MaxHealth();

	UFUNCTION()
	void OnRep_Stamina();

	UFUNCTION()
	void OnRep_MaxStamina();

	UFUNCTION()
	void OnRep_Mana();

	UFUNCTION()
	void OnRep_MaxMana();

};
