// Fill out your copyright notice in the Description page of Project Settings.


#include "Unbound_WaitTargetData.h"

UUnbound_WaitTargetData* UUnbound_WaitTargetData::UnboundWaitTargetData(UGameplayAbility* OwningAbility, FName TaskInstanceName, TEnumAsByte<EGameplayTargetingConfirmation::Type> ConfirmationType, TSubclassOf<AGameplayAbilityTargetActor> TargetActorClass, AGameplayAbilityTargetActor* InTargetActor)
{
	UUnbound_WaitTargetData* MyObj = NewAbilityTask<UUnbound_WaitTargetData>(OwningAbility, TaskInstanceName);		//Register for task list here, providing a given FName as a key
	MyObj->TargetClass = TargetActorClass;
	MyObj->TargetActor = InTargetActor;
	MyObj->ConfirmationType = ConfirmationType;
	return MyObj;
}

void UUnbound_WaitTargetData::Activate()
{
	if (Ability)
	{
		if (TargetActor)
		{
			AGameplayAbilityTargetActor* SpawnedActor = TargetActor;
			TargetClass = SpawnedActor->GetClass();

			RegisterTargetDataCallbacks();


			if (IsPendingKill())
			{
				return;
			}

			const bool bIsLocallyControlled = Ability->GetCurrentActorInfo()->IsLocallyControlled();
			const bool ShouldSpawnOnServer = TargetActor->ShouldProduceTargetDataOnServer;

			if (bIsLocallyControlled || ShouldSpawnOnServer)
			{
				InitializeTargetActor(SpawnedActor);
				FinalizeTargetActor(SpawnedActor);

				// Note that the call to FinalizeTargetActor, this task could finish and our owning ability may be ended.
			}
			else
			{
				TargetActor = nullptr;

				// We may need a better solution here.  We don't know the target actor isn't needed till after it's already been spawned.
				SpawnedActor->Destroy();
				SpawnedActor = nullptr;
			}
		}
		else if (!ShouldSpawnTargetActor())
		{
			RegisterTargetDataCallbacks();
		}
		else
		{
			EndTask();
		}
	}
}