// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include <Runtime\Engine\Classes\Engine\EngineTypes.h>
#include "UnboundTargetingTypes.generated.h"

UENUM(BlueprintType)
enum class ETargetingType : uint8
{
	/* Targets the caster. */
	Self UMETA(DisplayName = "Self"),
	/* Targets one character. */
	SingleTarget,
	/* Targets characters around a point. */
	AoEAtPoint,
	/* Targets characters around the caster. Can only target Characters & Pawns (for performance reasons).*/
	AreaOfEffect,
	/* Targets character in a cone infront of the caster. */
	Cone
};

namespace Targeting
{
	const ECollisionChannel LineOfSightChannel = ECollisionChannel::ECC_GameTraceChannel12;
	const float AllowedError = 5.f;

	bool IsLocationWithinLineOfSight(class UWorld* InWorld, FVector InStartLocation, FVector InEndLocation, FHitResult& OutHitResult, class AActor* InIgnoreActor = nullptr, float InAllowedError = Targeting::AllowedError);
	inline bool IsLocationWithinLineOfSight(class UWorld* InWorld, FVector InStartLocation, FVector InEndLocation, class AActor* InIgnoreActor = nullptr, float InAllowedError = Targeting::AllowedError)
	{
		FHitResult hitResult;
		return Targeting::IsLocationWithinLineOfSight(InWorld, InStartLocation, InEndLocation, hitResult, InIgnoreActor, InAllowedError);
	}

	bool IsActorWithinLineOfSight(class UWorld* InWorld, FVector InStartLocation, class AActor* InTargetActor, FHitResult& InOutHitResult, class AActor* InIgnoreActor = nullptr);
	inline bool IsActorWithinLineOfSight(class UWorld* InWorld, FVector InStartLocation, class AActor* InTargetActor, class AActor* InIgnoreActor = nullptr)
	{
		FHitResult hitResult;
		return Targeting::IsActorWithinLineOfSight(InWorld, InStartLocation, InTargetActor, hitResult, InIgnoreActor);
	}
}