// Fill out your copyright notice in the Description page of Project Settings.


#include "UnboundTargetingTypes.h"
#include "Engine/World.h"

bool Targeting::IsLocationWithinLineOfSight(UWorld* InWorld, FVector InStartLocation, FVector InEndLocation, FHitResult& OutHitResult, class AActor* InIgnoreActor, float InAllowedError)
{
    TRACE_CPUPROFILER_EVENT_SCOPE("Targeting::IsLocationWithinLineOfSight");

    FCollisionQueryParams params;
    params.bTraceComplex = false;
    params.bReturnPhysicalMaterial = false;
    params.bReturnFaceIndex = false;
    params.AddIgnoredActor(InIgnoreActor);

    InWorld->LineTraceSingleByChannel(OutHitResult, InStartLocation, InEndLocation, LineOfSightChannel, params);

    return !OutHitResult.bBlockingHit || FVector::DistSquared(OutHitResult.Location, InEndLocation) <= AllowedError * AllowedError;
}

bool Targeting::IsActorWithinLineOfSight(UWorld* InWorld, FVector InStartLocation, AActor* InTargetActor, FHitResult& OutHitResult, AActor* InIgnoreActor)
{
    TRACE_CPUPROFILER_EVENT_SCOPE("Targeting::IsActorWithinLineOfSight");

    FCollisionQueryParams params;
    params.bTraceComplex = false;
    params.bReturnPhysicalMaterial = false;
    params.bReturnFaceIndex = false;
    params.AddIgnoredActor(InIgnoreActor);

    InWorld->LineTraceSingleByChannel(OutHitResult, InStartLocation, InTargetActor->GetActorLocation(), LineOfSightChannel, params);

    return InTargetActor == OutHitResult.Actor;
}
