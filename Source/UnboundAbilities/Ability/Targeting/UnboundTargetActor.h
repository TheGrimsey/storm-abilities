// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbilityTargetActor.h"
#include "Ability/Targeting/UnboundTargetingTypes.h"

#include "Engine/CollisionProfile.h"
#include "UnboundTargetActor.generated.h"

DECLARE_DELEGATE_OneParam(FAoETargetingTraceDelegate, const TArray<TWeakObjectPtr<AActor>>&);

/**
 * 
 */
UCLASS(NotBlueprintable)
class UNBOUNDABILITIES_API AUnboundTargetActor : public AGameplayAbilityTargetActor
{
	GENERATED_BODY()

	/*
	*	Variables
	*/
protected:
	UPROPERTY()
	ETargetingType TargetingType;

	UPROPERTY()
	float Range;

	UPROPERTY()
	FCollisionProfileName CollisionProfile;

	UPROPERTY()
	bool bTraceAffectsAimPitch;

	UPROPERTY()
	TSoftClassPtr<class AGameplayAbilityWorldReticle> SoftReticleClass;

	UPROPERTY()
	TWeakObjectPtr<class AGameplayAbilityWorldReticle> ReticleActor;

	/*
	*	Methods
	*/
public:
	AUnboundTargetActor(const FObjectInitializer& ObjectInitializer);

	static bool ShouldProduceTargetDataOnServer(ETargetingType TargetingType);

	virtual void Tick(float DeltaSeconds) override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	void SetUp(ETargetingType InTargetingType, float InRange, const FCollisionProfileName& InCollisionProfile, const TSoftClassPtr<class AGameplayAbilityWorldReticle>& InReticleClass, const FWorldReticleParameters& InReticleParams, const FGameplayAbilityTargetingLocationInfo& InStartLocation);

	virtual void StartTargeting(UGameplayAbility* InAbility) override;

	virtual void ConfirmTargetingAndContinue() override;

	void BroadcastTargetData();

	//Spawns the reticle actor when applicable.
	void SpawnReticleActor();

	//Returns true if we should spawn a reticle.
	bool ShouldSpawnReticle() const;

	//Do single target trace.
	static struct FHitResult DoSingleTrace(UGameplayAbility* OwningAbility, AActor* SourceActor, const FGameplayAbilityTargetingLocationInfo& StartLocation, float Range, const FCollisionProfileName& CollisionProfile, bool bDebug = false);

	//Do area trace around character.
	static void DoAreaTrace(FAoETargetingTraceDelegate ReturnDelegate, AActor* SourceActor, const FGameplayAbilityTargetingLocationInfo& StartLocation, float Range, bool bDebug = false);

	static void AimWithPlayerController(UGameplayAbility* OwningAbility, AActor* SourceActor, const FGameplayAbilityTargetingLocationInfo& StartLocation, float Range, const FCollisionProfileName& CollisionProfile, const FCollisionQueryParams& Params, const FVector& TraceStart, FVector& OutTraceEnd, bool bTraceAffectsAimPitch = true);

	//Clamps linetrace from camera to within ability range (from AvatarActor). Returns true if it was clipped.
	static bool ClipCameraRayToAbilityRange(const FGameplayAbilityTargetingLocationInfo& StartLocation, float Range, const FVector& CameraLocation, const FVector& CameraDirection, FVector& ClippedEnd);

	static void LineTrace(FHitResult& OutHitResult, const UWorld* World, const FVector& Start, const FVector& End, FName ProfileName, const FCollisionQueryParams Params);
};
