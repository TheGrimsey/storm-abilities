// Fill out your copyright notice in the Description page of Project Settings.


#include "UnboundTargetActor.h"

#include "Abilities/GameplayAbility.h"
#include "GameplayAbilityWorldReticle.h"

#include "GameFramework/PlayerController.h"
#include "Engine/AssetManager.h"
#include "Engine/StreamableManager.h"

#include "DrawDebugHelpers.h"

AUnboundTargetActor::AUnboundTargetActor(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	//Dont tick by default.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;
	PrimaryActorTick.TickGroup = TG_PostUpdateWork;

	SpawnCollisionHandlingMethod = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
}

bool AUnboundTargetActor::ShouldProduceTargetDataOnServer(ETargetingType TargetingType)
{
	switch (TargetingType)
	{
	case ETargetingType::AreaOfEffect:
	case ETargetingType::Cone:
		return true;
	}

	return false;
}

void AUnboundTargetActor::Tick(float DeltaSeconds)
{
	TRACE_CPUPROFILER_EVENT_SCOPE("AUnboundTargetActor::Tick")

	if (ReticleActor.IsValid())
	{
		switch (TargetingType)
		{
		case ETargetingType::SingleTarget:
		case ETargetingType::AoEAtPoint:
			{
				//Set actorlocation to hit location.
				const FHitResult HitResult = DoSingleTrace(OwningAbility, SourceActor, StartLocation, Range, CollisionProfile, bDebug);

				if (AGameplayAbilityWorldReticle* LocalReticleActor = ReticleActor.Get())
				{
					const bool bHitActor = (HitResult.bBlockingHit && (HitResult.Actor != NULL));
					const FVector ReticleLocation = (bHitActor && LocalReticleActor->bSnapToTargetedActor) ? HitResult.Actor->GetActorLocation() : HitResult.Location;

					LocalReticleActor->SetActorLocation(ReticleLocation);
					LocalReticleActor->SetIsTargetAnActor(bHitActor);
				}
				break;
			}
		default:
			//Just set reticle position to our position.
			ReticleActor.Get()->SetActorLocation(SourceActor->GetActorLocation());
			break;
		}
	}

	Super::Tick(DeltaSeconds);
}

void AUnboundTargetActor::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if (ReticleActor.IsValid())
	{
		ReticleActor.Get()->Destroy();
	}

	Super::EndPlay(EndPlayReason);
}

void AUnboundTargetActor::SetUp(ETargetingType InTargetingType, float InRange, const FCollisionProfileName& InCollisionProfile, const TSoftClassPtr<AGameplayAbilityWorldReticle>& InReticleClass, const FWorldReticleParameters& InReticleParams, const FGameplayAbilityTargetingLocationInfo& InStartLocation)
{
	TargetingType = InTargetingType;
	Range = InRange;
	CollisionProfile = InCollisionProfile;

	SoftReticleClass = InReticleClass;
	ReticleParams = InReticleParams;

	StartLocation = InStartLocation;
	
	Super::ShouldProduceTargetDataOnServer = ShouldProduceTargetDataOnServer(InTargetingType);

	TargetDataReadyDelegate.Clear();
}

void AUnboundTargetActor::StartTargeting(UGameplayAbility* InAbility)
{
	TRACE_CPUPROFILER_EVENT_SCOPE("AUnboundTargetActor::StartTargeting")

	Super::StartTargeting(InAbility); 
	SourceActor = InAbility->GetCurrentActorInfo()->AvatarActor.Get();

	/*
	*	Reticle Spawning.
	*/
	//Check if we should spawn a reticle.
	if (ShouldSpawnReticle())
	{
		PrimaryActorTick.SetTickFunctionEnable(true);

		//If we have the Reticle class in memory then let's spawn it.
		if (SoftReticleClass.IsValid())
		{
			/*
			*   Spawn Recticle Actor.
			*/
			SpawnReticleActor();
		}
		else if (!SoftReticleClass.IsNull()) //Make sure the soft pointer is actually pointing towards something.
		{
			//It isn't loaded so let's load it and spawn it.
			UAssetManager::GetStreamableManager().RequestAsyncLoad(SoftReticleClass.ToSoftObjectPath(), FStreamableDelegate::CreateUObject(this, &AUnboundTargetActor::SpawnReticleActor));
		}
	}
}

void AUnboundTargetActor::ConfirmTargetingAndContinue()
{
	check(ShouldProduceTargetData());
	if (SourceActor)
	{
		bDebug = false;
		
		BroadcastTargetData();
	}
}

void AUnboundTargetActor::BroadcastTargetData()
{
	TRACE_CPUPROFILER_EVENT_SCOPE("AUnboundTargetActor::MakeTargetData")

	switch (TargetingType)
	{
	case ETargetingType::SingleTarget:
		{
			FHitResult hitResult = DoSingleTrace(OwningAbility, SourceActor, StartLocation, Range, CollisionProfile, bDebug);
			TArray<TWeakObjectPtr<AActor>> SingleTraceActorArray;
			SingleTraceActorArray.Add(hitResult.Actor);

			TargetDataReadyDelegate.Broadcast(StartLocation.MakeTargetDataHandleFromActors(SingleTraceActorArray, false));
		}
	case ETargetingType::AoEAtPoint:
		{
			FHitResult hitResult = DoSingleTrace(OwningAbility, SourceActor, StartLocation, Range, CollisionProfile, bDebug);

			TargetDataReadyDelegate.Broadcast(StartLocation.MakeTargetDataHandleFromHitResult(OwningAbility, hitResult));
		}
	case ETargetingType::AreaOfEffect:
		{
			DoAreaTrace(FAoETargetingTraceDelegate::CreateLambda([this](const TArray<TWeakObjectPtr<AActor>>& Targets) {
				TargetDataReadyDelegate.Broadcast(StartLocation.MakeTargetDataHandleFromActors(Targets, false));
			}), SourceActor, StartLocation, Range, bDebug);
		}
	default: //Default is self targeting.
		{
			TArray<TWeakObjectPtr<AActor>> SourceActorArray;
			SourceActorArray.Add(SourceActor);

			TargetDataReadyDelegate.Broadcast(StartLocation.MakeTargetDataHandleFromActors(SourceActorArray, false));
		}
	}
}

void AUnboundTargetActor::SpawnReticleActor()
{
	TRACE_CPUPROFILER_EVENT_SCOPE("AUnboundTargetActor::SpawnReticleActor")

	if (SoftReticleClass.IsValid())
	{
		//Spawn a reticle actor with our softreticleclass.
		AGameplayAbilityWorldReticle* SpawnedReticleActor = GetWorld()->SpawnActor<AGameplayAbilityWorldReticle>(SoftReticleClass.Get(), GetActorLocation(), GetActorRotation());
		
		if (SpawnedReticleActor)
		{
			//Initialize Reticle.
			SpawnedReticleActor->InitializeReticle(this, MasterPC, ReticleParams);
			//Save it in the target actor.
			ReticleActor = SpawnedReticleActor;

			/*
			*   On Listen Servers make sure that reticles don't replicate to other players unless we should produce the target data on the server.
			*/
			if (!ShouldProduceTargetDataOnServer(TargetingType))
			{
				SpawnedReticleActor->SetReplicates(false);
			}
		}
	}
}

bool AUnboundTargetActor::ShouldSpawnReticle() const
{
	//If we are the owning player or we should produce target data on the server (AoE ability for example) then we should spawn a reticle. 
	//We don't need to spawn one for NPCs targeting a single player usually.
	//NOTE: In the future we may want to add a bool to the ability itself for if we should spawn the reticle on the server.
	return OwningAbility->GetCurrentActorInfo()->IsLocallyControlledPlayer() || ShouldProduceTargetDataOnServer(TargetingType);
}

FHitResult AUnboundTargetActor::DoSingleTrace(UGameplayAbility* OwningAbility, AActor* SourceActor, const FGameplayAbilityTargetingLocationInfo& StartLocation, float Range, const FCollisionProfileName& CollisionProfile, bool bDebug)
{
	bool bTraceComplex = false;

	FCollisionQueryParams Params(SCENE_QUERY_STAT(AUnboundTargetActor_DoSingleTrace), bTraceComplex);
	Params.bReturnPhysicalMaterial = true;
	Params.AddIgnoredActor(SourceActor);

	FVector TraceStart = StartLocation.GetTargetingTransform().GetLocation();
	FVector TraceEnd;
	AimWithPlayerController(OwningAbility, SourceActor, StartLocation, Range, CollisionProfile, Params, TraceStart, TraceEnd);		//Effective on server and launching client only

	// ------------------------------------------------------

	FHitResult ReturnHitResult;
	LineTrace(ReturnHitResult, SourceActor->GetWorld(), TraceStart, TraceEnd, CollisionProfile.Name, Params);
	//Default to end of trace line if we don't hit anything.
	if (!ReturnHitResult.bBlockingHit)
	{
		ReturnHitResult.Location = TraceEnd;
	}

#if ENABLE_DRAW_DEBUG
	if (bDebug)
	{
		DrawDebugLine(SourceActor->GetWorld(), TraceStart, TraceEnd, FColor::Green);
		DrawDebugSphere(SourceActor->GetWorld(), TraceEnd, 100.0f, 16, FColor::Green);
	}
#endif // ENABLE_DRAW_DEBUG
	return ReturnHitResult;
}

void AUnboundTargetActor::DoAreaTrace(FAoETargetingTraceDelegate ReturnDelegate, AActor* SourceActor, const FGameplayAbilityTargetingLocationInfo& StartLocation, float Range, bool bDebug)
{
	static auto OverlapLambda = [](const FTraceHandle& TraceHandle, FOverlapDatum& OverlapDatum, FAoETargetingTraceDelegate ReturnDelegate)
		{
			if (ReturnDelegate.IsBound())
			{
				TArray<TWeakObjectPtr<AActor>> OutActors;

				for (const FOverlapResult& Overlap : OverlapDatum.OutOverlaps)
				{
					AActor* overlapActor = Overlap.GetActor();
					if (overlapActor && !OutActors.Contains(overlapActor))
					{
						OutActors.Add(overlapActor);
					}
				}

				ReturnDelegate.Execute(OutActors);
			}
		};

	FCollisionQueryParams Params(SCENE_QUERY_STAT(DoAreaTrace));
	Params.bTraceComplex = false;
	Params.bReturnPhysicalMaterial = false;
 	Params.AddIgnoredActor(SourceActor);

	FOverlapDelegate Delegate = FOverlapDelegate::CreateStatic(OverlapLambda, ReturnDelegate); //This is legal because they FOverlapDatum does not keep a pointer but the function takes in a pointer. Really the function should take a reference.
	SourceActor->GetWorld()->AsyncOverlapByObjectType(StartLocation.GetTargetingTransform().GetLocation(), FQuat::Identity, FCollisionObjectQueryParams(ECollisionChannel::ECC_Pawn), FCollisionShape::MakeSphere(Range), Params, &Delegate);

#if ENABLE_DRAW_DEBUG
	if (bDebug)
	{
		DrawDebugSphere(SourceActor->GetWorld(), StartLocation.GetTargetingTransform().GetLocation(), Range, 3, FColor::White, false, 30.f, (uint8)'\000', 10.f);
	}
#endif
}

void AUnboundTargetActor::AimWithPlayerController(UGameplayAbility* OwningAbility, AActor* SourceActor, const FGameplayAbilityTargetingLocationInfo& StartLocation, float Range, const FCollisionProfileName& CollisionProfile, const FCollisionQueryParams& Params, const FVector& TraceStart, FVector& OutTraceEnd, bool bTraceAffectsAimPitch)
{
	if (!OwningAbility) // Server and launching client only
	{
		return;
	}

	APlayerController* PC = OwningAbility->GetCurrentActorInfo()->PlayerController.Get();
	check(PC);

	FVector ViewStart;
	FRotator ViewRot;
	PC->GetPlayerViewPoint(ViewStart, ViewRot);

	const FVector ViewDir = ViewRot.Vector();
	FVector ViewEnd = ViewStart + (ViewDir * Range);

	ClipCameraRayToAbilityRange(StartLocation, Range, ViewStart, ViewDir, ViewEnd);

	FHitResult HitResult;
	LineTrace(HitResult, SourceActor->GetWorld(), ViewStart, ViewEnd, CollisionProfile.Name, Params);

	const bool bUseTraceResult = HitResult.bBlockingHit && (FVector::DistSquared(TraceStart, HitResult.Location) <= (Range * Range));

	const FVector AdjustedEnd = (bUseTraceResult) ? HitResult.Location : ViewEnd;

	FVector AdjustedAimDir = (AdjustedEnd - TraceStart).GetSafeNormal();
	if (AdjustedAimDir.IsZero())
	{
		AdjustedAimDir = ViewDir;
	}

	if (!bTraceAffectsAimPitch && bUseTraceResult)
	{
		FVector OriginalAimDir = (ViewEnd - TraceStart).GetSafeNormal();

		if (!OriginalAimDir.IsZero())
		{
			// Convert to angles and use original pitch
			const FRotator OriginalAimRot = OriginalAimDir.Rotation();

			FRotator AdjustedAimRot = AdjustedAimDir.Rotation();
			AdjustedAimRot.Pitch = OriginalAimRot.Pitch;

			AdjustedAimDir = AdjustedAimRot.Vector();
		}
	}

	OutTraceEnd = TraceStart + (AdjustedAimDir * Range);

}

bool AUnboundTargetActor::ClipCameraRayToAbilityRange(const FGameplayAbilityTargetingLocationInfo& StartLocation, float Range, const FVector& CameraLocation, const FVector& CameraDirection, FVector& ClippedEnd)
{
	FVector CameraToCenter = StartLocation.GetTargetingTransform().GetLocation() - CameraLocation;
	float DotToCenter = FVector::DotProduct(CameraToCenter, CameraDirection);

	//If this fails, we're pointed away from the center, but we might be inside the sphere and able to find a good exit point.
	if (DotToCenter >= 0)
	{
		float DistanceSquared = CameraToCenter.SizeSquared() - (DotToCenter * DotToCenter);
		float RadiusSquared = (Range * Range);

		if (DistanceSquared <= RadiusSquared)
		{
			float DistanceFromCamera = FMath::Sqrt(RadiusSquared - DistanceSquared);
			float DistanceAlongRay = DotToCenter + DistanceFromCamera;						//Subtracting instead of adding will get the other intersection point
			ClippedEnd = CameraLocation + (DistanceAlongRay * CameraDirection);		//Cam aim point clipped to range sphere
			return true;
		}
	}
	return false;
}

void AUnboundTargetActor::LineTrace(FHitResult& OutHitResult, const UWorld* World, const FVector& Start, const FVector& End, FName ProfileName, const FCollisionQueryParams Params)
{
	check(World);

	FHitResult HitResult;
	World->LineTraceSingleByProfile(HitResult, Start, End, ProfileName, Params);

	OutHitResult.TraceStart = Start;
	OutHitResult.TraceEnd = End;

	if (HitResult.Actor.IsValid())
	{
		OutHitResult = HitResult;
		OutHitResult.bBlockingHit = true; // treat it as a blocking hit
		return;
	}
}
