// Fill out your copyright notice in the Description page of Project Settings.


#include "UnboundAbilitySystemComponent.h"

#include "Ability/UnboundGameplayAbility.h"

UUnboundAbilitySystemComponent::UUnboundAbilitySystemComponent()
{
	SetIsReplicatedByDefault(true);
}

void UUnboundAbilitySystemComponent::TeachAbility(TSubclassOf<UUnboundGameplayAbility> Ability, int32 Level, bool bAllowLevelDowngrade, UObject* SourceObject)
{
	if (!IsOwnerActorAuthoritative()) return;

	FGameplayAbilitySpec* ExistingAbilitySpec = FindAbilitySpecFromClass(Ability);
	if (ExistingAbilitySpec)
	{
		//Check if we have an Ability of lower level and if so remove it OR If bAllowLevelDowngrade remove it ignore the level and remove it anyway.
		if (bAllowLevelDowngrade || ExistingAbilitySpec->Level < Level)
		{
			//Remove the old ability.
			ClearAbility(ExistingAbilitySpec->Handle);
		}
		else
		{
			//We already have an ability with a higher level and we dont want to force downrank it so we don't do anything. 
			return;
		}
	}

	GiveAbility(FGameplayAbilitySpec(Ability, Level, -1, SourceObject));
}

void UUnboundAbilitySystemComponent::OnAbilityStartedCasting(const FGameplayAbilitySpecHandle& CastingAbility, TSubclassOf<class UUnboundGameplayAbility> Ability, float CastStartTime, float CastTime)
{
	FAbilityCastInfo CastInfo = FAbilityCastInfo(CastingAbility, Ability, this, CastStartTime, CastTime);

	CurrentlyCastingAbilities.Add(CastInfo);

	OnAbilityStartedCastingDelegate.Broadcast(CastInfo);
}

void UUnboundAbilitySystemComponent::OnAbilityStoppedCasting(const FGameplayAbilitySpecHandle& CastingAbility)
{
	CurrentlyCastingAbilities.Remove(FAbilityCastInfo(CastingAbility));

	OnAbilityStoppedCastingDelegate.Broadcast(CastingAbility);
}
