// Fill out your copyright notice in the Description page of Project Settings.


#include "UnboundGameplayAbility.h"
#include "GameplayAbilityTargetTypes.h"

#include "UnboundAbilities.h"
#include "UnboundAbilitySystemComponent.h"
#include "Ability/Unbound_WaitTargetData.h"
#include "Targeting/UnboundTargetActor.h"

#include "Projectiles/ProjectileManagerSubsystem.h"

#include "AssertionMacros.h"
#include "TimerManager.h"


UUnboundGameplayAbility::UUnboundGameplayAbility() : UGameplayAbility()
{
	ReplicationPolicy = EGameplayAbilityReplicationPolicy::ReplicateYes;
	InstancingPolicy = EGameplayAbilityInstancingPolicy::InstancedPerActor;

	bServerRespectsRemoteAbilityCancellation = true;
}

bool UUnboundGameplayAbility::ShouldSpawnTargetingActor() const
{
	return IsLocallyControlled() || AUnboundTargetActor::ShouldProduceTargetDataOnServer(TargetingType);
}

bool UUnboundGameplayAbility::HasRecievedTargetData() const
{
	return TargetingType == ETargetingType::Self || RawTargetData.Num() > 0 || TargetData.Num() > 0;
}

#if WITH_EDITORONLY_DATA
void UUnboundGameplayAbility::PreSave(const ITargetPlatform* TargetPlatform)
{
	if (!GUID.IsValid())
	{
		GUID = FGuid::NewGuid();
	}
}
#endif

void UUnboundGameplayAbility::ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
	TRACE_CPUPROFILER_EVENT_SCOPE("UUnboundGameplayAbility::ActivateAbility")
	
	//Save EventData if it is exists.
	if (TriggerEventData)
	{
		CurrentEventData = *TriggerEventData;
	}
	
	//Set up targeting callbacks & actor.
	SetUpTargeting();

	// If we have a CastTime then set a timer for that.
	if (bHasCastTime)
	{
		SetIsCasting(true);

		// Start CastTimer.
		ActorInfo->OwnerActor.Get()->GetWorld()->GetTimerManager().SetTimer(CastTimer, FTimerDelegate::CreateUObject(this, &UUnboundGameplayAbility::OnCastFinished), GetCastTime(), false);

		// Register local cancel callbacks for players.
		if (ActorInfo->IsLocallyControlledPlayer())
		{
			GetAbilitySystemComponentFromActorInfo()->GenericLocalCancelCallbacks.AddDynamic(this, &UUnboundGameplayAbility::OnLocalCastCancelled);
		}
	}
	else
	{
		OnCastFinished();
	}
}

void UUnboundGameplayAbility::SetUpTargeting()
{
	//If we aren't self targeting & instanced.
	if (TargetingType != ETargetingType::Self && GetInstancingPolicy() != EGameplayAbilityInstancingPolicy::NonInstanced)
	{
		if (ShouldSpawnTargetingActor())
		{
			//Spawn targeting actor. IF we are locally controlled (client ability on server) or should gather target data on server.
			AGameplayAbilityTargetActor* TargetingActor = SpawnTargetingActor();

			// Create WaitForTargetData Task.
			TargetDataTask = UUnbound_WaitTargetData::UnboundWaitTargetData(this, GetFName(), EGameplayTargetingConfirmation::Custom, AUnboundTargetActor::StaticClass(), TargetingActor);
			TargetDataTask->ValidData.AddDynamic(this, &UUnboundGameplayAbility::OnTargetConfirmed);
			TargetDataTask->Activate();
		}
		else
		{
			//Hook up the callbacks.
			GetAbilitySystemComponentFromActorInfo()->AbilityTargetDataSetDelegate(GetCurrentAbilitySpecHandle(), GetCurrentActivationInfo().GetActivationPredictionKey()).AddUObject(this, &UUnboundGameplayAbility::OnTargetReplicated);

			GetAbilitySystemComponentFromActorInfo()->CallReplicatedTargetDataDelegatesIfSet(GetCurrentAbilitySpecHandle(), GetCurrentActivationInfo().GetActivationPredictionKey());
		}
	}
}

AGameplayAbilityTargetActor* UUnboundGameplayAbility::SpawnTargetingActor()
{
	TRACE_CPUPROFILER_EVENT_SCOPE("UUnboundGameplayAbility::SetUpTargetingActor")

	//Set StartLocation
	FGameplayAbilityTargetingLocationInfo StartLocation = FGameplayAbilityTargetingLocationInfo();
	StartLocation.LocationType = EGameplayAbilityTargetingLocationType::ActorTransform;
	StartLocation.SourceActor = GetAvatarActorFromActorInfo();
	StartLocation.SourceAbility = this;
	
	//Create filter.
	FGameplayTargetDataFilterHandle FilterHandle = FGameplayTargetDataFilterHandle();
	FGameplayTargetDataFilter* NewFilter = new FGameplayTargetDataFilter(TargetingFilter);
	NewFilter->InitializeFilterContext(GetCurrentActorInfo()->AvatarActor.Get());
	FilterHandle.Filter = TSharedPtr<FGameplayTargetDataFilter>(NewFilter);

	//Spawn Actor.
	AUnboundTargetActor* TargetingActor = GetWorld()->SpawnActor<AUnboundTargetActor>(AUnboundTargetActor::StaticClass(), GetAvatarActorFromActorInfo()->GetTransform());
	TargetingActor->SetUp(TargetingType, GetMaxRange(), TargetingTraceProfile, TargetingReticleClass, FWorldReticleParameters(), StartLocation);

	return TargetingActor;
}

void UUnboundGameplayAbility::EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled)
{
	TRACE_CPUPROFILER_EVENT_SCOPE("UUnboundGameplayAbility::EndAbility")

	//Only replicate end if we are the server or cancelling.
	bReplicateEndAbility = bWasCancelled || HasAuthority(&ActivationInfo);

#if UE_BUILD_DEBUG
	FString EndState = bWasCancelled ? TEXT("cancelled") : TEXT("ended");
	FString AuthorityState = HasAuthority(&CurrentActivationInfo) ? TEXT("Server") : TEXT("Client");
	UE_LOG(LogAbilitySystem, Warning, TEXT("Ability %s on %s"), *EndState, *AuthorityState);
#endif

	//Handle removing the cancel callbacks.
	if (ActorInfo->IsLocallyControlled())
	{
		ActorInfo->AbilitySystemComponent->GenericLocalCancelCallbacks.RemoveAll(this);
	}

	//Handle ability cancellation.
	if (bWasCancelled)
	{
		//Clear the cast timer.
		ActorInfo->OwnerActor.Get()->GetWorld()->GetTimerManager().ClearTimer(CastTimer);

		//Make sure we stop the casting even when cancelled.
		SetIsCasting(false);

		if (TargetingType != ETargetingType::Self)
		{
			//Make sure we consumed targetting events.
			ActorInfo->AbilitySystemComponent->CancelAbilityTargetData(Handle, ActivationInfo.GetActivationPredictionKey());
		}
	}

	//Clean up TargetData task
	if (TargetDataTask)
	{
		TargetDataTask->EndTask();
		TargetDataTask = nullptr;
	}

	//Clean up RawTargetData.
	RawTargetData.Clear();

	//Clean up TargetData.
	TargetData.Clear();

	//Reset event data.
	CurrentEventData = FGameplayEventData();

	Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);
}

void UUnboundGameplayAbility::FinalizeTargeting()
{

	FGameplayAbilityTargetingLocationInfo StartLocation;
	StartLocation.LocationType = EGameplayAbilityTargetingLocationType::ActorTransform;
	StartLocation.SourceActor = GetAvatarActorFromActorInfo();
	StartLocation.SourceAbility = this;

	switch (TargetingType)
	{
	case ETargetingType::Self:
		{
			ExecuteAbilityLogic();

			return;
		}	
	case ETargetingType::AoEAtPoint:
		if (FGameplayAbilityTargetData* pointData = RawTargetData.Get(0))
		{
			FVector StartLocationVector = StartLocation.GetTargetingTransform().GetLocation();

			const FHitResult* hitResult = pointData->GetHitResult();
			if (hitResult && IsLocationInRangeOfStartLocation(StartLocationVector, hitResult->Location))
			{
				FHitResult NewHitResult;
				if (Targeting::IsLocationWithinLineOfSight(GetWorld(), StartLocationVector, hitResult->Location, NewHitResult, StartLocation.SourceActor))
				{
					StartLocation.LocationType = EGameplayAbilityTargetingLocationType::LiteralTransform;
					StartLocation.LiteralTransform = FTransform(NewHitResult.Location);

#if !WITH_EDITORONLY_DATA
					const bool bDebugTargeting = false;
#endif
					
					AUnboundTargetActor::DoAreaTrace(FAoETargetingTraceDelegate::CreateLambda([this, StartLocation](const TArray<TWeakObjectPtr<AActor>> Targets)
						{
							RawTargetData = StartLocation.MakeTargetDataHandleFromActors(Targets, false);

							if (FilterTargets(StartLocation, RawTargetData, TargetData))
							{
								ExecuteAbilityLogic();
							}
							else
							{
								CancelAbility(GetCurrentAbilitySpecHandle(), GetCurrentActorInfo(), GetCurrentActivationInfo(), true);
							}

						}), GetAvatarActorFromActorInfo(), StartLocation, GetMaxRange(), bDebugTargeting);
					return;
				}
			}
		}
		break;
	default:
		if (FilterTargets(StartLocation, RawTargetData, TargetData))
		{
			ExecuteAbilityLogic();
			return;
		}
		break;
	}

	CancelAbility(GetCurrentAbilitySpecHandle(), GetCurrentActorInfo(), GetCurrentActivationInfo(), true);
}

void UUnboundGameplayAbility::ExecuteAbilityLogic()
{	
	if (bHasBlueprintActivate)
	{
		// A Blueprinted ActivateAbility function must call CommitAbility somewhere in its execution chain.
		K2_ActivateAbility();
	}
	else if (bHasBlueprintActivateFromEvent)
	{
		if (CurrentEventData.EventTag.IsValid())
		{
			// A Blueprinted ActivateAbility function must call CommitAbility somewhere in its execution chain.
			K2_ActivateAbilityFromEvent(CurrentEventData);
		}
		else
		{
			UE_LOG(LogUnboundAbilities, Warning, TEXT("Ability %s expects event data but none is being supplied. Use Activate Ability instead of Activate Ability From Event."), *GetName());
			bool bReplicateEndAbility = false;
			bool bWasCancelled = false;
			EndAbility(GetCurrentAbilitySpecHandle(), GetCurrentActorInfo(), GetCurrentActivationInfo(), bReplicateEndAbility, bWasCancelled);
		}
	}
	else
	{
		UE_LOG(LogUnboundAbilities, Warning, TEXT("No logic has been implemented for ability %s."), *GetName());
		bool bReplicateEndAbility = false;
		bool bWasCancelled = false;
		EndAbility(GetCurrentAbilitySpecHandle(), GetCurrentActorInfo(), GetCurrentActivationInfo(), bReplicateEndAbility, bWasCancelled);
	}
}

void UUnboundGameplayAbility::OnLocalCastCancelled()
{
	FScopedPredictionWindow ScopedPrediction(GetAbilitySystemComponentFromActorInfo(), IsPredictingClient());

	//Send cancel event to server if we are the predicting client.
	if (GetAbilitySystemComponentFromActorInfo() && IsPredictingClient())
	{
		GetAbilitySystemComponentFromActorInfo()->ServerSetReplicatedEvent(EAbilityGenericReplicatedEvent::GenericCancel, GetCurrentAbilitySpecHandle(), GetCurrentActivationInfo().GetActivationPredictionKey(), GetAbilitySystemComponentFromActorInfo()->ScopedPredictionKey);
	}

	//Cancel the ability.
	CancelAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true);
}

void UUnboundGameplayAbility::OnCastFinished()
{
	SetIsCasting(false);

	/*If we are in control then confirm targeting. */
	if (TargetingType != ETargetingType::Self && (IsLocallyControlled() || AUnboundTargetActor::ShouldProduceTargetDataOnServer(TargetingType)))
	{
		TargetDataTask->ExternalConfirm(true);
	}
	else
	{
		//If we have already recieved the targetdata or aren't looking for a target then finalize targeting.
		if (HasRecievedTargetData())
		{
			FinalizeTargeting();
		}

		// TODO Check so we're not waiting for targeting forever and cancel if we are.
	}
}

void UUnboundGameplayAbility::SetIsCasting(bool IsCasting)
{
	//Only do this if things have actually changed.
	if (bIsCasting != IsCasting)
	{
		bIsCasting = IsCasting;

		if (UUnboundAbilitySystemComponent* UnboundAbilitySysComp = Cast<UUnboundAbilitySystemComponent>(GetAbilitySystemComponentFromActorInfo()))
		{
			if (IsCasting)
			{
				UnboundAbilitySysComp->OnAbilityStartedCasting(GetCurrentAbilitySpecHandle(), GetClass(), GetWorld()->GetTimeSeconds(), GetCastTime());
			}
			else
			{
				UnboundAbilitySysComp->OnAbilityStoppedCasting(GetCurrentAbilitySpecHandle());
			}
		}
	}
}

void UUnboundGameplayAbility::OnTargetReplicated(const FGameplayAbilityTargetDataHandle& Data, FGameplayTag ActivationTag)
{
	GetAbilitySystemComponentFromActorInfo()->ConsumeClientReplicatedTargetData(GetCurrentAbilitySpecHandle(), GetCurrentActivationInfo().GetActivationPredictionKey());

	if (IsActive())
	{
		OnTargetConfirmed(Data);
	}
}

void UUnboundGameplayAbility::OnTargetConfirmed(const FGameplayAbilityTargetDataHandle& Data)
{
	UE_LOG(LogUnboundAbilities, Warning, TEXT("Targeting confirmed on %s!"), (HasAuthority(&CurrentActivationInfo) ? TEXT("Authority") : TEXT("Non Authority")));

	RawTargetData = Data;

	//Check if we have already finished casting if so then finalize targeting.
	if (!IsCasting())
	{
		FinalizeTargeting();
	}
}

bool UUnboundGameplayAbility::FilterTargets(const FGameplayAbilityTargetingLocationInfo& StartLocation, const FGameplayAbilityTargetDataHandle& InTargetData, FGameplayAbilityTargetDataHandle& OutTargetData)
{
	TRACE_CPUPROFILER_EVENT_SCOPE("UUnboundGameplayAbility::RemoveInvalidTargets");

	//Make sure OutTargetData is empty.
	OutTargetData.Clear();

	TArray<TWeakObjectPtr<AActor>> validTargetActors;
	validTargetActors.Reserve(MinTargets); // Reserve space for MinTargets since we will always need that amount. Hopefully saves us allocations.
	
	FVector StartLocationVector = StartLocation.GetTargetingTransform().GetLocation();

	//Loop through all TargetData.
	for (const TSharedPtr<FGameplayAbilityTargetData>& SharedTargetDataPtr : InTargetData.Data)
	{
		if (SharedTargetDataPtr.IsValid())
		{
			for (TWeakObjectPtr<AActor> targetActor : SharedTargetDataPtr->GetActors())
			{
				if (!targetActor.IsValid())
					continue;
				
				if (!IsLocationInRangeOfStartLocation(StartLocationVector, targetActor->GetActorLocation()))
					continue;

				if (!TargetingFilter.FilterPassesForActor(targetActor.Get()))
					continue;

				if (!Targeting::IsActorWithinLineOfSight(GetWorld(), StartLocationVector, targetActor.Get(), GetAvatarActorFromActorInfo()))
					continue;

				validTargetActors.Add(targetActor);

				//Exit if we have enough targets.
				if (validTargetActors.Num() >= GetMaxTargets())
					break;
			}
		}

		//Exit if we have enough targets
		if (validTargetActors.Num() >= GetMaxTargets())
			break;
	}

	FGameplayAbilityTargetData_ActorArray* ReturnData = new FGameplayAbilityTargetData_ActorArray();
	ReturnData->TargetActorArray = validTargetActors;
	OutTargetData.Add(ReturnData);

	const bool bFoundEnoughTargets = validTargetActors.Num() >= GetMinTargets();

	return bFoundEnoughTargets;
}

void UUnboundGameplayAbility::SpawnProjectiles(const FGameplayAbilityTargetDataHandle& Targets, UProjectileDataAsset* InProjectileData, const FVector& InSpawnPosition, const FRotator& InSpawnRotation, const FGameplayEffectSpecHandle EffectHandle, TArray<class AProjectile*>& OutProjectiles, bool bCaptureAttributesDirectly)
{
	check(EffectHandle.Data.IsValid());

	if (!HasAuthority(&CurrentActivationInfo))
		return;

	UProjectileManagerSubsystem* ProjectileManager = GetWorld()->GetGameInstance()->GetSubsystem<UProjectileManagerSubsystem>();
	ProjectileManager->SpawnProjectiles(Targets, InProjectileData, InSpawnPosition, InSpawnRotation, EffectHandle, OutProjectiles, bCaptureAttributesDirectly);
}
