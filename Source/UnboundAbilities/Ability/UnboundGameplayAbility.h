// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Abilities/GameplayAbility.h"
#include "Abilities/GameplayAbilityTargetDataFilter.h"
#include "Engine/CollisionProfile.h"

#include "UnboundAbilities.h"
#include "Targeting/UnboundTargetingTypes.h"

#include "UnboundGameplayAbility.generated.h"

/**
 * 
 */
UCLASS()
class UNBOUNDABILITIES_API UUnboundGameplayAbility : public UGameplayAbility
{
	GENERATED_BODY()

	/*
	*	Variables
	*/
protected:
	UPROPERTY(VisibleAnywhere, AdvancedDisplay, AssetRegistrySearchable)
	FGuid GUID;

	//Data used by the UI when displaying the Ability.
	UPROPERTY(EditDefaultsOnly, Category = "Display", Instanced)
	class UUnboundGASUIData* AbilityUIData;

	/*
	*	Casting
	*/
	UPROPERTY(EditDefaultsOnly, Category = "Casting")
	bool bHasCastTime = true;

	//The time it takes to activate this ability.
	UPROPERTY(EditDefaultsOnly, Category = "Casting", meta = (ClampMin = "0.1", EditCondition = bHasCastTime))
	float CastTime = 1.f;

	/*
	*	Targeting
	*/
	UPROPERTY(EditDefaultsOnly, Category = "Targeting")
	ETargetingType TargetingType;

	// The Minimum Range of the Ability in Unreal Units (100  = 1m). If the distance to the target is below this it won't activate.
	UPROPERTY(EditDefaultsOnly, Category = "Targeting", meta = (ClampMin = "0.0", EditCondition = "TargetingType != ETargetingType::Self"))
	float MinRange = 0.f;

	// The Max Range of the Ability in Unreal Units (100 = 1m)
	UPROPERTY(EditDefaultsOnly, Category = "Targeting", meta = (ClampMin = "0.0", EditCondition = "TargetingType != ETargetingType::Self"))
	float MaxRange = 0.f;

	//The minimum amount of targets this ability needs to execute. Any less than this and it will cancel.
	UPROPERTY(EditDefaultsOnly, Category = "Targeting", meta = (ClampMin = "1", EditCondition = "TargetingType != ETargetingType::Self"))
	uint8 MinTargets = 1;

	//The maximum amount of targets this ability accepts for executing. Any more will be ignored.
	UPROPERTY(EditDefaultsOnly, Category = "Targeting", meta = (ClampMin = "1", EditCondition = "TargetingType != ETargetingType::Self && TargetingType != ETargetingType::SingleTarget"))
	uint8 MaxTargets = 1;

	/* Targeting */
	UPROPERTY(EditDefaultsOnly, Category = "Targeting", meta = (EditCondition = "TargetingType != ETargetingType::Self"))
	TSoftClassPtr<class AGameplayAbilityWorldReticle> TargetingReticleClass;

	UPROPERTY(EditDefaultsOnly, Category = "Targeting", meta = (EditCondition = "TargetingType != ETargetingType::Self"))
	FCollisionProfileName TargetingTraceProfile;

	UPROPERTY(EditDefaultsOnly, Category = "Targeting", meta = (EditCondition = "TargetingType != ETargetingType::Self"))
	FGameplayTargetDataFilter TargetingFilter;

#if WITH_EDITORONLY_DATA
	UPROPERTY(EditDefaultsOnly, Category = "Targeting", meta = (EditCondition = "TargetingType != ETargetingType::Self"), AdvancedDisplay)
	bool bDebugTargeting;
#endif

	/*
	* Per-Instance Variables
	*/

	//Timer for the Cast.
	UPROPERTY()
	struct FTimerHandle CastTimer;

	UPROPERTY()
	bool bIsCasting;

	//WaitTargetData task that is used for targeting.
	UPROPERTY()
	class UUnbound_WaitTargetData* TargetDataTask;


	//Unfiltered TargetData. These are the recieved targets.
	UPROPERTY()
	struct FGameplayAbilityTargetDataHandle RawTargetData;

	//Filtered TargetData. These are our targets.
	UPROPERTY()
	struct FGameplayAbilityTargetDataHandle TargetData;

	/*
	*	Methods
	*/
public:
	UUnboundGameplayAbility();

	//Returns the UIData for this Ability.
	UFUNCTION(BlueprintPure)
	FORCEINLINE class UUnboundGASUIData* GetUIData() const
	{
		return AbilityUIData;
	}

	//Returns the CastTime of this ability.
	UFUNCTION(BlueprintPure)
	FORCEINLINE float GetCastTime() const
	{
		return CastTime;
	}

	//Returns the Ability's minimum range.
	UFUNCTION(BlueprintPure)
	FORCEINLINE float GetMinRange() const
	{
		return MinRange;
	}

	//Returns the Ability's max range.
	UFUNCTION(BlueprintPure)
	FORCEINLINE float GetMaxRange() const
	{
		return MaxRange;
	}

	UFUNCTION(BlueprintPure)
	FORCEINLINE int GetMinTargets() const
	{
		return TargetingType != ETargetingType::Self ? MinTargets : 0;
	}

	UFUNCTION(BlueprintPure)
	FORCEINLINE int GetMaxTargets() const
	{
		return TargetingType > ETargetingType::SingleTarget ? MaxTargets : 1;
	}

	UFUNCTION(BlueprintPure)
	FORCEINLINE bool IsCasting() const
	{
		return bIsCasting;
	}
	
	//Returns our gathered target data.
	UFUNCTION(BlueprintPure)
	FORCEINLINE FGameplayAbilityTargetDataHandle GetTargetData() const
	{
		return TargetData;
	}

	//Returns true if the other actor is in range of startlocation. Only works in instanced abilities.
	UFUNCTION(BlueprintCallable)
	FORCEINLINE bool IsLocationInRangeOfStartLocation(const FVector& StartLocation, const FVector& OtherLocation) const
	{
		float SquaredDistance = FVector::DistSquared(StartLocation, OtherLocation);

		return SquaredDistance <= (GetMaxRange() * GetMaxRange()) && SquaredDistance >= (GetMinRange() * GetMinRange());
	}

	bool ShouldSpawnTargetingActor() const;

	bool HasRecievedTargetData() const;

#if WITH_EDITORONLY_DATA
	virtual void PreSave(const class ITargetPlatform* TargetPlatform) override;
#endif
protected:
	virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;

    void SetUpTargeting();

	virtual void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled) override;

	class AGameplayAbilityTargetActor* SpawnTargetingActor();


	/*
	*	Calls blueprint Ability logic.
	*/
    void FinalizeTargeting();
    void ExecuteAbilityLogic();

	/*
	*	Called on the local player when they cancel the Ability cast.
	*/
	UFUNCTION()
	void OnLocalCastCancelled();

	/*
	*	Called when our cast has finished (or instantly if we have no CastTime)
	*/
	void OnCastFinished();

	void SetIsCasting(bool IsCasting);

	//Called when Target data has been replicated from client.
	void OnTargetReplicated(const FGameplayAbilityTargetDataHandle& Data, FGameplayTag ActivationTag);

	//Called when the Target has been confirmed. (On either client or server)
	UFUNCTION()
	void OnTargetConfirmed(const FGameplayAbilityTargetDataHandle& Data);

	//Filter out invalid targets from TargetData and put them into NewTargetData.
	UFUNCTION()
	bool FilterTargets(const FGameplayAbilityTargetingLocationInfo& StartLocation, const FGameplayAbilityTargetDataHandle& InTargetData, FGameplayAbilityTargetDataHandle& OutTargetData);

	UFUNCTION(BlueprintCallable)
	void SpawnProjectiles(const FGameplayAbilityTargetDataHandle& Targets, class UProjectileDataAsset* InProjectileData, const FVector& InSpawnPosition, const FRotator& InSpawnRotation, const FGameplayEffectSpecHandle EffectHandle, TArray<class AProjectile*>& OutProjectiles, bool bCaptureAttributesDirectly = true);
};
