// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/Tasks/AbilityTask_WaitTargetData.h"
#include "Unbound_WaitTargetData.generated.h"

/**
 * 
 */
UCLASS()
class UNBOUNDABILITIES_API UUnbound_WaitTargetData : public UAbilityTask_WaitTargetData
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable, meta = (HidePin = "OwningAbility", DefaultToSelf = "OwningAbility", BlueprintInternalUseOnly = "true", HideSpawnParms = "Instigator"), Category = "Ability|Tasks")
	static UUnbound_WaitTargetData* UnboundWaitTargetData(UGameplayAbility* OwningAbility, FName TaskInstanceName, TEnumAsByte<EGameplayTargetingConfirmation::Type> ConfirmationType, TSubclassOf<AGameplayAbilityTargetActor> TargetActorClass, AGameplayAbilityTargetActor* InTargetActor);

	virtual void Activate() override;

};
