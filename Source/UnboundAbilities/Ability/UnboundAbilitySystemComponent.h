// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemComponent.h"
#include "UnboundAbilitySystemComponent.generated.h"

USTRUCT(BlueprintType)
struct FAbilityCastInfo
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, NotReplicated)
	FGameplayAbilitySpecHandle CastingAbility;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	TSubclassOf<class UUnboundGameplayAbility> AbilityClass;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	class UUnboundAbilitySystemComponent* AbilitySystem;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	float CastStartTime;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	float CastTime;

	FAbilityCastInfo(FGameplayAbilitySpecHandle InCastingAbility = FGameplayAbilitySpecHandle(), TSubclassOf<class UUnboundGameplayAbility> InAbilityClass = nullptr, class UUnboundAbilitySystemComponent* InAbilitySystem = nullptr, float InCastStartTime = 0.f, float InCastTime = 0.f)
	{
		CastingAbility = InCastingAbility;
		AbilityClass = InAbilityClass;
		AbilitySystem = InAbilitySystem;
		CastStartTime = InCastStartTime;
		CastTime = InCastTime;
	}

	bool operator==(const FAbilityCastInfo& Other) const
	{
		return CastingAbility == Other.CastingAbility;
	}

	bool operator!=(const FAbilityCastInfo& Other) const
	{
		return CastingAbility != Other.CastingAbility;
	}
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAbilityCastingDelegate, const FAbilityCastInfo, CastInfo);

/**
 * 
 */
UCLASS()
class UNBOUNDABILITIES_API UUnboundAbilitySystemComponent : public UAbilitySystemComponent
{
	friend class UUnboundGameplayAbility;

	GENERATED_BODY()

	/*
	*	Variables
	*/
public:
	UPROPERTY(BlueprintAssignable, meta = (DisplayName="OnAbilityStartedCasting"))
	FOnAbilityCastingDelegate OnAbilityStartedCastingDelegate;
	
	UPROPERTY(BlueprintAssignable, meta = (DisplayName = "OnAbilityStoppedCasting"))
	FOnAbilityCastingDelegate OnAbilityStoppedCastingDelegate;

	UPROPERTY(BlueprintReadOnly)
	TArray<FAbilityCastInfo> CurrentlyCastingAbilities;

	/*
	*	Methods
	*/
public:
	UUnboundAbilitySystemComponent();

	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly)
	/**
	* Teach the character an ability of input level.
	* @Param Ability The ability to teach.
	* @Param Level The level of the Ability to teach.
	* @Param bAllowLevelDowngrade If true we skip checking if the new ability level is lower. This allows 'downgrading' of Abilities.
	* @Param SourceObject [OPTIONAL] The Object that granted this Ability.
	*/
	void TeachAbility(TSubclassOf<class UUnboundGameplayAbility> Ability, int32 Level = 1, bool bAllowLevelDowngrade = false, UObject * SourceObject = nullptr);

protected:
	void OnAbilityStartedCasting(const FGameplayAbilitySpecHandle& CastingAbility, TSubclassOf<class UUnboundGameplayAbility> Ability, float CastStartTime, float CastTime);

	void OnAbilityStoppedCasting(const FGameplayAbilitySpecHandle& CastingAbility);
};
