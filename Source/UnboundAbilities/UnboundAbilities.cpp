// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "UnboundAbilities.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_GAME_MODULE( FDefaultGameModuleImpl, UnboundAbilities);

DEFINE_LOG_CATEGORY(LogUnboundAbilities);
