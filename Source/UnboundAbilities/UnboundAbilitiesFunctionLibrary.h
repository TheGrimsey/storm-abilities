// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"

#include "GameplayEffectTypes.h"
#include "GameplayTags.h"

#include "AbilitySystemComponent.h"

#include "UnboundAbilitiesFunctionLibrary.generated.h"

/**
 * 
 */
UCLASS()
class UNBOUNDABILITIES_API UAbilitiesUtilityFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	/*
	*	Gameplay Effect related functions
	*/
		
	UFUNCTION(BlueprintCallable, Category = "Ability|GameplayEffect")
	//Returns all active effects from the AbilitySystem that don't have entered tag.
	static TArray<FActiveGameplayEffectHandle> GetActiveEffectsWithoutTag(const UAbilitySystemComponent* AbilitySystem, const FGameplayTag GameplayTag);

	UFUNCTION(BlueprintCallable, Category = "Ability|GameplayEffect")
	//Returns true if the Gameplay Effect has the GameplayTag.
	static bool DoesEffectHaveTag(const struct FActiveGameplayEffectHandle Handle, const FGameplayTag GameplayTag, bool ExactTag = false);

	UFUNCTION(BlueprintCallable, Category = "Ability|GameplayEffect", meta = (DefaultToSelf))
	static void BindStackChangeEvent(struct FActiveGameplayEffectHandle Handle, UObject* Object, const FName FuncName);

	UFUNCTION(BlueprintCallable, Category = "Ability|GameplayEffect", meta = (DefaultToSelf))
	static void BindEffectRemovedEvent(struct FActiveGameplayEffectHandle Handle, UObject* Object, const FName FuncName);

	/*
	*	Ability System related functions
	*/

	UFUNCTION(BlueprintCallable, Category = "Ability|Ability System")
	static void BindEffectAddedEvent(class UAbilitySystemComponent* AbilitySystem, TScriptInterface<class IGameplayEffectHandler> GameplayEffectHandler);

	UFUNCTION(BlueprintCallable, Category = "Ability|Ability System")
	static const TArray<struct FGameplayAbilitySpec>& GetAllActivatableAbilities(const class UAbilitySystemComponent* AbilitySystem);
	
	UFUNCTION(BlueprintCallable, Category = "Ability|Ability System")
	static const struct FGameplayAbilitySpecHandle GetAbilityHandleFromClass(class UAbilitySystemComponent* AbilitySystem, const TSubclassOf<class UGameplayAbility>& AbilityClass);

	UFUNCTION(BlueprintCallable, Category = "Ability|Ability System")
	static bool HasAbilityByClass(class UAbilitySystemComponent* AbilitySystem, const TSubclassOf<class UGameplayAbility>& AbilityClass);

	/*
	*	Ability related functions.
	*/

	UFUNCTION(BlueprintCallable, Category = "Ability")
	static const TSubclassOf<UGameplayAbility> GetAbilityClassFromSpec(const struct FGameplayAbilitySpec& AbilitySpec);

	/*
	*	Returns true if the Ability has the input tag.
	*	@Param Ability Ability to check for tag.
	*	@Param GameplayTag Tag to check.
	*	@Param ExactTag Whether we are matching for the exact tag, if true {A.1.X} will return false if we are looking for tag {A.1}
	*/
	UFUNCTION(BlueprintCallable, Category = "Ability")
	static bool DoesAbilityHaveTag(const TSubclassOf<UGameplayAbility> Ability, const FGameplayTag GameplayTag, bool ExactTag = false);
	
	UFUNCTION(BlueprintCallable, Category = "Ability")
	static void GetAbilityCooldownTimeFromSpec(const class UAbilitySystemComponent* AbilitySystem, const struct FGameplayAbilitySpec& Spec, float& TimeLeft, float& CooldownDuration);

	UFUNCTION(BlueprintCallable, Category = "Ability")
	static void GetAbilityCooldownTimeFromHandle(class UAbilitySystemComponent* AbilitySystem, const struct FGameplayAbilitySpecHandle& Handle, float& TimeLeft, float& CooldownDuration);

};
