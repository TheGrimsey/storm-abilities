// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "UnboundGameMode.h"
#include "UObject/ConstructorHelpers.h"

AUnboundGameMode::AUnboundGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/BP_UnboundCharacterPlayer"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
