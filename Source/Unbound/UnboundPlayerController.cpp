// Fill out your copyright notice in the Description page of Project Settings.


#include "UnboundPlayerController.h"

#include "GameFramework/PlayerState.h"
#include "AbilitySystemInterface.h"

#include "UnboundInteraction/InteractionComponent.h"

#include "UnrealNetwork.h"

AUnboundPlayerController::AUnboundPlayerController()
{
	InteractionComponent = CreateDefaultSubobject<UInteractionComponent>(TEXT("Interaction Component"));
}

void AUnboundPlayerController::GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(AUnboundPlayerController, ActionBarCache, COND_InitialOnly);
}

void AUnboundPlayerController::SetActionBarCache(struct FActionBarCacheData InActionBarCacheData)
{
	if (InActionBarCacheData.ActionBarId > MAX_ACTIONBARID || InActionBarCacheData.ActionBarId > MAX_ACTIONBARSLOT)
	{
		UE_LOG(LogTemp, Warning, TEXT("Tried to add ActionBarCacheData out of range. Action Bar Id: %d (Max is %d). Action Bar Slot: %d (Max is %d)"), InActionBarCacheData.ActionBarId, MAX_ACTIONBARID, InActionBarCacheData.Slot, MAX_ACTIONBARSLOT);
		return;
	}

	//Check if we are the server doing this else let's send this to the server too so it can save it.
	if (GetLocalRole() != ENetRole::ROLE_Authority)
	{
		ServerSetActionBarCache(InActionBarCacheData);
	}

	/*
	*	Try to find an existing cache data matching our slot.
	*/
	for (int i = 0; i < ActionBarCache.Num(); i++)
	{
		if (InActionBarCacheData.HasSameSlot(ActionBarCache[i]))
		{
			//Check if the actioncache type isn't none, if it is then we remove it.
			const bool bShouldRemove = InActionBarCacheData.ActionType == EActionCacheType::None;

			if (bShouldRemove)
			{
				//Remove from the actionbarcache.
				ActionBarCache.RemoveAtSwap(i);
			}
			else
			{
				//Insert our new actionbarcachedata.
				ActionBarCache[i] = InActionBarCacheData;
			}
			//Found what we were looking for let's exit out.
			return;
		}
	}

	//We didn't find one matching our slot so let's just add one.
	ActionBarCache.Add(InActionBarCacheData);
}

void AUnboundPlayerController::ServerSetActionBarCache_Implementation(struct FActionBarCacheData InActionBarCacheData)
{
	SetActionBarCache(InActionBarCacheData);
}

bool AUnboundPlayerController::GetActionBarCache(FActionBarCacheData& OutActionBarCacheData, int ActionBar, int Slot)
{
	for (FActionBarCacheData& ActionBarCacheData : ActionBarCache)
	{
		if (ActionBarCacheData.ActionBarId == ActionBar && ActionBarCacheData.Slot == Slot)
		{
			OutActionBarCacheData = ActionBarCacheData;
			return true;
		}
	}

	return false;
}

UUnboundAbilitySystemComponent* AUnboundPlayerController::GetAbilitySystemComponent()
{
	if (IAbilitySystemInterface * AbilitySystemPS = GetPlayerState<IAbilitySystemInterface>())
	{
		return Cast<UUnboundAbilitySystemComponent>(AbilitySystemPS->GetAbilitySystemComponent());
	}

	return nullptr;
}

void AUnboundPlayerController::ClientSetHUD_Implementation(TSubclassOf<AHUD> NewHUDClass)
{
	NewPlayerHUD = NewHUDClass;

	if (PlayerState)
	{
		Super::ClientSetHUD_Implementation(NewHUDClass);
	}
	else
	{
		static FName FuncName = TEXT("OnHUDWaitingForPlayerState");
		FScriptDelegate HUDPlayerStateDelegate; 
		HUDPlayerStateDelegate.BindUFunction(this, FuncName);

		OnPlayerStateReplicated.AddUnique(HUDPlayerStateDelegate);
	}
}

void AUnboundPlayerController::OnHUDWaitingForPlayerState()
{
	ClientSetHUD_Implementation(NewPlayerHUD);
}

void AUnboundPlayerController::OnRep_ActionBarCache()
{
	OnActionBarCacheReplicated.Broadcast();
}

void AUnboundPlayerController::OnRep_PlayerState()
{
	OnPlayerStateReplicated.Broadcast();
}

void AUnboundPlayerController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	/*
	*	Update ability system avatar actor to our new pawn.
	*/
	if (UUnboundAbilitySystemComponent* UnboundAbilitySystemComponent = GetAbilitySystemComponent())
	{
		UnboundAbilitySystemComponent->SetAvatarActor(InPawn);
	}
}

void AUnboundPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	
	if (PlayerState)
	{
		SetupInputCompWithState();
	}
	else
	{
		FScriptDelegate InputCompDelegate;
		InputCompDelegate.BindUFunction(this, TEXT("SetupInputCompWithState"));

		OnPlayerStateReplicated.AddUnique(InputCompDelegate);
	}
}

void AUnboundPlayerController::SetupInputCompWithState()
{
	//Bind AbilitySystemComp to InputSystem on PlayerState
	if (UUnboundAbilitySystemComponent * UnboundAbilitySystemComponent = GetAbilitySystemComponent())
	{
		UnboundAbilitySystemComponent->BindToInputComponent(InputComponent);
	}
}
