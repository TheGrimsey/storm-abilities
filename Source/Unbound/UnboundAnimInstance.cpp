// Fill out your copyright notice in the Description page of Project Settings.


#include "UnboundAnimInstance.h"

#include "GameFramework/PawnMovementComponent.h" 

void UUnboundAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	TRACE_CPUPROFILER_EVENT_SCOPE("UUnboundAnimInstance::NativeUpdateAnimation")

	APawn* Owner = TryGetPawnOwner();
	if (Owner)
	{
		InAir = Owner->GetMovementComponent()->IsFalling();

		Speed = Owner->GetVelocity().Size();
	}
}
