// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Animation/AnimInstance.h"
#include "Animation/AnimInstanceProxy.h"

#include "UnboundAnimInstance.generated.h"

/**
 * 
 */
UCLASS()
class UNBOUND_API UUnboundAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

protected:
	UPROPERTY(Transient, BlueprintReadOnly)
	float Speed;

	UPROPERTY(Transient, BlueprintReadOnly)
	bool InAir;

public:
	virtual void NativeUpdateAnimation(float DeltaSeconds);

};
