// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Characters/UnboundCharacterBase.h"
#include "Interactable.h"

#include "UnboundCharacterNPC.generated.h"

/**
 * NPC Actor. Holds all data relevant to NPCs. 
 */
UCLASS()
class UNBOUND_API AUnboundCharacterNPC : public AUnboundCharacterBase, public IInteractable
{
	GENERATED_BODY()

	/*
	*	Components
	*/
protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Gameplay Ability System", meta = (AllowPrivateAccess = "true"))
	class UUnboundAbilitySystemComponent* AbilitySystem;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Gameplay Ability System", meta = (AllowPrivateAccess = "true"))
	class UUnboundAttributeSet* AttributeSet;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "AI", meta = (AllowPrivateAccess = "true"))
	class UNavigationInvokerComponent* NavigationInvoker;

	/*
	*	Variables
	*/
protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, ReplicatedUsing=OnRep_NPCData)
	class UNPCData* NPCData;
	/*
	*	Methods
	*/
public:
	AUnboundCharacterNPC();

	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
	void Init(class UNPCData* InNPCData);

	virtual void Interact_Implementation(AActor* Interactor, class AUnboundPlayerController* InteractorController) override;


	//IAbilitySystemInterface
	class UAbilitySystemComponent* GetAbilitySystemComponent() const override;
	//End IAbilitySystemInterface

	void GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const override;
	
	void OnConstruction(const FTransform& Transform) override;

protected:
	UFUNCTION()
	void OnRep_NPCData();
};
