// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "RaceData.generated.h"

/**
 * Holds all data relevant to a race such as racials, name and model
 */
UCLASS(Blueprintable, BlueprintType)
class UNBOUND_API URaceData : public UDataAsset
{
	GENERATED_BODY()
	
	/*
	*	Variables
	*/
private:
	//// VISUALS

	//The name used in UI and such for the race.
	UPROPERTY(EditDefaultsOnly, Category="Display")
	FText DisplayName;

	//Base body mesh for the race (Not including hair, etc)
	UPROPERTY(EditDefaultsOnly, Category = "Display")
	class USkeletalMesh* RaceBaseMesh;

	//Default animation blueprint for this race.
	UPROPERTY(EditDefaultsOnly, Category = "Display")
	class TSubclassOf<class UAnimInstance> AnimationClass;

	//// GAMEPLAY

	//Racial effects applied to the character.
	UPROPERTY(EditDefaultsOnly, Category="Gameplay")
	TArray<TSubclassOf<class UGameplayEffect>> RacialEffects;
	
	//Racial abilities given to the character.
	UPROPERTY(EditDefaultsOnly, Category = "Gameplay")
	TArray<TSubclassOf<class UUnboundGameplayAbility>> RacialAbilities;

	/*
	*	Methods
	*/
public:
	//Returns the displayname for this race.
	UFUNCTION(BlueprintPure)
	const FText& GetDisplayName() { return DisplayName; }

	UFUNCTION(BlueprintPure)
	class USkeletalMesh* GetRaceMesh() { return RaceBaseMesh; }

	UFUNCTION(BlueprintPure)
	class TSubclassOf<class UAnimInstance> GetAnimationClass() { return AnimationClass; }

	//Returns the RacialEffect for this race. These are applied when the character spawns.
	UFUNCTION(BlueprintPure)
	const TArray<TSubclassOf<class UGameplayEffect>>& GetRacialEffects() { return RacialEffects;  }

	//Returns the RacialAbilities for this race. These are given to the character on spawn.
	UFUNCTION(BlueprintPure)
	const TArray<TSubclassOf<class UUnboundGameplayAbility>>& GetRacialAbilities() { return RacialAbilities; }
};
