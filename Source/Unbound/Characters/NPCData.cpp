// Fill out your copyright notice in the Description page of Project Settings.


#include "NPCData.h"
#include "RaceData.h"

TSubclassOf<class UAnimInstance> UNPCData::GetAnimationClass()
{
    return OverrideAnimationClass->IsValidLowLevel() ? OverrideAnimationClass : GetRace()->GetAnimationClass();
}
