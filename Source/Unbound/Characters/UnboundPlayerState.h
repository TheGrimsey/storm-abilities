// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "GameFramework/PlayerState.h"

#include "AbilitySystemInterface.h"

#include "UnboundPlayerState.generated.h"

/**
 * UnboundPlayerState.
 * NOTABLE DIFFERENCES:
 *	- Only replicated to people in range of pawn instead of everyone.
 */
UCLASS()
class UNBOUND_API AUnboundPlayerState : public APlayerState, public IAbilitySystemInterface
{
	GENERATED_BODY()
	
	/*
	*	Variables
	*/
public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Gameplay Ability System", meta = (AllowPrivateAccess = "true"))
	class UUnboundAbilitySystemComponent* AbilitySystem;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Gameplay Ability System", meta = (AllowPrivateAccess = "true"))
	class UUnboundAttributeSet* AttributeSet;
protected:

	/*
	*	Methods
	*/
public:
	AUnboundPlayerState();

	//Override netrelevancy so we can more tightly control it.
	virtual bool IsNetRelevantFor(const AActor* RealViewer, const AActor* ViewTarget, const FVector& SrcLocation) const override;

	//IAbilitySystemInterface
	virtual class UAbilitySystemComponent* GetAbilitySystemComponent() const override;
	//End IAbilitySystemInterface
};
