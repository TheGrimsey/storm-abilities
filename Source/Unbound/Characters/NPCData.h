// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "NPCData.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class UNBOUND_API UNPCData : public UDataAsset
{
	GENERATED_BODY()
	
	/*
	*	Variables
	*/
private:
	//Name of the NPC
	UPROPERTY(EditDefaultsOnly, Category = "NPC Details")
	FText Name;

	//Title of the NPC
	UPROPERTY(EditDefaultsOnly, Category = "NPC Details")
	FText Title;

	//Dialogue tree for NPC.
	UPROPERTY(EditDefaultsOnly, Category = "NPC Details")
	class UDialogueTree* Dialogue;

	//Race of the NPC.
	UPROPERTY(EditDefaultsOnly, Category="Race")
	class URaceData* Race;	

	//Overrides animation 
	UPROPERTY(EditDefaultsOnly, Category = "Display")
	class TSubclassOf<class UAnimInstance> OverrideAnimationClass;

	//Behaviour Tree used when the NPC is idle (not in combat).
	UPROPERTY(EditDefaultsOnly, Category = "AI")
	class UBehaviorTree* IdleBehaviourTree;

	//Behaviour Tree used when the NPC is in combat.
	UPROPERTY(EditDefaultsOnly, Category = "AI")
	class UBehaviorTree* CombatBehaviourTree;

	/*
	*	Methods
	*/
public:
	//Returns the name of the NPC.
	UFUNCTION(BlueprintPure)
	const FText& GetName() { return Name; }

	//Returns the title of the NPC.
	UFUNCTION(BlueprintPure)
	const FText& GetTitle() { return Title; }

	UFUNCTION(BlueprintPure)
	class UDialogueTree* GetDialogueTree() { return Dialogue; }

	UFUNCTION(BlueprintPure)
	class URaceData* GetRace() { return Race; }

	UFUNCTION(BlueprintPure)
	class TSubclassOf<class UAnimInstance> GetAnimationClass();

	UFUNCTION(BlueprintPure)
	class UBehaviorTree* GetIdleBehaviourTree() { return IdleBehaviourTree; }

	UFUNCTION(BlueprintPure)
	class UBehaviorTree* GetCombatBehaviourTree() { return CombatBehaviourTree; }
};
