// Fill out your copyright notice in the Description page of Project Settings.


#include "UnboundPlayerState.h"

#include "GameFramework/Pawn.h"

#include "UnboundAbilities/Ability/UnboundAbilitySystemComponent.h"
#include "UnboundAbilities/UnboundAttributeSet.h"

AUnboundPlayerState::AUnboundPlayerState() : APlayerState()
{
	/*
	*	Replication
	*/
	//We don't actually want this to always be relevant. We only want it to be relevant to the owner and people near the pawn AND people in the same party.
	bAlwaysRelevant = false;
	
	//Don't replicate ping. It isn't necessary for us and wasted bandwidth.
	SetShouldUpdateReplicatedPing(false);

	NetUpdateFrequency = 30.f;
	MinNetUpdateFrequency = 2.f;

	/*
	*	Construct components.
	*/
	AbilitySystem = CreateDefaultSubobject<UUnboundAbilitySystemComponent>(TEXT("Ability System"));

	//AttributeSet
	AttributeSet = CreateDefaultSubobject<UUnboundAttributeSet>(TEXT("AttributeSet"));
}

bool AUnboundPlayerState::IsNetRelevantFor(const AActor* RealViewer, const AActor* ViewTarget, const FVector& SrcLocation) const
{
	/*
	*	If we are owned by RealViewer (Player Controller) then we replicate of course.
	*	ELSE only replicate if the pawn is NetRelevant (and we have one)
	*/
	return IsOwnedBy(RealViewer)|| GetPawn() && GetPawn()->IsNetRelevantFor(RealViewer, ViewTarget, SrcLocation);
}

UAbilitySystemComponent* AUnboundPlayerState::GetAbilitySystemComponent() const
{
	return AbilitySystem;
}
