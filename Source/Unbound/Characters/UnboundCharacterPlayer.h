// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UnboundCharacterBase.h"

#include "AbilitySystemInterface.h"
#include "AbilitySystemComponent.h"

#include "UnboundCharacterPlayer.generated.h"

UCLASS(config=Game)
class AUnboundCharacterPlayer : public AUnboundCharacterBase
{
	GENERATED_BODY()

	/*
	*	Variables
	*/
private:
	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoomComponent;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* CameraComponent;

protected:
	/** Minimum distance away from the player the Camera can be. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Camera)
	float MinCameraDistance = 100.f;

	/** Maximum distance away from the player the Camera can be. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Camera)
	float MaxCameraDistance = 300.f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Camera)
	float CameraZoomSpeed = 50.f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Gameplay)
	float ReachDistance = 200.f;

	/*
	*	Methods
	*/
public:
	AUnboundCharacterPlayer();

	//IAbilitySystemInterface
	class UAbilitySystemComponent* GetAbilitySystemComponent() const override;

	UFUNCTION(BlueprintCallable)
	void TestLoadAbilityFromGUID();

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** Called for scroll input */
	void ZoomCamera(float Value);

	void Interact();

	UFUNCTION(Server, Reliable)
	void ServerInteract(AActor* ActorToInteractWith);

	void InternalInteract(AActor* ActorToInteractWith);
};

