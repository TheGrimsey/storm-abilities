// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "UnboundCharacterPlayer.h"

#include "Interactable.h"

#include "UnboundPlayerController.h"
#include "Camera/CameraComponent.h"
#include "Components/InputComponent.h"

#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"

#include "Ability/UnboundAbilitySystemComponent.h"
#include "UnboundAttributeSet.h"
#include "UnboundPlayerState.h"

#include "DrawDebugHelpers.h" 
#include "UnboundAbilities/Ability/UnboundGameplayAbility.h"
#include <Runtime\AssetRegistry\Public\AssetRegistryModule.h>

//////////////////////////////////////////////////////////////////////////
// AUnboundCharacterPlayer

AUnboundCharacterPlayer::AUnboundCharacterPlayer() : AUnboundCharacterBase()
{

	if (GetNetMode() != ENetMode::NM_DedicatedServer)
	{
		/*
		*	Configure camera boom (pulls in towards the player if there is a collision)
		*/
		CameraBoomComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));

		CameraBoomComponent->SetupAttachment(RootComponent);
		// Set the distance that the camera follows the player at.
		CameraBoomComponent->TargetArmLength = 200.0f;
		CameraBoomComponent->SocketOffset = FVector(0.f, 50.f, 0.f);
		// Rotate the arm based on the controller
		CameraBoomComponent->bUsePawnControlRotation = true;

		/*
		*	Configure Camera Component
		*/
		CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));

		// Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
		CameraComponent->SetupAttachment(CameraBoomComponent, USpringArmComponent::SocketName);
	}
}

UAbilitySystemComponent* AUnboundCharacterPlayer::GetAbilitySystemComponent() const
{
	if (AUnboundPlayerState* UnboundPlayerState = Cast<AUnboundPlayerState>(GetPlayerState()))
	{
		return UnboundPlayerState->GetAbilitySystemComponent();
	}

	return nullptr;
}

void AUnboundCharacterPlayer::TestLoadAbilityFromGUID()
{
	double GetAssetsStartTime = FPlatformTime::Seconds();

	FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>("AssetRegistry");
	TArray<FAssetData> AssetData;

	FARFilter Filter;
	Filter.ClassNames.Add(FName("GameplayAbilityBlueprint"));
	Filter.bRecursiveClasses = true;
	Filter.PackagePaths.Add(FName("/Game/Blueprints/Abilities"));
	Filter.bRecursivePaths = true;
	Filter.TagsAndValues.Add("GUID", TOptional<FString>("A78B28F04996C6C364853BBD44007A41"));

	AssetRegistryModule.Get().GetAssets(Filter, AssetData);

	UE_LOG(LogTemp, Warning, TEXT("Search Time: %0.9f Seconds"), FPlatformTime::Seconds() - GetAssetsStartTime);

	for (const FAssetData& asset : AssetData)
	{
		UE_LOG(LogTemp, Warning, TEXT("%s"), *asset.GetFullName());
	}

}

//////////////////////////////////////////////////////////////////////////
// Input

void AUnboundCharacterPlayer::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);

	// JUMP
	static FName Jump = FName(TEXT("Jump"));
	PlayerInputComponent->BindAction(Jump, IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction(Jump, IE_Released, this, &ACharacter::StopJumping);

	//MOVEMENT
	static FName MoveForward = FName(TEXT("MoveForward"));
	PlayerInputComponent->BindAxis(MoveForward, this, &AUnboundCharacterPlayer::MoveForward);
	static FName MoveRight = FName(TEXT("MoveRight"));
	PlayerInputComponent->BindAxis(MoveRight, this, &AUnboundCharacterPlayer::MoveRight);

	// TURNING
	static FName Turn = FName(TEXT("Turn"));
	PlayerInputComponent->BindAxis(Turn, this, &APawn::AddControllerYawInput);
	static FName LookUp = FName(TEXT("LookUp"));
	PlayerInputComponent->BindAxis(LookUp, this, &APawn::AddControllerPitchInput);

	// CAMERA
	static FName CameraZoom = FName(TEXT("CameraZoom"));
	PlayerInputComponent->BindAxis(CameraZoom, this, &AUnboundCharacterPlayer::ZoomCamera);

	// INTERACT
	static FName Interact = FName(TEXT("Interact"));
	PlayerInputComponent->BindAction(Interact, IE_Pressed, this, &AUnboundCharacterPlayer::Interact);
}

void AUnboundCharacterPlayer::MoveForward(float Value)
{
	if ((Controller != nullptr) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void AUnboundCharacterPlayer::MoveRight(float Value)
{
	if ( (Controller != nullptr) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void AUnboundCharacterPlayer::ZoomCamera(float Value)
{
	if ((Controller != nullptr) && (Value != 0.0f))
	{
		const float UnclampedNewCameraArmLength = CameraBoomComponent->TargetArmLength + (Value * CameraZoomSpeed);

		const float ClampedNewCameraArmLength = FMath::Clamp<float>(UnclampedNewCameraArmLength, MinCameraDistance, MaxCameraDistance);

		CameraBoomComponent->TargetArmLength = ClampedNewCameraArmLength;
	}
}

void AUnboundCharacterPlayer::Interact()
{
	FVector RayStart = CameraComponent->GetComponentLocation();
	FVector Direction = CameraComponent->GetForwardVector();
	FVector RayEnd;

	FVector CameraToCenter = GetActorLocation() - RayStart;

	float DotToCenter = FVector::DotProduct(CameraToCenter, Direction);
	if (DotToCenter >= 0)		//If this fails, we're pointed away from the center, but we might be inside the sphere and able to find a good exit point.
	{
		float DistanceSquared = CameraToCenter.SizeSquared() - (DotToCenter * DotToCenter);
		float ReachDistanceSquared = (ReachDistance * ReachDistance);

		if (DistanceSquared <= ReachDistanceSquared)
		{
			float DistanceFromCamera = FMath::Sqrt(ReachDistanceSquared - DistanceSquared);
			float DistanceAlongRay = DotToCenter + DistanceFromCamera;						//Subtracting instead of adding will get the other intersection point
			RayEnd = RayStart + (DistanceAlongRay * Direction);		//Cam aim point clipped to range sphere
		}
	}
	else
	{
		RayEnd = RayStart + Direction * ReachDistance;
	}

	FHitResult Result;
	FCollisionQueryParams QueryParams;
	QueryParams.AddIgnoredActor(this);//Ignore this actor.
	QueryParams.bTraceComplex = true; //Trace complex because we can.

	GetWorld()->LineTraceSingleByProfile(Result, RayStart, RayEnd, FName(TEXT("BlockAllDynamic")), QueryParams);

	DrawDebugLine(GetWorld(), RayStart, RayEnd, FColor::Green, false, 10.f, 0, 5.f);

	if (Result.Actor.IsValid())
	{
		ServerInteract(Result.GetActor());
	}
}

void AUnboundCharacterPlayer::ServerInteract_Implementation(AActor* ActorToInteractWith)
{
	//TODO Check visibiity.
	InternalInteract(ActorToInteractWith);
}

void AUnboundCharacterPlayer::InternalInteract(AActor* ActorToInteractWith)
{
	if (IInteractable * Interactable = Cast<IInteractable>(ActorToInteractWith))
	{
		//Check so target actor is in range.
		if (FVector::DistSquared(GetActorLocation(), ActorToInteractWith->GetActorLocation()) <= ReachDistance * ReachDistance)
		{
			Interactable->Execute_Interact(ActorToInteractWith, this, GetController<AUnboundPlayerController>());
		}
	}
}