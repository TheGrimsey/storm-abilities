// Fill out your copyright notice in the Description page of Project Settings.


#include "UnboundCharacterBase.h"

#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"

#include "GameFramework/CharacterMovementComponent.h"

// Sets default values
AUnboundCharacterBase::AUnboundCharacterBase()
{
 	// Set this character to never tick.
	PrimaryActorTick.bCanEverTick = false;
	PrimaryActorTick.bStartWithTickEnabled = false;

	/*
	*	Set default size for collision capsule.
	*/
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	/*
	*	Configure Character Movement.
	*/
	GetCharacterMovement()->JumpZVelocity = 500.f;
	GetCharacterMovement()->AirControl = 0.2f;

	/*
	*	Configure Mesh rotation & location.
	*/
	GetMesh()->SetRelativeRotation(FRotator(0.0f, 270.f, 0.0f));
	GetMesh()->SetRelativeLocation(FVector(0.f, 0.f, -96.f));
	GetMesh()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetMesh()->SetGenerateOverlapEvents(false);

	/*
	*	Replication.
	*/
	//Try to update this every tick (at 30 tps).
	NetUpdateFrequency = 30.f;
	MinNetUpdateFrequency = 5.f;

	FRepMovement& MutRepMovement = GetReplicatedMovement_Mutable();
	MutRepMovement.LocationQuantizationLevel = EVectorQuantization::RoundWholeNumber;
	MutRepMovement.VelocityQuantizationLevel = EVectorQuantization::RoundWholeNumber;
	MutRepMovement.RotationQuantizationLevel = ERotatorQuantization::ByteComponents;
}

UAbilitySystemComponent* AUnboundCharacterBase::GetAbilitySystemComponent() const
{
	return nullptr;
}


