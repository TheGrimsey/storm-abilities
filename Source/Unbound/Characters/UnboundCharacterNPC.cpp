// Fill out your copyright notice in the Description page of Project Settings.


#include "UnboundCharacterNPC.h"

#include "Ability/UnboundAbilitySystemComponent.h"
#include "UnboundAttributeSet.h"

#include "UnboundPlayerController.h"
#include "UnboundDialogue/DialogueInteraction.h"

#include "NavigationInvokerComponent.h"

#include "AIController.h"
#include "UnrealNetwork.h"

#include "NPCData.h"
#include "RaceData.h"

AUnboundCharacterNPC::AUnboundCharacterNPC()
{
	/*
	*	Construct components.
	*/
	//AbilitySystem
	AbilitySystem = CreateDefaultSubobject<UUnboundAbilitySystemComponent>(TEXT("AbilitySystem"));

	//AttributeSet
	AttributeSet = CreateDefaultSubobject<UUnboundAttributeSet>(TEXT("AttributeSet"));

	//NavigationInvoker
	NavigationInvoker = CreateDefaultSubobject<UNavigationInvokerComponent>(TEXT("NavigationInvoker"));
}

void AUnboundCharacterNPC::BeginPlay()
{
	Super::BeginPlay();

	if (NPCData)
	{
		Init(NPCData);
	}
}

void AUnboundCharacterNPC::Init(UNPCData* InNPCData)
{
	NPCData = InNPCData;

	//TODO Make function to handle setting all visuals.
	GetMesh()->SetSkeletalMesh(NPCData->GetRace()->GetRaceMesh());
	GetMesh()->SetAnimClass(NPCData->GetAnimationClass());

	if (HasAuthority())
	{
		//Run idle behaviour tree if it exists.
		if (AAIController * AIController = Cast<AAIController>(GetController()))
		{
			if (NPCData->GetIdleBehaviourTree())
			{
				AIController->RunBehaviorTree(NPCData->GetIdleBehaviourTree());
			}
		}

		//Remove ALL effects and abilities.
		AbilitySystem->RemoveActiveEffects(FGameplayEffectQuery(), -1);

		AbilitySystem->ClearAllAbilities();

		/*
		*	Apply all race effects and abilities.
		*/

		//Apply racial effects.
		for (TSubclassOf<UGameplayEffect> Effect : NPCData->GetRace()->GetRacialEffects())
		{
			FGameplayEffectSpec EffectSpec(Effect.GetDefaultObject(), FGameplayEffectContextHandle(), 1.f);

			AbilitySystem->ApplyGameplayEffectSpecToSelf(EffectSpec);
		}

		//Grant Racial Abilities.
		for (TSubclassOf<UUnboundGameplayAbility> Ability : NPCData->GetRace()->GetRacialAbilities())
		{
			AbilitySystem->TeachAbility(Ability, 1, true);
		}
	}
}

void AUnboundCharacterNPC::Interact_Implementation(AActor* Interactor, AUnboundPlayerController* InteractorController)
{
	if (NPCData)
	{
		if (InteractorController && NPCData->GetDialogueTree())
		{
			//Start a dialogue with this NPC if we have a DialogueTree.
			UDialogueInteraction::StartDialogue(InteractorController->InteractionComponent, this, NPCData->GetDialogueTree());
		}

	}
}

UAbilitySystemComponent* AUnboundCharacterNPC::GetAbilitySystemComponent() const
{
	return AbilitySystem;
}

void AUnboundCharacterNPC::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AUnboundCharacterNPC, NPCData);
}

void AUnboundCharacterNPC::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

	if (NPCData)
	{
		Init(NPCData);
	}
}

void AUnboundCharacterNPC::OnRep_NPCData()
{
	//Whenever it changes we need to reinit.
	Init(NPCData);
}
