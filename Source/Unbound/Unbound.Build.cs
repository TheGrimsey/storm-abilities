// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Unbound : ModuleRules
{
	public Unbound(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "AIModule", "NavigationSystem" });

		/*
		*	Unbound Modules
		*/
        PublicDependencyModuleNames.AddRange(new string[] { "UnboundAbilities", "UnboundDialogue", "UnboundInteraction" });
	}
}
