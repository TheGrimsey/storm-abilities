// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"

#include "UnboundAbilities/Ability/UnboundAbilitySystemComponent.h"

#include "UI/ActionCacheTypes.h"

#include "UnboundPlayerController.generated.h"

//Event for when a property is replicated.
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnPropertyReplicated);

/*
*	Static variables for ActionBars.
*/
static const int MAX_ACTIONBARID = 2;
static const int MAX_ACTIONBARSLOT = 11;

/**
 * Unbound Player Controller.
 * Slightly changed for our purposes notably:
 *	- HUD doesnt spawn until the PlayerState has been replicated. (This is to make sure we can always register our events for AbilitySystem stuff which is on the playerstate)
 */
UCLASS()
class UNBOUND_API AUnboundPlayerController : public APlayerController
{
	GENERATED_BODY()

	/*
	*	Variables
	*/
public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UInteractionComponent* InteractionComponent;
	
	UPROPERTY(BlueprintAssignable)
	FOnPropertyReplicated OnActionBarCacheReplicated;

	UPROPERTY(BlueprintAssignable)
	FOnPropertyReplicated OnPlayerStateReplicated;

protected:
	UPROPERTY(VisibleAnywhere, ReplicatedUsing=OnRep_ActionBarCache)
	TArray<struct FActionBarCacheData> ActionBarCache;
	
private:
	//Player HUD that came in from ClientSetHUD if we didnt have our playerstate at the time.
	UPROPERTY()
	TSubclassOf<class AHUD> NewPlayerHUD;
		
	/*
	*	Methods
	*/
public:
	AUnboundPlayerController();

	void GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const override;

	//Sets the actionbarcache data to input InActionBarCacheData. This replaces the data (if any) relevant to the new data's slot or removes it if the action is None.
	UFUNCTION(BlueprintCallable)
	void SetActionBarCache(struct FActionBarCacheData InActionBarCacheData);

	UFUNCTION(Server, Reliable)
	void ServerSetActionBarCache(struct FActionBarCacheData InActionBarCacheData);

	//Returns ActionBarCacheData for relevant slot and actionbar.
	UFUNCTION(BlueprintCallable)
	bool GetActionBarCache(FActionBarCacheData& OutActionBarCacheData, int ActionBar, int Slot);

	UUnboundAbilitySystemComponent* GetAbilitySystemComponent();

	virtual void ClientSetHUD_Implementation(TSubclassOf<class AHUD> NewHUDClass) override;

protected:
	//Called when we recieve the playerstate if we have been waitng for it.
	UFUNCTION()
	void OnHUDWaitingForPlayerState();

	UFUNCTION()
	void OnRep_ActionBarCache();

	virtual void OnRep_PlayerState() override;

	virtual void OnPossess(APawn* InPawn) override;

	virtual void SetupInputComponent() override;

	//Called to set up input that needs PlayerState.
	UFUNCTION()
	void SetupInputCompWithState();
};
