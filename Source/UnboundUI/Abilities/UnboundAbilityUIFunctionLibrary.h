// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "GameplayEffectTypes.h"

#include "UnboundAbilityUIFunctionLibrary.generated.h"

/**
 *
 */
UCLASS()
class UNBOUNDUI_API UUnboundAbilityUIFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	/*
	*	GAMEPLAY EFFECT RELATED FUNCTIONS
	*/

	UFUNCTION(BlueprintCallable, Category = "Ability|GameplayEffect", BlueprintCosmetic)
	//Returns the UIData for the effect related to the handle.
	static class UGameplayEffectUIData* GetUIDataFromEffectHandle(struct FActiveGameplayEffectHandle Handle);

	/*
	*	ABILITY RELATED FUNCTIONS.
	*/
	
	/*
	*	Returns UI Data from AbilityClass.
	*/
	UFUNCTION(BlueprintPure, Category = "Ability", BlueprintCosmetic)
	static class UUnboundGASUIData* GetUIDataFromAbilityClass(const TSubclassOf<class UUnboundGameplayAbility> AbilityClass);

	/*
	*	GENERAL UI RELATED FUNCTIONS.
	*/

	/*
	*	Returns the name of the first key that is mapped to the input action.
	*	@Param PlayerController Controller to pull input mappings from.
	*	@Param InputAction The Input Action we are looking for the key for.
	*/
	UFUNCTION(BlueprintCallable, Category = "Input", BlueprintCosmetic)
	static FText GetFirstKeyNameForInputAction(const class APlayerController* PlayerController, const FName InputAction);
	
	/**
	* Format Time left.
	* @Param TimeLeft The time to format.
	* @Param RoundingMode Mode for rounding the numbers.
	* @Param MinuteFormat The format for when the time left is higher than a minute. {M} for minute, {S} for seconds.
	* @Param MaximumForDecimal The maximum time left where decimals will be included in the text.
	*/
	UFUNCTION(BlueprintCallable, Category = "Text", BlueprintCosmetic)
	static FText FormatTimeLeft(const float TimeLeft, const ERoundingMode RoundingMode, const FText MinuteFormat, const float MaximumForDecimal);
};
