// Fill out your copyright notice in the Description page of Project Settings.


#include "UnboundAbilityUIFunctionLibrary.h"

#include "GameFramework/PlayerInput.h"
#include "GameFramework/PlayerController.h"

#include "GameplayAbilities/Public/AbilitySystemComponent.h"

#include "UnboundAbilities/UI/UnboundGASUIData.h"
#include "UnboundAbilities/Ability/UnboundGameplayAbility.h"

UGameplayEffectUIData* UUnboundAbilityUIFunctionLibrary::GetUIDataFromEffectHandle(struct FActiveGameplayEffectHandle Handle)
{
	const UGameplayEffect* ActiveGE = Handle.GetOwningAbilitySystemComponent()->GetGameplayEffectDefForHandle(Handle);
	if (ActiveGE)
	{
		return ActiveGE->UIData;
	}

	return nullptr;
}

UUnboundGASUIData* UUnboundAbilityUIFunctionLibrary::GetUIDataFromAbilityClass(const TSubclassOf<UUnboundGameplayAbility> AbilityClass)
{
	check(AbilityClass);

	return AbilityClass->GetDefaultObject<UUnboundGameplayAbility>()->GetUIData();
}

FText UUnboundAbilityUIFunctionLibrary::GetFirstKeyNameForInputAction(const APlayerController* PlayerController, const FName InputAction)
{
	if (!PlayerController || InputAction.IsNone())
		return FText::GetEmpty();

	const TArray<FInputActionKeyMapping>& Keys = PlayerController->PlayerInput->GetKeysForAction(InputAction);
	if (Keys.Num() != 0)
	{
		FString Text;

		if (Keys[0].bCtrl)
		{
			Text.Append(TEXT("C"));
		}

		if (Keys[0].bShift)
		{
			Text.Append(TEXT("S"));
		}

		if (Keys[0].bAlt)
		{
			Text.Append(TEXT("A"));
		}

		Text.Append(Keys[0].Key.GetDisplayName().ToString());

		return FText::FromString(Text);
	}
	
	return FText::GetEmpty();
}

FText UUnboundAbilityUIFunctionLibrary::FormatTimeLeft(const float TimeLeft, const ERoundingMode RoundingMode, const FText MinuteFormat, const float MaximumForDecimal)
{
	FNumberFormattingOptions NumberFormatOptions;
	NumberFormatOptions.AlwaysSign = false;
	NumberFormatOptions.UseGrouping = true;
	NumberFormatOptions.RoundingMode = RoundingMode;
	NumberFormatOptions.MinimumIntegralDigits = 1;
	NumberFormatOptions.MaximumIntegralDigits = 324;
	NumberFormatOptions.MinimumFractionalDigits = 0;
	NumberFormatOptions.MaximumFractionalDigits = 0;

	if (TimeLeft > 60.f)
	{
		NumberFormatOptions.MinimumIntegralDigits = 2;

		int Minutes = TimeLeft / 60;
		int Seconds = (int)TimeLeft % 60;

		return FText::FormatNamed(FTextFormat(MinuteFormat), TEXT("M"), Minutes, TEXT("S"), FText::AsNumber(Seconds, &NumberFormatOptions));
	}
	if (TimeLeft < MaximumForDecimal)
	{
		NumberFormatOptions.MaximumFractionalDigits = 1;

		return FText::AsNumber(TimeLeft, &NumberFormatOptions);
	}

	return FText::AsNumber(TimeLeft, &NumberFormatOptions);
}