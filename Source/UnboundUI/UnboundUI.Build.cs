// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class UnboundUI : ModuleRules
{
	public UnboundUI(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });

        /*
         *  UI used modules
         */
        PublicDependencyModuleNames.AddRange(new string[] { "GameplayAbilities" });

        /*
         * Unbound Modules
         */
        PublicDependencyModuleNames.AddRange(new string[] { "Unbound", "UnboundAbilities", "UnboundDungeons" });
	}
}
