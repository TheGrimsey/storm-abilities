// Fill out your copyright notice in the Description page of Project Settings.


#include "DialogueTreeFactory.h"
#include "AssetTypeCategories.h"

#include "DialogueTree.h"
#include "DialogueTreeData.h"

#include "Package.h"
#include "AssetRegistryModule.h"

UDialogueTreeFactory::UDialogueTreeFactory()
{
	bCreateNew = true;
	bEditAfterNew = true;
	SupportedClass = UDialogueTree::StaticClass();
}

UObject* UDialogueTreeFactory::FactoryCreateNew(UClass* Class, UObject* InParent, FName Name, EObjectFlags Flags, UObject* Context, FFeedbackContext* Warn)
{
	UDialogueTree* NewDialogueTree = NewObject<UDialogueTree>(InParent, Class, Name, Flags | RF_Transactional);

	/*
	*	Create DialogueTreeData asset.
	*/
	FString DataPackageName = NewDialogueTree->GetOuter()->GetPathName() + TEXT("Data");
	UPackage* DataPackage = CreatePackage(NULL, *DataPackageName);
	DataPackage->FullyLoad();

	FString DataName = NewDialogueTree->GetName();
	DataName += TEXT("Data");

	UDialogueTreeData* NewDialogueTreeData = NewObject<UDialogueTreeData>(DataPackage, *DataName, Flags | RF_Transactional);

	DataPackage->MarkPackageDirty();
	FAssetRegistryModule::AssetCreated(NewDialogueTreeData);

	FString PackageFileName = FPackageName::LongPackageNameToFilename(DataPackageName, FPackageName::GetAssetPackageExtension());
	bool bSaved = UPackage::SavePackage(DataPackage, NewDialogueTreeData, Flags | RF_Transactional, *PackageFileName, GError, nullptr, true, true, SAVE_NoError);

	NewDialogueTree->DialogueTreeData = NewDialogueTreeData;

	return NewDialogueTree;
}

uint32 UDialogueTreeFactory::GetMenuCategories() const
{
	return EAssetTypeCategories::Gameplay;
}

FText UDialogueTreeFactory::GetDisplayName() const
{
	return FText::FromString(TEXT("Dialogue Tree"));
}

FString UDialogueTreeFactory::GetDefaultNewAssetName() const
{
	return TEXT("NewDialogueTree");
}
