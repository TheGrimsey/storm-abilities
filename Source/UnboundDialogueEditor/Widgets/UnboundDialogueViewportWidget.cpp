// Fill out your copyright notice in the Description page of Project Settings.

#include "UnboundDialogueViewportWidget.h"

#include "DialogueTree.h"
#include "DialogueTreeData.h"
#include "DialogueTreeEditor.h"
#include "UnboundDialogueNodeWidget.h"

#include "Editor/GraphEditor/Private/SGraphEditorImpl.h"
#include "Runtime/Slate/Public/Widgets/Layout/SConstraintCanvas.h"
#include "Editor/UnrealEd/Public/ScopedTransaction.h"
#include "GraphEditorSettings.h"
#include "Editor.h"
#include "EdGraphUtilities.h"

#include "Framework/Commands/UIAction.h"
#include "Framework/Commands/UICommandList.h"

#include "HAL/PlatformApplicationMisc.h"

#include "Dom/JsonObject.h"
#include "Dom/JsonValue.h"

#include "Sound/SoundBase.h"
#include "Sound/DialogueWave.h"

#include "JsonObjectConverter.h"
#include "UObject/NoExportTypes.h"

#include "Widgets/Layout/SBox.h"

#include "Framework/Application/SlateApplication.h"
#include "Framework/MultiBox/MultiBoxBuilder.h"

#include "Serialization/BufferArchive.h"
#include "Serialization/MemoryReader.h"

#define LOCTEXT_NAMESPACE "SUnboundDialogueViewportWidget"

namespace ZoomLevels
{
	FText GetZoomText(int32 InZoomLevel)
	{
		FString string = TEXT("");
		string.AppendInt(InZoomLevel);

		return FText::FromString(string);
	}

	float GetZoomAmount(int32 ZoomLevel)
	{
		if (ZoomLevel >= 0)
		{
			return (0.14 * ZoomLevel) + 1;
		}
		else
		{
			return (0.0835 * ZoomLevel) + 1;
		}
	}

	int32 GetMinZoomLevel()
	{
		return -7;
	}

	int32 GetMaxZoomLevel()
	{
		return 7;
	}

	int32 GetDefaultZoomLevel()
	{
		return 0;
	}

	EDialogueRenderingLOD GetLOD(int32 InZoomLevel)
	{
		if (InZoomLevel <= -3)
		{
			return EDialogueRenderingLOD::LowDetail;
		}
		else if (InZoomLevel <= 3)
		{
			return EDialogueRenderingLOD::MediumDetail;
		}
		else
		{
			return EDialogueRenderingLOD::DefaultDetail;
		}
	}
}

void SUnboundDialogueViewportWidget::Construct(const FArguments& InArgs, TSharedPtr<class FDialogueTreeEditor> InDialogueTreeEditor)
{
	CurrentZoomLevel = ZoomLevels::GetDefaultZoomLevel();
	PreviousZoomLevel = ZoomLevels::GetDefaultZoomLevel();
	ZoomLevelFade = FCurveSequence(0.0f, 1.0f);
	ZoomLevelFade.Play(this->AsShared());
	PostChangedZoom();
	UpdateZoomAffectedValues();

	DialogueTree = InArgs._DialogueTree;
	DialogueTreeEditor = InDialogueTreeEditor;
	ShowGraphStateOverlay = InArgs._ShowGraphStateOverlay;
	IsEditable = InArgs._IsEditable;

	ChildSlot
		[
			SNew(SBox)
			[
				SAssignNew(CanvasPanel, SConstraintCanvas)
			]
		];
	SpawnNodes();
}

FCursorReply SUnboundDialogueViewportWidget::OnCursorQuery(const FGeometry& MyGeometry, const FPointerEvent& CursorEvent) const
{
	if (bIsPanning)
	{
		return FCursorReply::Cursor(EMouseCursor::None);
	}
	if (bBreakingLinksMode)
	{
		return FCursorReply::Cursor(EMouseCursor::Crosshairs);
	}

	// If we're dragging a node or multiple nodes.
	if (DraggedNodeIndex != -1)
	{
		// All selected nodes besides the one under the cursor:
		for (int32 NodeId : SelectedNodes)
		{
			int32 NodeIndex = NodeIdsToIndexes.FindRef(NodeId);
			if (DraggedNodeIndex != NodeIndex)
			{
				// NodeCoord = (MouseCoords + DragOffset) * Zoom - PanOffset + (difference in distance between this node and the node being dragged by the cursor)
				DialogueTree->Nodes[NodeIndex].Coordinates = (MyGeometry.AbsoluteToLocal(CursorEvent.GetScreenSpacePosition()) + draggingOffset) / GetZoomAmount() - panningOffset + (DialogueTree->Nodes[NodeIndex].Coordinates - DialogueTree->Nodes[DraggedNodeIndex].Coordinates);
			}
		}
		// The one that is being dragged by the cursor:
		// MouseCoords = (NodeCoords + panningOffset) * Zoom - draggingOffset
		// <=> NodeCoords = (MouseCoords + draggingOffset) / Zoom - PanningOffset
		DialogueTree->Nodes[DraggedNodeIndex].Coordinates = (MyGeometry.AbsoluteToLocal(CursorEvent.GetScreenSpacePosition()) + draggingOffset) / GetZoomAmount() - panningOffset;

		return FCursorReply::Cursor(EMouseCursor::CardinalCross);
	}

	if (bIsSelectingMultipleNodes)
	{
		// sets marquee coordinates
		MarqueePointOfOrigin = FVector2D(
			(clickDownCoords.X + selectionPanningOffset.X) < dragMouseCoords.X ? clickDownCoords.X + selectionPanningOffset.X : dragMouseCoords.X,
			(clickDownCoords.Y + selectionPanningOffset.Y) < dragMouseCoords.Y ? clickDownCoords.Y + selectionPanningOffset.Y : dragMouseCoords.Y
		);

		MarqueeSize = FVector2D(
			(clickDownCoords.X + selectionPanningOffset.X) < dragMouseCoords.X ?
			(dragMouseCoords.X - clickDownCoords.X) - selectionPanningOffset.X :
			clickDownCoords.X - dragMouseCoords.X + selectionPanningOffset.X
			,
			(clickDownCoords.Y + selectionPanningOffset.Y) < dragMouseCoords.Y ?
			(dragMouseCoords.Y - clickDownCoords.Y) - selectionPanningOffset.Y :
			clickDownCoords.Y - dragMouseCoords.Y + selectionPanningOffset.Y
		);

		MarqueeEndPoint = MarqueePointOfOrigin + MarqueeSize;

		const FSlateRect MarqueeSlateRect(MarqueePointOfOrigin, MarqueeEndPoint);

		TArray<int32> SelectedNodeIds;
		TArray<int32> OriginallySelectedNodes = m_OriginallySelectedNodes;

		// Finds nodes affected by marquee selection
		for (int32 i = 0; i < DialogueTree->Nodes.Num(); i++)
		{
			const FDialogueNode& Node = DialogueTree->Nodes[i];

			FVector2D NodeTopLeftPoint = (Node.Coordinates + panningOffset) * GetZoomAmount() - (NodeWidgets[i]->NodeSize / 2);
			FVector2D NodeBottomRightPoint = NodeTopLeftPoint + NodeWidgets[i]->NodeSize;
			FSlateRect NodeRect = FSlateRect(NodeTopLeftPoint, NodeBottomRightPoint);

			if (FSlateRect::DoRectanglesIntersect(MarqueeSlateRect, NodeRect))
			{
				SelectedNodeIds.Add(Node.Id);
			}
		}

		// if we're selecting with Shift, we add nodes to current selection. If with Ctrl, we remove them from selection.
		if (CurrentSelection == ESelectionType::WithShift)
		{
			for (auto originallySelectedNode : m_OriginallySelectedNodes)
			{
				SelectedNodeIds.AddUnique(originallySelectedNode);
			}
		}
		else if (CurrentSelection == ESelectionType::WithCtrl)
		{
			for (auto curSelectedNode : SelectedNodeIds)
			{
				OriginallySelectedNodes.Remove(curSelectedNode);
			}
			SelectedNodeIds = OriginallySelectedNodes;
		}

		SelectNodes(SelectedNodeIds);
	}

	return FCursorReply::Unhandled();
}

int32 SUnboundDialogueViewportWidget::OnPaint(const FPaintArgs& Args, const FGeometry& AllottedGeometry, const FSlateRect& MyClippingRect, FSlateWindowElementList& OutDrawElements, int32 LayerId, const FWidgetStyle& InWidgetStyle, bool bParentEnabled) const
{
	int32 maxLayerId = SCompoundWidget::OnPaint(Args, AllottedGeometry, MyClippingRect, OutDrawElements, LayerId, InWidgetStyle, bParentEnabled);

	const FSlateBrush* BackgroundImage = FEditorStyle::GetBrush(TEXT("Graph.Panel.SolidBackground"));
	PaintBackgroundAsLines(BackgroundImage, AllottedGeometry, MyClippingRect, OutDrawElements, LayerId);

	LayerId -= 4; // @TODO: experiment with this value... set it to -=2, -=1 etc to see how it goes.

	// Draw software cursor
	if (bShowSoftwareCursor)
	{
		const FSlateBrush* Brush = FCoreStyle::Get().GetBrush(TEXT("SoftwareCursor_Grab"));

		FSlateDrawElement::MakeBox(
			OutDrawElements,
			maxLayerId, // draws the mouse cursor on top of all nodes
			AllottedGeometry.ToPaintGeometry(SoftwareCursorPosition - (Brush->ImageSize / 2), Brush->ImageSize),
			Brush
		);
	}

	// Display a marquee box when selecting multiple nodes
	if (bIsSelectingMultipleNodes)
	{
		FSlateDrawElement::MakeBox(
			OutDrawElements,
			LayerId + 3,
			AllottedGeometry.ToPaintGeometry(MarqueeSize, FSlateLayoutTransform(MarqueePointOfOrigin)),
			FEditorStyle::GetBrush("FocusRectangle"), // or MarqueeSelection ?
			ESlateDrawEffect::None,
			FColor(255, 255, 255, 255)
		);
	}

	// if we're in Play mode, display a yellow border inside the window
	if (ShowGraphStateOverlay.Get())
	{
		const FSlateBrush* BorderBrush = nullptr;
		if ((GEditor->bIsSimulatingInEditor || GEditor->PlayWorld != nullptr))
		{
			// Draw a surrounding indicator when PIE is active, to make it clear that the graph is read-only, etc...
			BorderBrush = FEditorStyle::GetBrush(TEXT("Graph.PlayInEditor"));
		}
		else if (!IsEditable.Get())
		{
			// Draw a different border when we're not simulating but the graph is read-only
			BorderBrush = FEditorStyle::GetBrush(TEXT("Graph.ReadOnlyBorder"));
		}

		if (BorderBrush != nullptr)
		{
			// Actually draw the border
			FSlateDrawElement::MakeBox(
				OutDrawElements,
				maxLayerId,
				AllottedGeometry.ToPaintGeometry(),
				BorderBrush
			);
		}
	}

	// draw a shadow overlay around the edges of the screen
	FSlateDrawElement::MakeBox(
		OutDrawElements,
		maxLayerId,
		AllottedGeometry.ToPaintGeometry(),
		FEditorStyle::GetBrush(TEXT("Graph.Shadow"))
	);

	// draw lines and arrows between nodes, representing links
	for (int i = 0; i < DialogueTree->Nodes.Num(); i++) //for each dialogue node
	{
		const FDialogueNode& Node = DialogueTree->Nodes[i];
		const FVector2D SourceNodeSize = NodeWidgets[i]->NodeSize;

		// Draw node shadow/selection overlay for this node
		if (isNodeVisible[i])
		{
			FVector2D overlaySize = NodeWidgets[i]->NodeSize + FVector2D(24.f, 24.f);
			FVector2D upperLeft = (Node.Coordinates + panningOffset) * GetZoomAmount() - (overlaySize / 2.f);

			FSlateDrawElement::MakeBox(
				OutDrawElements,
				LayerId + 5, // @TODO: why +5? Try different values?
				AllottedGeometry.ToPaintGeometry(overlaySize, FSlateLayoutTransform(upperLeft)),
				FEditorStyle::GetBrush(NodeWidgets[i]->bIsSelected ? TEXT("Graph.Node.ShadowSelected") : TEXT("Graph.Node.Shadow"))
			);
		}

		/*
		*	Draw Links
		*/
		for (int32 Link : Node.LinkIds)
		{
			int32 linkIndex = NodeIdsToIndexes.FindRef(Link);

			if (linkIndex != -1)
			{
				const FVector2D& TargetNodeSize = NodeWidgets[linkIndex]->NodeSize;
				const FVector2D& NodeCoords = Node.Coordinates;
				const FVector2D& LinkCoordinates = DialogueTree->Nodes[linkIndex].Coordinates;

				// If the child node is above the parent then draw a spline else draw a line.
				const bool bAboveParent = Node.Coordinates.Y > DialogueTree->Nodes[linkIndex].Coordinates.Y;

				if (bAboveParent)
				{
					//Draw spline.

					FVector2D startPoint = FVector2D(
						(NodeCoords.X + panningOffset.X - 8) * GetZoomAmount() + (SourceNodeSize.X / 2),
						(NodeCoords.Y + panningOffset.Y) * GetZoomAmount());

					FVector2D endPoint(	(LinkCoordinates.X + panningOffset.X + 3) * GetZoomAmount() - (TargetNodeSize.X / 2),
										(LinkCoordinates.Y + panningOffset.Y) * GetZoomAmount());

					FLinearColor drawColor = FLinearColor::White;

					// 1. if only one node is selected and if this node is the one selected, then draw blue lines (outgoing)
					if (SelectedNodes.Num() == 1 && SelectedNodes[0] == Node.Id)
					{
						drawColor = FLinearColor(0.013575f, 0.770000f, 0.429609f, 1.0f);
					}
					// 2. else if only one node is selected if this is the node that's being linked, draw red lines (incoming)
					else if (SelectedNodes.Num() == 1 && SelectedNodes[0] == Link)
					{
						drawColor = FLinearColor(0.607717f, 0.224984f, 1.0f, 1.0f); // lilac
					}
					// 3. else, for all other lines, draw gray lines if DisplayIdleSplies is checked
					else if (DialogueTree->DisplayIdleSplines && CurrentZoomLevel > 0)
					{
						drawColor = FLinearColor(0.220000f, 0.195800f, 0.195800f, 0.2f); // grey
					}

					FSlateDrawElement::MakeSpline(
						OutDrawElements,
						LayerId + 3,
						AllottedGeometry.ToPaintGeometry(),
						startPoint, ComputeSplineTangent(startPoint, endPoint),
						endPoint, ComputeSplineTangent(startPoint, endPoint),
						2.0f,
						ESlateDrawEffect::None,
						drawColor);
				}
				else
				{
					//Draw line.

					//Array of line points.
					TArray<FVector2D> LinePoints = TArray<FVector2D>();
					//Reserve space for 2 points to save a memory reallocation.
					LinePoints.Reserve(2);

					//Start point of the line.
					LinePoints.Add(FVector2D((NodeCoords.X + panningOffset.X) * GetZoomAmount(),
											 (NodeCoords.Y + panningOffset.Y - 3) * GetZoomAmount() + SourceNodeSize.Y / 2));

					//End point of the line.
					LinePoints.Add(FVector2D((LinkCoordinates.X + panningOffset.X) * GetZoomAmount(),
												   (LinkCoordinates.Y + panningOffset.Y) * GetZoomAmount() - TargetNodeSize.Y / 2));

					FSlateDrawElement::MakeLines(
						OutDrawElements,
						LayerId + 4,
						AllottedGeometry.ToPaintGeometry(),
						LinePoints,
						ESlateDrawEffect::None,
						FLinearColor::White
					);

					// Draw an arrow in the middle of the line if we are zoomed in far enough.
					if (CurrentLOD >= EDialogueRenderingLOD::MediumDetail) 
					{
						//Rotation of the arrow relative to the line.
						const float ArrowRotation = 20.f;

						FVector2D LineMidPoint = LinePoints[0] + ((LinePoints[1] - LinePoints[0]) * 0.5);
						FVector2D Difference = LinePoints[0] - LinePoints[1];

						//Array of arrow points.
						TArray<FVector2D> ArrowPoints;
						ArrowPoints.Reserve(2);

						ArrowPoints.Add(LineMidPoint);

						/*
						*	Draw first arrow point.
						*/
						FVector2D LeftArrowPoint = Difference.GetRotated(-ArrowRotation);
						LeftArrowPoint.Normalize();

						LeftArrowPoint = LineMidPoint + LeftArrowPoint * 15 * GetZoomAmount();
						ArrowPoints.Add(LeftArrowPoint);
						
						FSlateDrawElement::MakeLines(
							OutDrawElements,
							LayerId + 4,
							AllottedGeometry.ToPaintGeometry(),
							ArrowPoints,
							ESlateDrawEffect::None,
							FLinearColor::White
						);

						/*
						*	Draw second arrow point.
						*/
						FVector2D RightArrowPoint = Difference.GetRotated(ArrowRotation);
						RightArrowPoint.Normalize();

						RightArrowPoint = LineMidPoint + RightArrowPoint * 15 * GetZoomAmount();
						ArrowPoints[1] = RightArrowPoint; //Just replace the LeftArrowPoint since we already have the index allocated.

						FSlateDrawElement::MakeLines(
							OutDrawElements,
							LayerId + 4,
							AllottedGeometry.ToPaintGeometry(),
							ArrowPoints,
							ESlateDrawEffect::None,
							FLinearColor::White
						);
					}
				}
			}
		}
	}

	/* 
	*	Draw linking line if we're in linking mode
	*/
	if (DialogueTreeEditor.Pin()->IsLinking)
	{
		TArray<FVector2D> LinkingPoints;

		FVector2D& SourceNodeSize = NodeWidgets[DialogueTreeEditor.Pin()->LinkingFromIndex]->NodeSize;
		FVector2D& SourceCoords = DialogueTree->Nodes[DialogueTreeEditor.Pin()->LinkingFromIndex].Coordinates;

		FVector2D LinkStartPoint = FVector2D((SourceCoords.X + panningOffset.X) * GetZoomAmount(),
											 (SourceCoords.Y + panningOffset.Y - 3) * GetZoomAmount() + SourceNodeSize.Y / 2);

		//Add the startpoint of the linking (SourceNode)
		LinkingPoints.Add(LinkStartPoint);

		//Add the current end point (Mouse cursor)
		LinkingPoints.Add(DialogueTreeEditor.Pin()->LinkingCoords);

		FSlateDrawElement::MakeLines(
			OutDrawElements,
			LayerId + 4,
			AllottedGeometry.ToPaintGeometry(),
			LinkingPoints,
			ESlateDrawEffect::None,
			FLinearColor::White
		);
	}

	return maxLayerId;
}

FReply SUnboundDialogueViewportWidget::OnMouseMove(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent)
{
	if (bIsSelectingMultipleNodes)
	{
		dragMouseCoords = MyGeometry.AbsoluteToLocal(MouseEvent.GetScreenSpacePosition());
	}

	FReply Reply = FReply::Handled();

	if (bIsPanning)
	{
		panningOffset += MouseEvent.GetCursorDelta() / GetZoomAmount();

		// Capture the mouse if we haven't done it yet
		if (this->HasMouseCapture() == false)
		{
			Reply.CaptureMouse(AsShared()).UseHighPrecisionMouseMovement(AsShared());
			SoftwareCursorPosition = MyGeometry.AbsoluteToLocal(MouseEvent.GetScreenSpacePosition());
			bShowSoftwareCursor = true;
		}

		SoftwareCursorPosition += MouseEvent.GetCursorDelta();
	}

	if (DraggedNodeIndex != -1)
	{
		if (this->HasMouseCapture() == false)
		{
			Reply.CaptureMouse(AsShared());
		}

		FVector2D currentMouseCoords = MouseEvent.GetScreenSpacePosition();
		FSlateRect PanelScreenSpaceRect = MyGeometry.GetLayoutBoundingRect();

		UpdateAutoPanningDirection(currentMouseCoords, PanelScreenSpaceRect);

		FIntPoint BestPositionInPanel(
			FMath::RoundToInt(FMath::Clamp(currentMouseCoords.X, PanelScreenSpaceRect.Left, PanelScreenSpaceRect.Right)),
			FMath::RoundToInt(FMath::Clamp(currentMouseCoords.Y, PanelScreenSpaceRect.Top, PanelScreenSpaceRect.Bottom))
		);

		Reply.SetMousePos(BestPositionInPanel);
	}

	if (bIsSelectingMultipleNodes)
	{
		if (this->HasMouseCapture() == false)
		{
			Reply.CaptureMouse(AsShared());
		}

		FVector2D currentMouseCoords = MouseEvent.GetScreenSpacePosition();
		FSlateRect PanelScreenSpaceRect = MyGeometry.GetLayoutBoundingRect();

		UpdateAutoPanningDirection(currentMouseCoords, PanelScreenSpaceRect);

		FIntPoint BestPositionInPanel(
			FMath::RoundToInt(FMath::Clamp(currentMouseCoords.X, PanelScreenSpaceRect.Left, PanelScreenSpaceRect.Right)),
			FMath::RoundToInt(FMath::Clamp(currentMouseCoords.Y, PanelScreenSpaceRect.Top, PanelScreenSpaceRect.Bottom))
		);

		Reply.SetMousePos(BestPositionInPanel);
	}

	if (DialogueTreeEditor.Pin()->IsLinking)
	{
		// We're still linking, but no node under the cursor. Cancelling linking.
		if (!bIsLinkingAndCapturing)
		{
			DialogueTreeEditor.Pin()->IsLinking = false;
			return Reply;
		}

		if (bIsLinkingAndCapturing && this->HasMouseCapture() == false)
		{
			Reply.CaptureMouse(AsShared());
		}

		DialogueTreeEditor.Pin()->LinkingCoords = MyGeometry.AbsoluteToLocal(MouseEvent.GetScreenSpacePosition());
		FVector2D currentMouseCoords = MouseEvent.GetScreenSpacePosition();
		FSlateRect PanelScreenSpaceRect = MyGeometry.GetLayoutBoundingRect();

		UpdateAutoPanningDirection(currentMouseCoords, PanelScreenSpaceRect);

		FIntPoint BestPositionInPanel(
			FMath::RoundToInt(FMath::Clamp(currentMouseCoords.X, PanelScreenSpaceRect.Left, PanelScreenSpaceRect.Right)),
			FMath::RoundToInt(FMath::Clamp(currentMouseCoords.Y, PanelScreenSpaceRect.Top, PanelScreenSpaceRect.Bottom))
		);

		Reply.SetMousePos(BestPositionInPanel);
	}

	return Reply;
}

FReply SUnboundDialogueViewportWidget::OnMouseButtonUp(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent)
{
	EditingTextOfNode = -1;

	clickUpCoords = MyGeometry.AbsoluteToLocal(MouseEvent.GetScreenSpacePosition());
	FReply Reply = FReply::Unhandled();

	if (DialogueTreeEditor.Pin()->IsLinking)
	{
		if (MouseEvent.GetEffectingButton() == EKeys::RightMouseButton) // in case of RMB, we also cancel linking
		{
			DialogueTreeEditor.Pin()->IsLinking = false;
		}

		// cancel capture, which will result in either of two scenarios
		// 1) NodeWidget will get entered and will result in an attempt to link nodes
		// 2) or DialogueViewportWidget::OnMouseMove will cancel linking, seeing as no node was under the cursor
		// Potential problem: OnMouseMove will not trigger until the mouse returns to the screen. Is it a problem? Test it.
		if (bIsLinkingAndCapturing && this->HasMouseCapture())
		{
			bIsLinkingAndCapturing = false;
			SelectUnderCursor();
			return FReply::Handled().ReleaseMouseCapture(); // an alternative is FSlateApplication::Get().ReleaseMouseCapture();
		}
	}

	if (MouseEvent.GetEffectingButton() == EKeys::LeftMouseButton)
	{
		if (bFlagForRefresh)
			ForceRefresh();

		if (bBreakingLinksMode)
		{
			bBreakingLinksMode = false;
			return Reply;
		}

		if (DraggedNodeIndex != -1)
		{
			// TODO: this may result in quite a resort due to duplicates. We can drag 5 children of the same parent, and the result is that we'll resort the parent 5 times. Write a method optimizing it in the future.
			for (auto nodeId : SelectedNodes)
			{
				int32 nodeIndex = NodeIdsToIndexes.FindRef(nodeId);
				NodeWidgets[nodeIndex]->SortParentsLinks();
			}
			DraggedNodeIndex = -1;
			return FReply::Handled().ReleaseMouseCapture();
		}

		if (bIsSelectingMultipleNodes)
		{
			bIsSelectingMultipleNodes = false;
			return FReply::Handled().ReleaseMouseCapture();
		}
		else
		{
			if (MouseEvent.IsShiftDown())
			{
				CurrentSelection = ESelectionType::WithShift;
			}
			else if (MouseEvent.IsControlDown())
			{
				CurrentSelection = ESelectionType::WithCtrl;
			}
			else
			{
				CurrentSelection = ESelectionType::Default;
			}
			SelectUnderCursor();
		}
	}
	if (MouseEvent.GetEffectingButton() == EKeys::RightMouseButton)
	{
		if (bIsPanning)
		{
			bIsPanning = false;
			Reply = FReply::Handled().ReleaseMouseCapture();

			bShowSoftwareCursor = false;

			FSlateRect PanelScreenSpaceRect = MyGeometry.GetLayoutBoundingRect();
			FVector2D CursorPosition = MyGeometry.LocalToAbsolute(SoftwareCursorPosition);

			FIntPoint BestPositionInPanel(
				FMath::RoundToInt(FMath::Clamp(CursorPosition.X, PanelScreenSpaceRect.Left, PanelScreenSpaceRect.Right)),
				FMath::RoundToInt(FMath::Clamp(CursorPosition.Y, PanelScreenSpaceRect.Top, PanelScreenSpaceRect.Bottom))
			);

			Reply.SetMousePos(BestPositionInPanel);
		}
		else if (bBreakingLinksMode)
		{
			bBreakingLinksMode = false;
			return Reply;
		}

		if (FVector2D::Distance(clickUpCoords, clickDownCoords) > 2)
		{
			return Reply;
		}

		// If dragging a node and right clicked, do nothing.
		if (DraggedNodeIndex != -1)
		{
			return Reply;
		}

		// Right click menu when clicked on the background.
		{
			FMenuBuilder MenuBuilder(true, NULL);

			FUIAction AddNodeAction(FExecuteAction::CreateSP(this, &SUnboundDialogueViewportWidget::OnAddNodeClicked));

			MenuBuilder.BeginSection(NAME_None, NSLOCTEXT("PropertyView", "ExpansionHeading", "Dialogue"));
			MenuBuilder.AddMenuEntry(NSLOCTEXT("PropertyView", "AddNode", "Add Node"), NSLOCTEXT("PropertyView", "AddNode_ToolTip", "Adds an empty node"), FSlateIcon(), AddNodeAction);
			MenuBuilder.EndSection();

			FWidgetPath WidgetPath = MouseEvent.GetEventPath() != nullptr ? *MouseEvent.GetEventPath() : FWidgetPath();

			FSlateApplication::Get().PushMenu(AsShared(), WidgetPath, MenuBuilder.MakeWidget(), MouseEvent.GetScreenSpacePosition(), FPopupTransitionEffect::ContextMenu);
		}
	}
	return Reply;
}

FReply SUnboundDialogueViewportWidget::OnMouseButtonDown(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent)
{
	FReply Reply = FReply::Handled();

	dragMouseCoords = MyGeometry.AbsoluteToLocal(MouseEvent.GetScreenSpacePosition());
	clickDownCoords = MyGeometry.AbsoluteToLocal(MouseEvent.GetScreenSpacePosition());
	selectionPanningOffset = FVector2D(0, 0);

	if (MouseEvent.GetEffectingButton() == EKeys::LeftMouseButton)
	{
		Reply.DetectDrag(SharedThis(this), EKeys::LeftMouseButton);
	}
	if (MouseEvent.GetEffectingButton() == EKeys::RightMouseButton)
	{
		Reply.DetectDrag(SharedThis(this), EKeys::RightMouseButton);
	}

	return Reply;
}

void SUnboundDialogueViewportWidget::Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime)
{
	FVector2D CursorPos = FSlateApplication::Get().GetCursorPos();
	FVector2D UnclampedCoords = AllottedGeometry.AbsoluteToLocal(CursorPos) / GetZoomAmount() - panningOffset;

	FSlateRect PanelScreenSpaceRect = AllottedGeometry.GetLayoutBoundingRect();
	FSlateRect ScreenRectLocalAndZoomed = FSlateRect(AllottedGeometry.AbsoluteToLocal(FVector2D(PanelScreenSpaceRect.Left, PanelScreenSpaceRect.Top)) / GetZoomAmount() - panningOffset,
		AllottedGeometry.AbsoluteToLocal(FVector2D(PanelScreenSpaceRect.Right, PanelScreenSpaceRect.Bottom)) / GetZoomAmount() - panningOffset);
	
	CoordsForPasting = FIntPoint(
		FMath::RoundToInt(FMath::Clamp(UnclampedCoords.X, ScreenRectLocalAndZoomed.Left, ScreenRectLocalAndZoomed.Right)),
		FMath::RoundToInt(FMath::Clamp(UnclampedCoords.Y, ScreenRectLocalAndZoomed.Top, ScreenRectLocalAndZoomed.Bottom))
	);

	CachedLocalSize = AllottedGeometry.GetLocalSize();

	if (!bShouldTickAllWidgets)
	{
		for (int i = 0; i < NodeWidgets.Num(); i++)
		{
			/* If node is inside the viewport or near the viewport by 50 pixels.*/
			if ((NodeFSlots[i]->OffsetAttr.Get().Left + NodeWidgets[i]->NodeSize.X > -50.f
				&& NodeFSlots[i]->OffsetAttr.Get().Left - NodeWidgets[i]->NodeSize.X < CachedLocalSize.X + 50.f)
				&&
				((NodeFSlots[i]->OffsetAttr.Get().Top + NodeWidgets[i]->NodeSize.Y > -50.f
					&& NodeFSlots[i]->OffsetAttr.Get().Top - NodeWidgets[i]->NodeSize.Y < CachedLocalSize.Y + 50.f)))
			{
				if (!isNodeVisible[i])
				{
					isNodeVisible[i] = true;
					// Set a node's visible field to true. However, it'll only really become visible when it enters the bounds of the Viewport, and that's when it'll start ticking.
					NodeWidgets[i]->bIsVisible = true;
				}
			}
			else
			{
				if (isNodeVisible[i])
				{
					isNodeVisible[i] = false;
					NodeWidgets[i]->bIsVisible = false;
				}
			}
		}
	}

	// When we just opened an empty dialogue for the first time, and the first node was just created, we have to position it properly.
	// We have to do it on the first tick, when AllottedGeometry is already initialized, so that we can place the node in the top center of the screen.
	if (bFirstNodeRequiresRepositioning)
	{
		bFirstNodeRequiresRepositioning = false;

		FVector2D TopLeftCoords = AllottedGeometry.AbsoluteToLocal(FVector2D(AllottedGeometry.GetLayoutBoundingRect().Left, AllottedGeometry.GetLayoutBoundingRect().Top));
		FVector2D BottomRightCoords = AllottedGeometry.AbsoluteToLocal(FVector2D(AllottedGeometry.GetLayoutBoundingRect().Right, AllottedGeometry.GetLayoutBoundingRect().Bottom));

		float HCoord = FMath::LerpStable(TopLeftCoords.X, BottomRightCoords.X, 0.5f);
		float VCoord = FMath::LerpStable(TopLeftCoords.Y, BottomRightCoords.Y, 0.3f);
		DialogueTree->Nodes[0].Coordinates = FVector2D(HCoord, VCoord);
	}

	// Applying automatic panning when linking, dragging or selecting multiple, and the mouse is near the border.
	if ((bIsLinkingAndCapturing || DraggedNodeIndex != -1 || bIsSelectingMultipleNodes) && !AutoPanningDirection.IsZero())
	{
		panningOffset += AutoPanningDirection / GetZoomAmount() * InDeltaTime * 60;

		if (bIsSelectingMultipleNodes)
		{
			selectionPanningOffset += AutoPanningDirection * InDeltaTime * 60;
		}

		float x = fmod(panningOffset.Y, 128.0f);
		float y = fmod(panningOffset.X, 128.0f);

		bgTop = -128 + x;
		bgLeft = -128 + y;
	}

	// Activate all widgets, tick them, disable them - this allows all widgets to update their size at the new zoom level even though they're culled and are beyond the Viewport.
	if (bShouldTickAllWidgets)
	{
		for (int32 i = 0; i < DialogueTree->Nodes.Num(); i++)
		{
			NodeWidgets[i]->bIsVisible = true;
			isNodeVisible[i] = true;
			NodeWidgets[i]->SetVisibility(EVisibility::Visible);
		}

		bShouldTickAllWidgets = false;
	}
}

FReply SUnboundDialogueViewportWidget::OnDragDetected(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent)
{	
	// we're panning
	if (MouseEvent.IsMouseButtonDown(EKeys::RightMouseButton) && !DialogueTreeEditor.Pin()->IsLinking && !bIsSelectingMultipleNodes && DraggedNodeIndex == -1)
	{
		bIsPanning = true;
	}
	// we're selecting multiple
	else if (MouseEvent.IsMouseButtonDown(EKeys::LeftMouseButton) && !bIsPanning && !DialogueTreeEditor.Pin()->IsLinking && DraggedNodeIndex == -1 && !bBreakingLinksMode)
	{
		m_OriginallySelectedNodes = SelectedNodes;

		if (MouseEvent.IsShiftDown())
		{
			CurrentSelection = ESelectionType::WithShift;
		}
		else if (MouseEvent.IsControlDown())
		{
			CurrentSelection = ESelectionType::WithCtrl;
		}
		else
		{
			CurrentSelection = ESelectionType::Default;
		}
		bIsSelectingMultipleNodes = true;
		ForceSlateToStayAwake();
	}

	return FReply::Unhandled();
}

FReply SUnboundDialogueViewportWidget::OnKeyDown(const FGeometry& MyGeometry, const FKeyEvent& InKeyEvent)
{
	if (DialogueTreeEditor.Pin()->ProcessCommandBindings(InKeyEvent))
	{
		return FReply::Handled();
	}

	return SCompoundWidget::OnKeyDown(MyGeometry, InKeyEvent);
}

void SUnboundDialogueViewportWidget::OnFocusLost(const FFocusEvent& InFocusEvent)
{
}

FReply SUnboundDialogueViewportWidget::OnFocusReceived(const FGeometry& MyGeometry, const FFocusEvent& InFocusEvent)
{
	return FReply::Unhandled();
}

void SUnboundDialogueViewportWidget::SpawnNodes(int32 IdToFocus)
{
	CanvasPanel->ClearChildren();
	NodeFSlots.Empty();
	NodeWidgets.Empty();
	isNodeVisible.Empty();
	NodeIdsToIndexes.Empty();

	/*
	*	Spawn Start node if it doesnt already exist.
	*/
	if (!DialogueTree->Nodes.IsValidIndex(0))
	{
		int32 Index = DialogueTreeEditor.Pin()->AddNode(FText::GetEmpty(), true, FVector2D::ZeroVector, false);

		bFirstNodeRequiresRepositioning = true;
	}

	for (int32 i = 0; i < DialogueTree->Nodes.Num(); i++)
	{
		const FDialogueNode& Node = DialogueTree->Nodes[i];

		TSharedPtr<class SUnboundDialogueNodeWidget> WidgetReference;

		SConstraintCanvas::FSlot* tempSlot = &CanvasPanel->AddSlot()
			.Offset(TAttribute<FMargin>::Create(TAttribute<FMargin>::FGetter::CreateSP(this, &SUnboundDialogueViewportWidget::GetOffset, i)))
			.AutoSize(true)
			[
				SAssignNew(WidgetReference, SUnboundDialogueNodeWidget)
				.Id(Node.Id)
				.NodeIndex(i)
				.DialogueTree(DialogueTree)
				.Owner(this)
			];

		isNodeVisible.Add(true);
		NodeFSlots.Add(tempSlot);
		NodeWidgets.Add(WidgetReference);

		//Cache index of node.
		NodeIdsToIndexes.Add(Node.Id, i);
	}

	// Bottom-right corner text indicating the type of tool: DIALOGUE
	CanvasPanel->AddSlot()
		.Anchors(FAnchors(1, 1, 1, 1))
		.AutoSize(true)
		.Alignment(FVector2D(1, 1))
		.Offset(FMargin(-10.0f, -10.0f))
		[
			SNew(STextBlock)
			.Visibility(EVisibility::HitTestInvisible)
			.TextStyle(FEditorStyle::Get(), "Graph.CornerText")
			.Text(FText::FromString(TEXT("DIALOGUE")))
		];

	// Top-right corner text indicating PIE is active
	CanvasPanel->AddSlot()
		.Anchors(FAnchors(1, 0, 1, 0))
		.AutoSize(true)
		.Alignment(FVector2D(1, 0))
		.Offset(FMargin(-20.f, 20.f))
		[
			SNew(STextBlock)
			.Visibility(this, &SUnboundDialogueViewportWidget::IsSimulating)
			.TextStyle(FEditorStyle::Get(), "Graph.SimulatingText")
			.Text(FText::FromString(TEXT("SIMULATING")))
		];

	// Indicator of current zoom level
	CanvasPanel->AddSlot()
		.Anchors(FAnchors(1, 0, 1, 0))
		.AutoSize(true)
		.Alignment(FVector2D(1, 0))
		.Offset(FMargin(-5.f, 5.f))
		[
			SNew(STextBlock)
			.TextStyle(FEditorStyle::Get(), "Graph.ZoomText")
			.Text(this, &SUnboundDialogueViewportWidget::GetZoomText)
			.ColorAndOpacity(this, &SUnboundDialogueViewportWidget::GetZoomTextColorAndOpacity)
		];

	// if we just respawned nodes and we need to focus a node (because we just created it, for example)
	if (IdToFocus != 0)
	{
		SelectNodes(IdToFocus);
		DialogueTree->SelectedNodeId = IdToFocus;
		ForceRefresh();
	}

	bShouldTickAllWidgets = true;
}

void SUnboundDialogueViewportWidget::SelectNodes(TArray<int32> NodesIds) const
{
	for (int32 NodeId : SelectedNodes)
	{
		int32 nodeIndex = NodeIdsToIndexes.FindRef(NodeId);
		if (nodeIndex != -1) // prevents issues for window draws while undoing/redoing
		{
			NodeWidgets[nodeIndex]->bIsSelected = false;
		}
	}
	SelectedNodes.Empty();

	SelectedNodes = NodesIds;

	for (int32 NodeId : SelectedNodes)
	{
		int32 nodeIndex = NodeIdsToIndexes.FindRef(NodeId);
		if (nodeIndex != -1) // prevents issues for window draws while undoing/redoing
		{
			NodeWidgets[nodeIndex]->bIsSelected = true;
		}

	}

	if (SelectedNodes.Num() > 1)
	{
		DialogueTree->SelectedNodeId = -1;
	}
	else if (SelectedNodes.Num() == 1)
	{
		DialogueTree->SelectedNodeId = SelectedNodes[0];
		bFlagForRefresh = true;
	}
	else if (SelectedNodes.Num() == 0)
	{
		DialogueTree->SelectedNodeId = -1;
	}
}

void SUnboundDialogueViewportWidget::SelectNodes(int32 NodeId) const
{
	TArray<int32> NodeToSelectAsArray;
	NodeToSelectAsArray.Add(NodeId);

	SelectNodes(NodeToSelectAsArray);
}

void SUnboundDialogueViewportWidget::SelectUnderCursor() const
{
	for (int32 i = 0; i < DialogueTree->Nodes.Num(); i++)
	{
		const FDialogueNode& Node = DialogueTree->Nodes[i];

		// Determening node rect
		FVector2D NodeTopLeftPoint = (Node.Coordinates + panningOffset) * GetZoomAmount() - (NodeWidgets[i]->NodeSize / 2);
		FVector2D NodeBottomRightPoint = (Node.Coordinates + panningOffset) * GetZoomAmount() + (NodeWidgets[i]->NodeSize / 2);
		FSlateRect NodeRect = FSlateRect(NodeTopLeftPoint, NodeBottomRightPoint);

		if (NodeRect.ContainsPoint(clickUpCoords))
		{
			if ((CurrentSelection == ESelectionType::WithShift || CurrentSelection == ESelectionType::WithCtrl) && !SelectedNodes.Contains(Node.Id))
			{
				SelectedNodes.Add(Node.Id);
				NodeWidgets[i]->bIsSelected = true;
			}
			else if (CurrentSelection == ESelectionType::WithCtrl && SelectedNodes.Contains(Node.Id))
			{
				SelectedNodes.Remove(Node.Id);
				NodeWidgets[i]->bIsSelected = false;
			}
			else if (CurrentSelection == ESelectionType::Default)
			{
				SelectNodes(Node.Id);
			}

			ForceRefresh();
			return;
		}
	}

	DeselectAllNodes();
}

void SUnboundDialogueViewportWidget::DeselectNode(int32 NodeId) const
{
	int32 NodeIndex = NodeIdsToIndexes.FindRef(NodeId);
	if (NodeIndex != -1)
	{
		NodeWidgets[NodeIndex]->bIsSelected = false;
	}

	SelectedNodes.Remove(NodeId);
}

void SUnboundDialogueViewportWidget::DeselectAllNodes() const
{
	SelectNodes(TArray<int32>());
}

void SUnboundDialogueViewportWidget::StartDraggingIndex(int32 NodeIndex)
{
	// if we're currently dragging and we don't have any nodes selected, select this one and keep dragging it
	// if we're dragging, but have nodes selected:
	// if our current node is part of the selected nodes, drag all nodes, don't change selection
	// if not, then lose that old selection, select our current node and drag only it.
	if (!SelectedNodes.IsValidIndex(0))
	{
		SelectNodes(DialogueTree->Nodes[NodeIndex].Id);
	}
	else
	{
		if (!SelectedNodes.Contains(DialogueTree->Nodes[NodeIndex].Id))
		{
			DeselectAllNodes();
			SelectNodes(DialogueTree->Nodes[NodeIndex].Id);
			ForceRefresh();
		}
	}
	DraggedNodeIndex = NodeIndex;
	ForceSlateToStayAwake();
}

void SUnboundDialogueViewportWidget::ForceRefresh() const
{
	DialogueTreeEditor.Pin()->RefreshDetails = true;
}

void SUnboundDialogueViewportWidget::DeleteSelected()
{
	// Check so we actually selected any nodes.
	if (SelectedNodes.Num() == 0)
	{
		return;
	}

	const FScopedTransaction Transaction(LOCTEXT("DeleteNodes", "Delete Nodes"));
	DialogueTree->Modify();
	DialogueTree->DialogueTreeData->Modify();
	
	for (int32 SelectedId : SelectedNodes)
	{
		DialogueTreeEditor.Pin()->RemoveNode(SelectedId, false);
	}

	DialogueTree->SelectedNodeId = -1;
	SpawnNodes();
	ForceRefresh();
}

void SUnboundDialogueViewportWidget::DeleteOneNode(int32 NodeId)
{
	// we never delete the first node
	if (NodeId == 0)
	{
		return;
	}

	const FScopedTransaction Transaction(LOCTEXT("DeleteNode", "Delete Node"));
	DialogueTree->Modify();

	int32 Index = NodeIdsToIndexes.FindRef(NodeId);

	DialogueTree->SelectedNodeId = -1;
	NodeWidgets[Index]->OnDeleteNode(true); // it also calls SpawnNodes()
	ForceRefresh();
}

EActiveTimerReturnType SUnboundDialogueViewportWidget::EmptyTimer(double InCurrentTime, float InDeltaTime)
{
	if (DialogueTreeEditor.Pin()->IsLinking || DraggedNodeIndex != -1 || bIsSelectingMultipleNodes)
		return EActiveTimerReturnType::Continue;
	else
		return EActiveTimerReturnType::Stop;
}

float SUnboundDialogueViewportWidget::GetZoomAmount() const
{
	return ZoomLevels::GetZoomAmount(CurrentZoomLevel);
}

FText SUnboundDialogueViewportWidget::GetZoomText() const
{
	return ZoomLevels::GetZoomText(CurrentZoomLevel);
}

FSlateColor SUnboundDialogueViewportWidget::GetZoomTextColorAndOpacity() const
{
	return FLinearColor(1, 1, 1, 1.25f - ZoomLevelFade.GetLerp());
}

inline float FancyMod(float Value, float Size)
{
	return ((Value >= 0) ? 0.0f : Size) + FMath::Fmod(Value, Size);
}

FVector2D SUnboundDialogueViewportWidget::GetViewOffset() const
{
	return -panningOffset;
}

FSlateFontInfo SUnboundDialogueViewportWidget::GetNodeFont()
{
	return NodeFont;
}

void SUnboundDialogueViewportWidget::UpdateFontInfo()
{
	NodeFont = FSlateFontInfo(FPaths::EngineContentDir() / TEXT("Slate/Fonts/Roboto-Regular.ttf"), fontSize);
}

float SUnboundDialogueViewportWidget::GetNodeTextWrapLength()
{
	return NodeTextWrapLength;
}

FVector2D SUnboundDialogueViewportWidget::GetNodeMinSize()
{
	return NodeMinSize;
}

void SUnboundDialogueViewportWidget::ForceSlateToStayAwake()
{
	SWidget::RegisterActiveTimer(0.f, FWidgetActiveTimerDelegate::CreateSP(this, &SUnboundDialogueViewportWidget::EmptyTimer));
}

TOptional<FSlateRenderTransform> SUnboundDialogueViewportWidget::GetIconScale()
{
	return TOptional<FSlateRenderTransform>(FSlateRenderTransform(GetZoomAmount()));
}

FMargin SUnboundDialogueViewportWidget::GetLeftCornerPadding()
{
	return FMargin(5 * GetZoomAmount(), 0, 0, 0);
}

int32 SUnboundDialogueViewportWidget::GetZoomLevel() const
{
	return CurrentZoomLevel;
}

void SUnboundDialogueViewportWidget::DuplicateSelected()
{
	if (SelectedNodes.Num() > 0)
	{
		CopySelected();
		PasteNodes();
	}
	else
	{
		UE_LOG(LogTemp, Log, TEXT("Log: No nodes selected to duplicate."));
	}
}

void SUnboundDialogueViewportWidget::CopySelected()
{
	if (SelectedNodes.Num() > 0)
	{
		UE_LOG(LogTemp, Log, TEXT("Log: Copying %d nodes."), SelectedNodes.Num());

		TArray<TSharedPtr<FJsonValue>> JsonNodesArray;
		for (int32 NodeId : SelectedNodes)
		{
			// Copy node.
			FDialogueNode Node = DialogueTree->GetNodeByIndex(DialogueTree->GetNodeIndexById(NodeId));

			// Remove all links to nodes that aren't part of the copied nodes.
			Node.LinkIds.RemoveAll([this](int32& LinkedNodeId) { return !SelectedNodes.Contains(LinkedNodeId); });

			if (Node.Id != -1)
			{
				// Serializes all fields
				TSharedPtr<FJsonObject> JsonNode = FJsonObjectConverter::UStructToJsonObject(Node, 0, 0, nullptr);

				// overwrites conditions and events as binary, because otherwise they're serialized as pointers to instances of conditions/events
				SerializeEventsConditions(Node, JsonNode);

				TSharedRef<FJsonValueObject> JsonValue = MakeShareable(new FJsonValueObject(JsonNode));
				JsonNodesArray.Add(JsonValue);
			}
		}

		TSharedPtr<FJsonObject> RootJsonObject = MakeShareable(new FJsonObject);
		RootJsonObject->SetArrayField("Nodes", JsonNodesArray);
		FString OutputString;
		TSharedRef<TJsonWriter<>> Writer = TJsonWriterFactory<>::Create(&OutputString);
		FJsonSerializer::Serialize(RootJsonObject.ToSharedRef(), Writer);
		Writer->Close();

		//UE_LOG(LogTemp, Log, TEXT("Copying: %s"), *OutputString);
		FPlatformApplicationMisc::ClipboardCopy(*OutputString);
	}
	else
	{
		UE_LOG(LogTemp, Log, TEXT("Log: No nodes selected to copy."));
	}
}

void SUnboundDialogueViewportWidget::CutSelected()
{
	if (SelectedNodes.Num() > 0)
	{
		UE_LOG(LogTemp, Log, TEXT("Log: Cutting %d nodes."), SelectedNodes.Num());
		CopySelected();
		DeleteSelected();
	}
	else
	{
		UE_LOG(LogTemp, Log, TEXT("Log: No nodes selected to cut."));
	}
}

void SUnboundDialogueViewportWidget::PasteNodes()
{
	FString TextToImport;
	FPlatformApplicationMisc::ClipboardPaste(TextToImport);

	TSharedPtr<FJsonObject> JsonObj = MakeShareable(new FJsonObject());
	TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(*TextToImport);
	if (FJsonSerializer::Deserialize(Reader, JsonObj))
	{
		TArray<TSharedPtr<FJsonValue>> JsonNodes = JsonObj->GetArrayField("Nodes");
		if (JsonNodes.Num() > 0)
		{
			TArray<FDialogueNode> DeserializedNodes;
			for (int i = 0; i < JsonNodes.Num(); i++)
			{
				const TSharedPtr<FJsonObject>* JsonNode;
				if (!JsonNodes[i]->TryGetObject(JsonNode))
					continue;

				FDialogueNode NewStructNode;
				if (!FJsonObjectConverter::JsonObjectToUStruct(JsonNode->ToSharedRef(), &NewStructNode, 0, 0))
					continue;

				DeserializeEventsConditions(NewStructNode, *JsonNode);

				DeserializedNodes.Add(NewStructNode);
			}

			if (DeserializedNodes.Num() > 0)
			{
				UE_LOG(LogTemp, Log, TEXT("Log: Pasting %d nodes."), DeserializedNodes.Num());
				const FScopedTransaction Transaction(LOCTEXT("PasteNodes", "Paste Nodes"));
				DialogueTree->Modify();

				// give nodes new unique IDs
				for (int i = 0; i < DeserializedNodes.Num(); i++)
				{
					int32 NodeOldId = DeserializedNodes[i].Id;
					int32 NodeNewId = DialogueTree->NextNodeId;
					DialogueTree->NextNodeId++;

					DeserializedNodes[i].Id = NodeNewId;

					// as soon as an id has changed, go through all nodes and update links to this node
					for (int j = 0; j < DeserializedNodes.Num(); j++)
					{
						if (DeserializedNodes[j].LinkIds.Remove(NodeOldId) > 0)
							DeserializedNodes[j].LinkIds.Add(NodeNewId);
					}
				}

				// find the gravitational center of all nodes that we're pasting
				int cycle = 1;
				float sumX = DeserializedNodes[0].Coordinates.X;
				float sumY = DeserializedNodes[0].Coordinates.Y;
				for (int i = 1; i < DeserializedNodes.Num(); i++)
				{
					float multiplierForPreviousSum = (float)i / ((float)i + 1.f);

					sumX *= multiplierForPreviousSum;
					sumX += DeserializedNodes[i].Coordinates.X / ((float)i + 1.f);

					sumY *= multiplierForPreviousSum;
					sumY += DeserializedNodes[i].Coordinates.Y / ((float)i + 1.f);
				}

				int32 AdjustX = CoordsForPasting.X - sumX;
				int32 AdjustY = CoordsForPasting.Y - sumY;

				// adjust their coordinates, making the pasting point their new gravitational center
				for (int i = 0; i < DeserializedNodes.Num(); i++)
				{
					DeserializedNodes[i].Coordinates.X += AdjustX;
					DeserializedNodes[i].Coordinates.Y += AdjustY;
				}

				// add them to Dialogue file
				for (auto& node : DeserializedNodes)
				{
					DialogueTree->Nodes.Add(node);
				}

				// spawn nodes and select them
				SpawnNodes(DeserializedNodes.Num() == 1 ? DeserializedNodes[0].Id : -1);
				TArray<int32> temp_nodes_to_select;
				for (auto& node : DeserializedNodes)
				{
					temp_nodes_to_select.Add(node.Id);
				}
				SelectNodes(temp_nodes_to_select);
			}
		}

	}
}

void SUnboundDialogueViewportWidget::PaintBackgroundAsLines(const FSlateBrush* BackgroundImage, const FGeometry& AllottedGeometry, const FSlateRect& MyClippingRect, FSlateWindowElementList& OutDrawElements, int32& DrawLayerId) const
{
	const bool bAntialias = false;
	const int32 RulePeriod = 8; // = (int32)FEditorStyle::GetFloat("Graph.Panel.GridRulePeriod");
	check(RulePeriod > 0);

	const FLinearColor RegularColor(FEditorStyle::GetColor("Graph.Panel.GridLineColor"));
	const FLinearColor RuleColor(FEditorStyle::GetColor("Graph.Panel.GridRuleColor"));
	const FLinearColor CenterColor(FEditorStyle::GetColor("Graph.Panel.GridCenterColor"));
	const float GraphSmallestGridSize = 8.0f;
	const float NominalGridSize = GetSnapGridSize();

	float ZoomFactor = GetZoomAmount();
	float Inflation = 1.0f;
	while (ZoomFactor * Inflation * NominalGridSize <= GraphSmallestGridSize)
	{
		Inflation *= 2.0f;
	}

	const float GridCellSize = NominalGridSize * ZoomFactor * Inflation;

	const float GraphSpaceGridX = FancyMod(-panningOffset.X, Inflation * NominalGridSize * RulePeriod);
	const float GraphSpaceGridY = FancyMod(-panningOffset.Y, Inflation * NominalGridSize * RulePeriod);

	float ImageOffsetX = GraphSpaceGridX * -ZoomFactor;
	float ImageOffsetY = GraphSpaceGridY * -ZoomFactor;

	const FVector2D ZeroSpace = GraphCoordToPanelCoord(FVector2D::ZeroVector);

	// Fill the background
	FSlateDrawElement::MakeBox(
		OutDrawElements,
		DrawLayerId,
		AllottedGeometry.ToPaintGeometry(),
		BackgroundImage
	);

	TArray<FVector2D> LinePoints;
	LinePoints.AddDefaulted(2);

	// Horizontal bars
	for (int32 GridIndex = 0; ImageOffsetY < AllottedGeometry.Size.Y; ImageOffsetY += GridCellSize, ++GridIndex)
	{
		if (ImageOffsetY >= 0.0f)
		{
			const bool bIsRuleLine = (GridIndex % RulePeriod) == 0;
			const int32 Layer = bIsRuleLine ? (DrawLayerId + 1) : DrawLayerId;

			const FLinearColor* Color = bIsRuleLine ? &RuleColor : &RegularColor;

			LinePoints[0] = FVector2D(0.0f, ImageOffsetY);
			LinePoints[1] = FVector2D(AllottedGeometry.Size.X, ImageOffsetY);

			FSlateDrawElement::MakeLines(
				OutDrawElements,
				Layer,
				AllottedGeometry.ToPaintGeometry(),
				LinePoints,
				ESlateDrawEffect::None,
				*Color,
				bAntialias);
		}
	}

	// Vertical bars
	for (int32 GridIndex = 0; ImageOffsetX < AllottedGeometry.Size.X; ImageOffsetX += GridCellSize, ++GridIndex)
	{
		if (ImageOffsetX >= 0.0f)
		{
			const bool bIsRuleLine = (GridIndex % RulePeriod) == 0;
			const int32 Layer = bIsRuleLine ? (DrawLayerId + 1) : DrawLayerId;

			const FLinearColor* Color = bIsRuleLine ? &RuleColor : &RegularColor;

			LinePoints[0] = FVector2D(ImageOffsetX, 0.0f);
			LinePoints[1] = FVector2D(ImageOffsetX, AllottedGeometry.Size.Y);

			FSlateDrawElement::MakeLines(
				OutDrawElements,
				Layer,
				AllottedGeometry.ToPaintGeometry(),
				LinePoints,
				ESlateDrawEffect::None,
				*Color,
				bAntialias);
		}
	}

	DrawLayerId += 2; // puts the overlay for the first node in front of the node itself
}

FVector2D SUnboundDialogueViewportWidget::GraphCoordToPanelCoord(const FVector2D& GraphSpaceCoordinate) const
{
	return (GraphSpaceCoordinate - GetViewOffset()) * GetZoomAmount();
}

void SUnboundDialogueViewportWidget::OnAddNodeClicked()
{
	const FText DefaultText = FText::FromString(TEXT("Enter some text"));
	const bool IsPlayerResponse = false;
	const FVector2D Coordinates = clickUpCoords / GetZoomAmount() - panningOffset;

	int32 NewNodeId = DialogueTreeEditor.Pin()->AddNode(DefaultText, IsPlayerResponse, Coordinates, true);

	DialogueTree->SelectedNodeId = NewNodeId;

	//Spawn node widget.
	SpawnNodes(NewNodeId);
}

void SUnboundDialogueViewportWidget::OnIsResponseCommited(ECheckBoxState NewState)
{
	const bool NewIsResponse = (NewState == ECheckBoxState::Checked);
	DialogueTreeEditor.Pin()->UpdateNodeIsResponse(DialogueTree->SelectedNodeId, NewIsResponse);
}

void SUnboundDialogueViewportWidget::UpdateAutoPanningDirection(FVector2D& currentMouseCoords, FSlateRect& PanelScreenSpaceRect)
{
	if (currentMouseCoords.X >= PanelScreenSpaceRect.Right - 40)
	{
		float clampedX = currentMouseCoords.X;

		if (clampedX > PanelScreenSpaceRect.Right) clampedX = PanelScreenSpaceRect.Right;

		float xFromBorder = 40 - (PanelScreenSpaceRect.Right - clampedX);

		AutoPanningDirection.X = -xFromBorder;
	}
	else if (currentMouseCoords.X <= PanelScreenSpaceRect.Left + 40)
	{
		float clampedX = currentMouseCoords.X;

		if (clampedX < PanelScreenSpaceRect.Left) clampedX = PanelScreenSpaceRect.Left;

		float xFromBorder = 40 + (PanelScreenSpaceRect.Left - clampedX);

		AutoPanningDirection.X = xFromBorder;
	}
	else
	{
		AutoPanningDirection.X = 0.0f;
	}

	if (currentMouseCoords.Y >= PanelScreenSpaceRect.Bottom - 40)
	{
		float clampedY = currentMouseCoords.Y;

		if (clampedY > PanelScreenSpaceRect.Bottom) clampedY = PanelScreenSpaceRect.Bottom;

		float yFromBorder = 40 - (PanelScreenSpaceRect.Bottom - clampedY);

		AutoPanningDirection.Y = -yFromBorder;
	}
	else if (currentMouseCoords.Y <= PanelScreenSpaceRect.Top + 40)
	{
		float clampedY = currentMouseCoords.Y;

		if (clampedY < PanelScreenSpaceRect.Top) clampedY = PanelScreenSpaceRect.Top;

		float yFromBorder = 40 + (PanelScreenSpaceRect.Top - clampedY);

		AutoPanningDirection.Y = yFromBorder;
	}
	else
	{
		AutoPanningDirection.Y = 0.0f;
	}
}

void SUnboundDialogueViewportWidget::OnSelectedNodeTextCommited(const FText& InText, ETextCommit::Type)
{
	DialogueTreeEditor.Pin()->UpdateNodeText(DialogueTree->SelectedNodeId, InText);
}

FMargin SUnboundDialogueViewportWidget::GetOffset(int32 index)
{
	FMargin Output;

	const FDialogueNode& Node = DialogueTree->GetNodeByIndex(index);
	Output.Left = (Node.Coordinates.X + panningOffset.X) * GetZoomAmount();
	Output.Top = (Node.Coordinates.Y + panningOffset.Y) * GetZoomAmount();

	return Output;
}

FText SUnboundDialogueViewportWidget::GetNodeText() const
{
	if (DialogueTree->SelectedNodeId != -1)
	{
		int32 Index = DialogueTree->GetNodeIndexById(DialogueTree->SelectedNodeId);
		
		if (Index != -1)
		{
			return DialogueTree->DialogueTreeData->DialogueDatas[Index].Text;
		}
	}

	return FText::GetEmpty();
}

ECheckBoxState SUnboundDialogueViewportWidget::GetIsResponse() const
{
	if (DialogueTree->SelectedNodeId != -1)
	{
		int32 Index = DialogueTree->GetNodeIndexById(DialogueTree->SelectedNodeId);

		if (Index != -1)
		{
			if (DialogueTree->GetNodeByIndex(Index).IsPlayerResponse)
			{
				return ECheckBoxState::Checked;
			}
		}
	}

	return ECheckBoxState::Unchecked;
}

FVector2D SUnboundDialogueViewportWidget::ComputeSplineTangent(const FVector2D& Start, const FVector2D& End) const
{
	const FVector2D DeltaPos = End - Start;
	const bool bGoingForward = DeltaPos.X >= 0.0f;

	const float ClampedTensionX = FMath::Min<float>(FMath::Abs<float>(DeltaPos.X), bGoingForward ? GetDefault<UGraphEditorSettings>()->ForwardSplineHorizontalDeltaRange : GetDefault<UGraphEditorSettings>()->BackwardSplineHorizontalDeltaRange);
	const float ClampedTensionY = FMath::Min<float>(FMath::Abs<float>(DeltaPos.Y), bGoingForward ? GetDefault<UGraphEditorSettings>()->ForwardSplineVerticalDeltaRange : GetDefault<UGraphEditorSettings>()->BackwardSplineVerticalDeltaRange);

	if (bGoingForward)
	{
		return (ClampedTensionX * GetDefault<UGraphEditorSettings>()->ForwardSplineTangentFromVerticalDelta) + (ClampedTensionY * GetDefault<UGraphEditorSettings>()->ForwardSplineTangentFromVerticalDelta);
	}
	else
	{
		return (ClampedTensionX * GetDefault<UGraphEditorSettings>()->BackwardSplineTangentFromHorizontalDelta) + (ClampedTensionY * GetDefault<UGraphEditorSettings>()->BackwardSplineTangentFromVerticalDelta);
	}
}

FReply SUnboundDialogueViewportWidget::OnMouseWheel(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent)
{	
	// We want to zoom into this point; i.e. keep it the same fraction offset into the panel
	const FVector2D WidgetSpaceCursorPos = MyGeometry.AbsoluteToLocal(MouseEvent.GetScreenSpacePosition());
	const int32 ZoomLevelDelta = FMath::FloorToInt(MouseEvent.GetWheelDelta());
	ChangeZoomLevel(ZoomLevelDelta, WidgetSpaceCursorPos, MouseEvent.IsControlDown());

	return FReply::Handled();
}

void SUnboundDialogueViewportWidget::ChangeZoomLevel(int32 ZoomLevelDelta, const FVector2D& WidgetSpaceZoomOrigin, bool bOverrideZoomLimiting)
{
	// We want to zoom into this point; i.e. keep it the same fraction offset into the panel
	const FVector2D PointToMaintainGraphSpace = WidgetSpaceZoomOrigin / GetZoomAmount() + GetViewOffset();

	const int32 DefaultZoomLevel = ZoomLevels::GetDefaultZoomLevel();

	//Allow zooming in further than DefaultZoomLevel if you are holding CTRL.
	const bool bAllowFullZoomRange = (CurrentZoomLevel > DefaultZoomLevel) || bOverrideZoomLimiting;

	const int32 OldZoomLevel = CurrentZoomLevel;
	const int32 MinZoomLevel = ZoomLevels::GetMinZoomLevel();
	const int32 MaxZoomLevel = bAllowFullZoomRange ? ZoomLevels::GetMaxZoomLevel() : ZoomLevels::GetDefaultZoomLevel();

	CurrentZoomLevel = FMath::Clamp(CurrentZoomLevel + ZoomLevelDelta, MinZoomLevel, MaxZoomLevel);

	if (OldZoomLevel != CurrentZoomLevel)
	{
		PostChangedZoom();
		UpdateZoomAffectedValues();
	}

	// Note: This happens even when maxed out at a stop; so the user sees the animation and knows that they're at max zoom in/out
	ZoomLevelFade.Play(this->AsShared());

	// Re-center the screen so that it feels like zooming around the cursor.
	panningOffset = -(PointToMaintainGraphSpace - WidgetSpaceZoomOrigin / GetZoomAmount());

	bShouldTickAllWidgets = true;
}

void SUnboundDialogueViewportWidget::PostChangedZoom()
{
	CurrentLOD = ZoomLevels::GetLOD(CurrentZoomLevel);
}

void SUnboundDialogueViewportWidget::UpdateZoomAffectedValues()
{
	if (CurrentZoomLevel >= 0)
	{
		fontSize = 2 * CurrentZoomLevel + 10;
	}
	else
	{
		fontSize = CurrentZoomLevel + 10;
	}

	NodeMinSize = FVector2D(144, 60) * GetZoomAmount();

	NodeTextWrapLength = NodeMinSize.X * 1.25f; 

	UpdateFontInfo();
}

EVisibility SUnboundDialogueViewportWidget::IsSimulating() const
{
	if (ShowGraphStateOverlay.Get() && (GEditor->bIsSimulatingInEditor || GEditor->PlayWorld != NULL))
	{
		return EVisibility::Visible;
	}
	return EVisibility::Hidden;
}

void SUnboundDialogueViewportWidget::SerializeEventsConditions(const FDialogueNode& Node, TSharedPtr<FJsonObject> JsonNode)
{

}

void SUnboundDialogueViewportWidget::DeserializeEventsConditions(FDialogueNode& Node, TSharedPtr<FJsonObject> JsonNode)
{
}

#undef LOCTEXT_NAMESPACE