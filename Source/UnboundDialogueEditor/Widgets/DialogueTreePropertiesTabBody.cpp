// Fill out your copyright notice in the Description page of Project Settings.


#include "DialogueTreePropertiesTabBody.h"

#include "DialogueTreeEditor.h"
#include "DialogueTree.h"

#include "Framework/Application/SlateApplication.h"
#include "IDetailsView.h"

void SDialogueTreePropertiesTabBody::Construct(const FArguments& InArgs, TSharedPtr<class FDialogueTreeEditor> InDialogueEditor)
{
	DialogueTreeEditorPtr = InDialogueEditor;

	SSingleObjectDetailsPanel::Construct(SSingleObjectDetailsPanel::FArguments().HostCommandList(InDialogueEditor->GetToolkitCommands()), /*bAutomaticallyObserveViaGetObjectToObserve=*/ true, /*bAllowSearch=*/ true);

}

UObject* SDialogueTreePropertiesTabBody::GetObjectToObserve() const
{
	return DialogueTreeEditorPtr.Pin()->GetDialogueTree();
}

TSharedRef<SWidget> SDialogueTreePropertiesTabBody::PopulateSlot(TSharedRef<SWidget> PropertyEditorWidget)
{
	return SNew(SVerticalBox)
		+ SVerticalBox::Slot()
		.FillHeight(1)
		[
			PropertyEditorWidget
		];
}

void SDialogueTreePropertiesTabBody::Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime)
{
	if (FSlateApplication::IsInitialized() && DialogueTreeEditorPtr.Pin()->RefreshDetails)
	{
		TArray<UObject*> SelectedObjects;
		SelectedObjects.Add(GetObjectToObserve());

		PropertyView->SetObjects(SelectedObjects, true);

		DialogueTreeEditorPtr.Pin()->RefreshDetails = false;
	}
}
