// Fill out your copyright notice in the Description page of Project Settings.


#include "DialogueTreeEditorDetailsWidget.h"

#include "DialogueTree.h"
#include "DialogueTreeData.h"

#include "Widgets/Input/SMultiLineEditableTextBox.h"
#include "Widgets/Layout/SBox.h"
#include "Widgets/Text/STextBlock.h"
#include "Editor/PropertyEditor/Public/DetailLayoutBuilder.h"
#include "Editor/PropertyEditor/Public/DetailCategoryBuilder.h"
#include "Editor/UnrealEd/Public/ScopedTransaction.h"

#define LOCTEXT_NAMESPACE "DialogueTreeEditorDetailsWidget"

TSharedRef<IDetailCustomization> FDialogueTreeEditorDetailsWidget::MakeInstance()
{
	return MakeShareable(new FDialogueTreeEditorDetailsWidget());
}

void FDialogueTreeEditorDetailsWidget::CustomizeDetails(IDetailLayoutBuilder& DetailLayout)
{
	DetailLayoutBuilder = &DetailLayout;

	const TSharedPtr<IPropertyHandle> NodesProperty = DetailLayout.GetProperty(GET_MEMBER_NAME_CHECKED(UDialogueTree, Nodes));
	DetailLayout.HideProperty(NodesProperty);

	// Create a category so this is displayed early in the properties
	IDetailCategoryBuilder& MyCategory = DetailLayout.EditCategory("Dialogue Editor");
	IDetailCategoryBuilder& CurrentNodeCategory = DetailLayout.EditCategory("Current Node", LOCTEXT("CurrentNode", "Current Node"), ECategoryPriority::Important);

	TArray<TWeakObjectPtr<UObject>> ObjectsBeingCustomized;
	DetailLayout.GetObjectsBeingCustomized(ObjectsBeingCustomized);

	if (ObjectsBeingCustomized.Num() == 0) return;

	UDialogueTree* DialogueTree = Cast<UDialogueTree>(ObjectsBeingCustomized[0].Get());

	if (DialogueTree && DialogueTree->SelectedNodeId != -1 && DialogueTree->SelectedNodeId != 0) //display current node details:
	{
		int32 Index = DialogueTree->GetNodeIndexById(DialogueTree->SelectedNodeId);
		FDialogueDataNode& CurrentDataNode = DialogueTree->DialogueTreeData->DialogueDatas[Index];

		CurrentNodeCategory.AddCustomRow(LOCTEXT("Text", "Text"))
			.WholeRowContent()
			[
				SNew(STextBlock).Font(IDetailLayoutBuilder::GetDetailFont())
				.Text(LOCTEXT("Text", "Text"))
			];

		CurrentNodeCategory.AddCustomRow(LOCTEXT("TextValue", "TextValue"))
			.WholeRowContent()
			[
				SNew(SBox)
				.HeightOverride(100)
			[
				SNew(SMultiLineEditableTextBox).Text(CurrentDataNode.Text)
				.AutoWrapText(true)
			.OnTextCommitted(this, &FDialogueTreeEditorDetailsWidget::TextCommited, DialogueTree, DialogueTree->SelectedNodeId)
			.ModiferKeyForNewLine(EModifierKey::Shift)
			]
			];

		const TSharedPtr<IPropertyHandleArray> NodeArray = NodesProperty->AsArray();
		const TSharedPtr<IPropertyHandle> CurrentNodeProperty = NodeArray->GetElement(Index);

		/*
		*	Add IsPlayerResponse property.
		*/
		const TSharedPtr<IPropertyHandle> IsResponseField = CurrentNodeProperty->GetChildHandle("IsPlayerResponse");
		CurrentNodeCategory.AddProperty(IsResponseField);

		/*
		*	Add Events field.
		*/
		const TSharedPtr<IPropertyHandle> EventsField = CurrentNodeProperty->GetChildHandle("Events");
		CurrentNodeCategory.AddProperty(EventsField);

		/*
		*	Add conditions
		*/
		const TSharedPtr<IPropertyHandle> ConditionsField = CurrentNodeProperty->GetChildHandle("Conditions");
		CurrentNodeCategory.AddProperty(ConditionsField);

	}
}

void FDialogueTreeEditorDetailsWidget::TextCommited(const FText& InText, ETextCommit::Type InCommitType, UDialogueTree* DialogueTree, int32 id)
{
	int32 Index = DialogueTree->GetNodeIndexById(id);

	if (Index != -1)
	{
		FDialogueDataNode& CurrentDataNode = DialogueTree->DialogueTreeData->DialogueDatas[Index];

		// we don't commit text if it hasn't changed
		if (CurrentDataNode.Text.EqualTo(InText))
		{
			return;
		}

		const FScopedTransaction Transaction(LOCTEXT("TextCommited", "Edited Data Node Text"));
		DialogueTree->Modify();

		CurrentDataNode.Text = InText;
	}
}

#undef LOCTEXT_NAMESPACE