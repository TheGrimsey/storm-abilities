// Fill out your copyright notice in the Description page of Project Settings.


#include "UnboundDialogueNodeWidget.h"

#include "DialogueTreeEditor.h"
#include "DialogueTreeEditorStyle.h"

#include "UnboundDialogue/DialogueTree.h"
#include "UnboundDialogue/DialogueTreeData.h"

#include "UnboundDialogueViewportWidget.h"
#include "Widgets/Text/SMultiLineEditableText.h"
#include "Widgets/Text/STextBlock.h"
#include "Widgets/Images/SImage.h"
#include "Framework/Application/SlateApplication.h"
#include "Framework/MultiBox/MultiBoxBuilder.h"
#include "Layout/WidgetPath.h"
#include "Framework/Application/MenuStack.h"
#include "Editor/UnrealEd/Public/ScopedTransaction.h"

#define LOCTEXT_NAMESPACE "SUnboundDialogueNodeWidget"

void SUnboundDialogueNodeWidget::Construct(const FArguments& InArgs)
{
	Id = InArgs._Id;
	NodeIndex = InArgs._NodeIndex;
	DialogueTree = InArgs._DialogueTree; // for styles, TD: move somewhere?
	Owner = InArgs._Owner;
	Visibility = TAttribute<EVisibility>::Create(TAttribute<EVisibility>::FGetter::CreateSP(this, &SUnboundDialogueNodeWidget::GetNodeVisibility));

	ChildSlot
	[
		SNew(SOverlay)
			/*
			* NodeImage
			*/
			+SOverlay::Slot() // the node itself
			[
				SNew(SImage).Image(this, &SUnboundDialogueNodeWidget::GetNodeStyle)
							.Visibility(EVisibility::Visible)
			]

			/*
			* a Spacer that gives a minimum size to the node
			*/
			+SOverlay::Slot().VAlign(VAlign_Top)
			[
				SNew(SSpacer).Size(TAttribute<FVector2D>::Create(TAttribute<FVector2D>::FGetter::CreateSP(Owner, &SUnboundDialogueViewportWidget::GetNodeMinSize)))
			]

			/*
			* horizontal box containing top left icons
			*/
			+SOverlay::Slot().VAlign(VAlign_Top).HAlign(HAlign_Left)
							 .Padding(TAttribute<FMargin>::Create(TAttribute<FMargin>::FGetter::CreateSP(Owner, &SUnboundDialogueViewportWidget::GetLeftCornerPadding)))
			[
				SNew(SHorizontalBox).RenderTransform(TAttribute<TOptional<FSlateRenderTransform>>::Create(TAttribute<TOptional<FSlateRenderTransform>>::FGetter::CreateSP(Owner, &SUnboundDialogueViewportWidget::GetIconScale)))
									.RenderTransformPivot(FVector2D(0, 0))
				+ SHorizontalBox::Slot().AutoWidth().Padding(5, 3, 0, 0)
				[
					SNew(SImage).Image(FDialogueTreeEditorStyle::Get()->GetBrush("EventIcon"))
								.ColorAndOpacity(FLinearColor(246.0f / 255.0f, 207.0f / 255.0f, 6.0f / 255.0f, 1.0f))
								.Visibility(this, &SUnboundDialogueNodeWidget::GetEventIconVisibility)
				]
				+ SHorizontalBox::Slot().AutoWidth().Padding(5, 3, 0, 0)
				[
					SNew(SImage).Image(FDialogueTreeEditorStyle::Get()->GetBrush("ConditionIcon"))
					.ColorAndOpacity(FLinearColor(106.0f / 255.0f, 221.0f / 255.0f, 214.0f / 255.0f, 1.0f)) // FLinearColor(1.0f, 1.0f, 1.0f, 1.0f)
					.Visibility(this, &SUnboundDialogueNodeWidget::GetConditionIconVisibility)
				]
			]

			/*
			* horizontal box containing top right icons
			*/
			+SOverlay::Slot().VAlign(VAlign_Top).HAlign(HAlign_Right).Padding(0, 0, 0, 0) // left top right bottom
			[
				SNew(SHorizontalBox).RenderTransform(TAttribute<TOptional<FSlateRenderTransform>>::Create(TAttribute<TOptional<FSlateRenderTransform>>::FGetter::CreateSP(Owner, &SUnboundDialogueViewportWidget::GetIconScale)))
									.RenderTransformPivot(FVector2D(1, 0))
				+ SHorizontalBox::Slot().HAlign(HAlign_Right).FillWidth(1).Padding(0, 3, 10, 0)
				[
					SNew(SImage).Image(FDialogueTreeEditorStyle::Get()->GetBrush("VoiceIcon"))
								.ColorAndOpacity(FLinearColor(106.0f / 255.0f, 174.0f / 255.0f, 101.0f / 255.0f, 1.0f))
								.Visibility(this, &SUnboundDialogueNodeWidget::GetSoundIconVisibility)
				]
			]

			/*
			* text block - displayed only when out of Editing mode
			*/
			+SOverlay::Slot().VAlign(VAlign_Center).HAlign(HAlign_Center).Padding(TAttribute<FMargin>(this, &SUnboundDialogueNodeWidget::GetTextMargin)) // left top right bottom
			[
				SAssignNew(NodeTextBlock, STextBlock)
					.Justification(ETextJustify::Left)
					.Text(this, &SUnboundDialogueNodeWidget::GetNodeText)
					.Visibility(this, &SUnboundDialogueNodeWidget::GetTextBlockVisibility)
					.WrapTextAt(TAttribute<float>::Create(TAttribute<float>::FGetter::CreateSP(Owner, &SUnboundDialogueViewportWidget::GetNodeTextWrapLength))) // 250 by default at 1:1 zoom level
					.Font(TAttribute<FSlateFontInfo>::Create(TAttribute<FSlateFontInfo>::FGetter::CreateSP(Owner, &SUnboundDialogueViewportWidget::GetNodeFont)))
			]
	];
}

void SUnboundDialogueNodeWidget::Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime)
{
	NodeSize = AllottedGeometry.GetLocalSize(); //TD: is it possible to bind it?
}

FReply SUnboundDialogueNodeWidget::OnMouseButtonUp(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent)
{	
	/* Left mouse button UP */
	if (MouseEvent.GetEffectingButton() == EKeys::LeftMouseButton)
	{
		if ((bJustDoubleClicked || (Owner->SelectedNodes.Num() == 1 && Owner->SelectedNodes.Contains(Id) && NodeTextBlock->IsHovered())) && Id != 0)
		{
			bJustDoubleClicked = false;

			return FReply::Handled();
		}

		if (Owner->bBreakingLinksMode)
		{
			// Breaking links between Id and Owner->breakingLinksFromId
			BreakLinksWithNode();
			return FReply::Handled();
		}

		return FReply::Unhandled();
	}
	/* Right mouse button UP */
	else if (MouseEvent.GetEffectingButton() == EKeys::RightMouseButton)
	{
		if (Owner->bBreakingLinksMode)
		{
			Owner->bBreakingLinksMode = false;
		}

		if (!Owner->SelectedNodes.Contains(Id))
		{
			Owner->SelectNodes(Id);
			DialogueTree->SelectedNodeId = Id;
			Owner->ForceRefresh();
		}

		if (Owner->DialogueTreeEditor.Pin()->IsLinking) // if RMB UP on a node when linking, cancel linking
		{
			Owner->DialogueTreeEditor.Pin()->IsLinking = false;
			return FReply::Handled();
		}

		FMenuBuilder MenuBuilder(true, NULL);

		FUIAction AddLinkAction(FExecuteAction::CreateSP(this, &SUnboundDialogueNodeWidget::OnAddLink));
		FUIAction AddPCAnswerAction(FExecuteAction::CreateSP(this, &SUnboundDialogueNodeWidget::OnAddPlayerResponse));
		FUIAction AddNPCAnswerAction(FExecuteAction::CreateSP(this, &SUnboundDialogueNodeWidget::OnAddNpcAnswer));
		FUIAction BreakOutLinksAction(FExecuteAction::CreateSP(this, &SUnboundDialogueNodeWidget::OnBreakOutLinks));
		FUIAction BreakInLinksAction(FExecuteAction::CreateSP(this, &SUnboundDialogueNodeWidget::OnBreakInLinks));
		FUIAction DeleteAction(FExecuteAction::CreateSP(Owner, &SUnboundDialogueViewportWidget::DeleteOneNode, Id));
		FUIAction DeleteAllAction(FExecuteAction::CreateSP(Owner, &SUnboundDialogueViewportWidget::DeleteSelected));
		FUIAction BreakLinksModeAction(FExecuteAction::CreateSP(this, &SUnboundDialogueNodeWidget::BreakLinksMode));

		MenuBuilder.BeginSection(NAME_None, NSLOCTEXT("PropertyView", "ExpansionHeading", "Dialogue"));

		// If this node is a Player Response (OR the Start node) then we allow NPC answers.
		if (DialogueTree->Nodes[NodeIndex].IsPlayerResponse || DialogueTree->Nodes[NodeIndex].Id == 0)
		{
			MenuBuilder.AddMenuEntry(NSLOCTEXT("PropertyView", "AddNpcResponse", "Add NPC Response"), NSLOCTEXT("PropertyView", "AddNpcResponse_ToolTip", "Adds a child node (NPC Response)"), FSlateIcon(), AddNPCAnswerAction);
		}
		else
		{
			//Else we allow Player answers.
			MenuBuilder.AddMenuEntry(NSLOCTEXT("PropertyView", "AddPlayerResponse", "Add Player Response"), NSLOCTEXT("PropertyView", "AddPlayerResponse_ToolTip", "Adds a child node (Player Response)"), FSlateIcon(), AddPCAnswerAction);
		}

		MenuBuilder.AddMenuEntry(NSLOCTEXT("PropertyView", "AddLink", "Add Link"), NSLOCTEXT("PropertyView", "AddLink_ToolTip", "Usage: left click on the node you want to link"), FSlateIcon(), AddLinkAction);

		if (Owner->SelectedNodes.Num() == 1 && Owner->SelectedNodes[0] == Id)
		{
			MenuBuilder.AddMenuEntry(NSLOCTEXT("PropertyView", "BreakLinks", "Break Links With Node..."), NSLOCTEXT("PropertyView", "BreakLinks_ToolTip", "Usage: left click on the node you want to break links with"), FSlateIcon(), BreakLinksModeAction);
		}

		MenuBuilder.AddMenuEntry(NSLOCTEXT("PropertyView", "BreakOutLinks", "Break Outgoing Links"), NSLOCTEXT("PropertyView", "BreakOutLinks_ToolTip", "Breaks all outgoing links"), FSlateIcon(), BreakOutLinksAction);
		MenuBuilder.AddMenuEntry(NSLOCTEXT("PropertyView", "BreakInLinks", "Break Incoming Links"), NSLOCTEXT("PropertyView", "BreakInLinks_ToolTip", "Breaks all incoming links"), FSlateIcon(), BreakInLinksAction);

		//can't delete start node
		if (Id != 0)
		{
			MenuBuilder.AddMenuEntry(NSLOCTEXT("PropertyView", "Delete", "Delete Node"), NSLOCTEXT("PropertyView", "Delete_ToolTip", "Deletes this node"), FSlateIcon(), DeleteAction);
		}

		if (Owner->SelectedNodes.IsValidIndex(0) && Owner->SelectedNodes.IsValidIndex(1)) // if multiple are selected
		{
			MenuBuilder.AddMenuEntry(NSLOCTEXT("PropertyView", "DeleteSelected", "Delete Selected Nodes"), NSLOCTEXT("PropertyView", "DeleteAll_ToolTip", "Deletes selected nodes"), FSlateIcon(), DeleteAllAction);
		}

		MenuBuilder.EndSection();

		FWidgetPath WidgetPath = MouseEvent.GetEventPath() != nullptr ? *MouseEvent.GetEventPath() : FWidgetPath();
		FSlateApplication::Get().PushMenu(AsShared(), WidgetPath, MenuBuilder.MakeWidget(), MouseEvent.GetScreenSpacePosition(), FPopupTransitionEffect::ContextMenu);
		return FReply::Handled();
	}
	return FReply::Unhandled();
}

FReply SUnboundDialogueNodeWidget::OnMouseButtonDown(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent)
{
	FReply Reply = FReply::Handled();

	if (MouseEvent.GetEffectingButton() == EKeys::RightMouseButton)
	{
		Reply.DetectDrag(SharedThis(this), EKeys::RightMouseButton);
	}
	if (MouseEvent.GetEffectingButton() == EKeys::LeftMouseButton)
	{
		Reply.DetectDrag(SharedThis(this), EKeys::LeftMouseButton);
	}

	return Reply;
}

int32 SUnboundDialogueNodeWidget::OnPaint(const FPaintArgs& Args, const FGeometry& AllottedGeometry, const FSlateRect& MyClippingRect, FSlateWindowElementList& OutDrawElements, int32 LayerId, const FWidgetStyle& InWidgetStyle, bool bParentEnabled) const
{
	return SCompoundWidget::OnPaint(Args, AllottedGeometry, MyClippingRect, OutDrawElements, LayerId, InWidgetStyle, bParentEnabled);
}

FReply SUnboundDialogueNodeWidget::OnDragDetected(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent)
{
	if (MouseEvent.IsMouseButtonDown(EKeys::RightMouseButton) && !Owner->DialogueTreeEditor.Pin()->IsLinking)
	{
		Owner->bIsPanning = true;
	}
	if (MouseEvent.IsMouseButtonDown(EKeys::LeftMouseButton))
	{
		if (Owner->bBreakingLinksMode)
			Owner->bBreakingLinksMode = false;

		const FScopedTransaction Transaction(LOCTEXT("DragNodes", "Drag Nodes"));
		DialogueTree->Modify();

		// offset = local coords of the center of the node - local coords of the mouse click (inside the node)
		Owner->draggingOffset = FVector2D(MyGeometry.GetLocalSize().X / 2, MyGeometry.GetLocalSize().Y / 2) - MyGeometry.AbsoluteToLocal(MouseEvent.GetScreenSpacePosition());
		Owner->StartDraggingIndex(NodeIndex);
	}
	return FReply::Handled();
}

void SUnboundDialogueNodeWidget::OnMouseEnter(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent)
{	
	// if mouse is over a node widget after we stopped linkingAndPanning, try to link the node
	if (Owner->DialogueTreeEditor.Pin()->IsLinking && !Owner->bIsLinkingAndCapturing)
	{
		const FDialogueNode& ThisNode = DialogueTree->Nodes[NodeIndex];
		FDialogueNode& LinkingNode = DialogueTree->Nodes[Owner->DialogueTreeEditor.Pin()->LinkingFromIndex];

		/*
		* Can the linking node link with us?
		*	- If we aren't the linkingnode.
		*	- If both nodes aren't a PlayerResponse or NPCResponse.
		*	- If we aren't already linked to the LinkingNode.
		*/
		const bool CanLinkWithThis = LinkingNode.Id != Id && LinkingNode.IsPlayerResponse != ThisNode.IsPlayerResponse && !LinkingNode.LinkIds.Contains(Id);

		// Make sure we aren't linking to ourselves.
		if (CanLinkWithThis)
		{
			const FScopedTransaction Transaction(LOCTEXT("AddLink", "Add Link"));
			DialogueTree->Modify();

			LinkingNode.LinkIds.Add(Id);
			SortParentsLinks();
		}

		// No matter if we linked or not we stop linking.
		Owner->DialogueTreeEditor.Pin()->IsLinking = false;
	}
}

FReply SUnboundDialogueNodeWidget::OnMouseButtonDoubleClick(const FGeometry& InMyGeometry, const FPointerEvent& InMouseEvent)
{
	bJustDoubleClicked = true;
	return FReply::Unhandled();
}

void SUnboundDialogueNodeWidget::BreakLinksMode()
{
	Owner->bBreakingLinksMode = true;
	Owner->BreakingLinksFromNode = Id;
}

void SUnboundDialogueNodeWidget::BreakLinksWithNode()
{
	Owner->bBreakingLinksMode = false;

	Owner->DialogueTreeEditor.Pin()->BreakLinksToNode(Id);
}

void SUnboundDialogueNodeWidget::SortParentsLinks()
{
	for (FDialogueNode& Node : DialogueTree->Nodes)
	{
		for (const int32 Link : Node.LinkIds)
		{
			if (Link == Id) // if a link to current node has been found, then perform a sort on the links
			{
				Node.LinkIds.Sort([&](const int32 Id1, const int32 Id2) // resort them depending on their x coordinate
					{
						int32 Index1 = Owner->NodeIdsToIndexes.FindRef(Id1);
						int32 Index2 = Owner->NodeIdsToIndexes.FindRef(Id2);

						return DialogueTree->Nodes[Index1].Coordinates.X < DialogueTree->Nodes[Index2].Coordinates.X;
					});
				break;
			}
		}
	}
}

void SUnboundDialogueNodeWidget::OnDeleteNode(bool WithRefresh)
{
	if (Id == 0) return; //Don't delete start node

	Owner->DialogueTreeEditor.Pin()->RemoveNode(Id);

	Owner->DeselectNode(Id);

	if (WithRefresh)
	{
		Owner->SpawnNodes();
	}
}

FMargin SUnboundDialogueNodeWidget::GetTextMargin() const
{
	if (!bIsVisible)
	{
		return FMargin(0, 0, 0, 0);
	}

	float BasicMargin = 10 * Owner->GetZoomAmount();
	float OverrideTopMargin = 16 * Owner->GetZoomAmount();

	if (Owner->CurrentLOD < EDialogueRenderingLOD::MediumDetail)
	{
		return FMargin(0, 0, 0, 0);
	}

	//if (Owner->GetZoomLevel() > 6 && (DialogueTree->Nodes[NodeIndex].Events.Num() > 0 || DialogueTree->Nodes[NodeIndex].Conditions.Num() > 0 || DialogueTree->Nodes[NodeIndex].Sound || DialogueTree->Nodes[NodeIndex].DialogueWave))
	//{
	//	return FMargin(BasicMargin, OverrideTopMargin, BasicMargin, BasicMargin);
	//}
	//else
	{
		return FMargin(BasicMargin, BasicMargin, BasicMargin, BasicMargin);
	}
}

EVisibility SUnboundDialogueNodeWidget::GetTextFieldVisibility() const
{
	if (!bIsVisible)
	{
		return EVisibility::Collapsed;
	}

	if (Owner->CurrentLOD < EDialogueRenderingLOD::MediumDetail)
	{
		return EVisibility::Collapsed;
	}

	return (Owner->EditingTextOfNode == Id) ? EVisibility::Visible : EVisibility::Collapsed;
}

EVisibility SUnboundDialogueNodeWidget::GetTextBlockVisibility() const
{
	if (!bIsVisible)
	{
		return EVisibility::Collapsed;
	}

	if (Owner->CurrentLOD < EDialogueRenderingLOD::LowDetail)
	{
		return EVisibility::Hidden;
	}

	return (Owner->EditingTextOfNode == Id) ? EVisibility::Collapsed : EVisibility::Visible;
}

EVisibility SUnboundDialogueNodeWidget::GetEventIconVisibility() const
{
	if (!bIsVisible)
	{
		return EVisibility::Collapsed;
	}

	if (Owner->CurrentLOD < EDialogueRenderingLOD::MediumDetail)
	{
		return EVisibility::Collapsed;
	}

	return EVisibility::Collapsed;
	//return DialogueTree->Nodes[NodeIndex].Events.Num() > 0 ? EVisibility::SelfHitTestInvisible : EVisibility::Collapsed;
}

EVisibility SUnboundDialogueNodeWidget::GetConditionIconVisibility() const
{
	if (!bIsVisible)
	{
		return EVisibility::Collapsed;
	}

	if (Owner->CurrentLOD < EDialogueRenderingLOD::MediumDetail)
	{
		return EVisibility::Collapsed;
	}

	return EVisibility::Collapsed;
	//return DialogueTree->Nodes[NodeIndex].Conditions.Num() > 0 ? EVisibility::SelfHitTestInvisible : EVisibility::Collapsed;
}

EVisibility SUnboundDialogueNodeWidget::GetSoundIconVisibility() const
{
	if (!bIsVisible)
	{
		return EVisibility::Collapsed;
	}

	if (Owner->CurrentLOD < EDialogueRenderingLOD::MediumDetail)
	{
		return EVisibility::Collapsed;
	}

	return EVisibility::Collapsed;
	//return (DialogueTree->Nodes[NodeIndex].Sound || DialogueTree->Nodes[NodeIndex].DialogueWave) ? EVisibility::SelfHitTestInvisible : EVisibility::Collapsed;
}

EVisibility SUnboundDialogueNodeWidget::GetNodeVisibility() const
{
	return bIsVisible ? EVisibility::Visible : EVisibility::Collapsed;
}

void SUnboundDialogueNodeWidget::OnBreakOutLinks()
{
	Owner->DialogueTreeEditor.Pin()->BreakLinksFromNode(Id);
}

void SUnboundDialogueNodeWidget::OnBreakInLinks()
{
	Owner->DialogueTreeEditor.Pin()->BreakLinksToNode(Id);
}

void SUnboundDialogueNodeWidget::OnAddPlayerResponse()
{
	AddResponse(true);
}

void SUnboundDialogueNodeWidget::OnAddNpcAnswer()
{
	AddResponse(false);
}

void SUnboundDialogueNodeWidget::AddResponse(bool bPlayerResponse)
{
	FText transactionText = bPlayerResponse ? LOCTEXT("AddPlayerResponse", "Add Player Response") : LOCTEXT("AddNpcAnswer", "Add NPC Answer");
	const FScopedTransaction Transaction(transactionText);
	DialogueTree->Modify();
	DialogueTree->DialogueTreeData->Modify();

	const FText ResponseText = FText::FromString(TEXT("New Response"));
	FVector2D NewNodeCoords = GetNewNodeCoords();

	//Add the new node.
	int32 NewId = Owner->DialogueTreeEditor.Pin()->AddNode(ResponseText, bPlayerResponse, NewNodeCoords, false);

	//Link the new node to us.
	int Index = DialogueTree->GetNodeIndexById(Id);
	if (Index != -1)
	{
		GetNode().LinkIds.Add(NewId);
	}

	//Refresh nodes so we show the new one.
	Owner->SpawnNodes(NewId);
}

FVector2D SUnboundDialogueNodeWidget::GetNewNodeCoords()
{
	const FDialogueNode& Node = GetNode();

	if (Node.LinkIds.Num() > 0)
	{
		int32 linkToLastId = Node.LinkIds.FindLastByPredicate([&](const int32 LinkId)
			{
				int LinkIndex = Owner->NodeIdsToIndexes.FindRef(LinkId);
				return DialogueTree->Nodes[LinkIndex].Coordinates.Y > Node.Coordinates.Y;
			});

		if (linkToLastId != INDEX_NONE)
		{
			int32 lastIndex = Owner->NodeIdsToIndexes.FindRef(Node.LinkIds[linkToLastId]);
			const FDialogueNode& lastLinkedNode = DialogueTree->GetNodeByIndex(lastIndex);

			//Same coordinates as the last node but shifted to the right by the half-size of that node * 1.25
			FVector2D NewCoords = lastLinkedNode.Coordinates;
			NewCoords.X += Owner->NodeWidgets[lastIndex]->NodeSize.X * 1.25f / Owner->GetZoomAmount();

			return NewCoords;
		}
	}
	
	//If we dont have any links yet. Take same X value but add onto Y.
	FVector2D NewCoords = GetNode().Coordinates;
	NewCoords.Y += NodeSize.Y * 1.5f / Owner->GetZoomAmount(); // half the size of current widget + half the size of first widget + desired distance

	return NewCoords;
}

void SUnboundDialogueNodeWidget::OnAddLink()
{
	Owner->bIsLinkingAndCapturing = true;

	Owner->DialogueTreeEditor.Pin()->IsLinking = true;
	Owner->DialogueTreeEditor.Pin()->LinkingFromIndex = NodeIndex;
	Owner->DialogueTreeEditor.Pin()->LinkingCoords = DialogueTree->Nodes[NodeIndex].Coordinates;
	
	Owner->ForceSlateToStayAwake();
}

FText SUnboundDialogueNodeWidget::GetNodeText() const
{
	if (!bIsVisible)
	{
		return FText::FromString("");
	}

	if (Id == 0)
	{
		return FText::FromString("Start");
	}

	const FText& NodeText = GetNodeData().Text;

	const int maxTextLength = 300; //TODO: change 300 to user's choice

	if (NodeText.ToString().Len() > maxTextLength)
	{
		FString Output;

		Output += NodeText.ToString();
		Output.RemoveAt(maxTextLength, NodeText.ToString().Len() - maxTextLength, true);
		Output += TEXT("...");

		return FText::FromString(Output);
	}

	return NodeText;
}

const FSlateBrush* SUnboundDialogueNodeWidget::GetNodeStyle() const
{
	if (Id == 0) return FDialogueTreeEditorStyle::Get()->GetBrush("StartNodeStyle");

	return GetNode().IsPlayerResponse ? FDialogueTreeEditorStyle::Get()->GetBrush("PlayerNodeStyle") : FDialogueTreeEditorStyle::Get()->GetBrush("NpcNodeStyle");
}

FDialogueNode& SUnboundDialogueNodeWidget::GetNode() const
{
	return DialogueTree->GetNodeByIndex_Mutable(NodeIndex);
}

FDialogueDataNode& SUnboundDialogueNodeWidget::GetNodeData() const
{
	return DialogueTree->DialogueTreeData->DialogueDatas[NodeIndex];
}

#undef LOCTEXT_NAMESPACE