// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Widgets/Text/STextBlock.h"
#include "PropertyCustomizationHelpers.h"

/**
 * 
 */
class UNBOUNDDIALOGUEEDITOR_API SUnboundDialogueNodeWidget : public SCompoundWidget
{
	/*
	*	Variables
	*/
public:
	FVector2D NodeSize = FVector2D(0, 0);

	bool bIsSelected = false;
	bool bIsVisible = true;

private:
	bool bJustDoubleClicked = false;
	TSharedPtr<class STextBlock> NodeTextBlock;

	int32 Id;
	int32 NodeIndex;
	class UDialogueTree* DialogueTree;
	class SUnboundDialogueViewportWidget* Owner;

	SLATE_BEGIN_ARGS(SUnboundDialogueNodeWidget) {}

	SLATE_ARGUMENT(int32, Id)
	SLATE_ARGUMENT(int32, NodeIndex)
	SLATE_ARGUMENT(class UDialogueTree*, DialogueTree)
	SLATE_ARGUMENT(class SUnboundDialogueViewportWidget*, Owner)
	SLATE_END_ARGS()

	/*
	*	Methods
	*/
public:
	void Construct(const FArguments& InArgs);
	virtual void Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime) override;
	virtual FReply OnMouseButtonUp(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override;
	virtual FReply OnMouseButtonDown(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override;
	virtual int32 OnPaint(const FPaintArgs& Args, const FGeometry& AllottedGeometry, const FSlateRect& MyClippingRect, FSlateWindowElementList& OutDrawElements, int32 LayerId, const FWidgetStyle& InWidgetStyle, bool bParentEnabled) const override;
	virtual FReply OnDragDetected(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override;
	virtual void OnMouseEnter(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override;
	virtual FReply OnMouseButtonDoubleClick(const FGeometry& InMyGeometry, const FPointerEvent& InMouseEvent) override;

	void BreakLinksMode();
	void BreakLinksWithNode();
	void SortParentsLinks();
	void OnDeleteNode(bool WithRefresh);
	FMargin GetTextMargin() const;

private:
	EVisibility GetTextFieldVisibility() const;
	EVisibility GetTextBlockVisibility() const;
	EVisibility GetEventIconVisibility() const;
	EVisibility GetConditionIconVisibility() const;
	EVisibility GetSoundIconVisibility() const;
	EVisibility GetNodeVisibility() const;

	void OnBreakOutLinks();
	void OnBreakInLinks();

	void OnAddPlayerResponse();
	void OnAddNpcAnswer();

	void AddResponse(bool bPlayerResponse);

	FVector2D GetNewNodeCoords();

	void OnAddLink();
	FText GetNodeText() const;
	const FSlateBrush* GetNodeStyle() const;

	struct FDialogueNode& GetNode() const;
	struct FDialogueDataNode& GetNodeData() const;
};
