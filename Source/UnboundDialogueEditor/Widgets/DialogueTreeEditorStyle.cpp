// Fill out your copyright notice in the Description page of Project Settings.


#include "DialogueTreeEditorStyle.h"

#include "Styling/SlateStyle.h"
#include "Styling/SlateStyleRegistry.h"

#define BOX_BRUSH( RelativePath, ... ) FSlateBoxBrush( RootToContentDir( RelativePath, TEXT(".png") ), __VA_ARGS__ )
#define IMAGE_BRUSH( RelativePath, ... ) FSlateImageBrush( RootToContentDir( RelativePath, TEXT(".png") ), __VA_ARGS__ )

void FDialogueTreeEditorStyle::Initialize()
{	 
	//Only register once
	if (StyleSet.IsValid())
	{
		return;
	}

	StyleSet = MakeShareable(new FSlateStyleSet(GetStyleSetName()));
	StyleSet->SetContentRoot(FPaths::EngineContentDir() / TEXT("Editor/Slate"));
	StyleSet->SetCoreContentRoot(FPaths::EngineContentDir() / TEXT("Slate"));

	{
		StyleSet->Set("PlayerNodeStyle", new BOX_BRUSH("PlayerNode", FMargin(5.f / 138.f, 5.f / 56.f, 5.f / 138.f, 5.f / 56.f))); // left top right bottom
		StyleSet->Set("NpcNodeStyle", new BOX_BRUSH("NpcNode", FMargin(5.f / 138.f, 5.f / 56.f, 5.f / 138.f, 5.f / 56.f)));
		StyleSet->Set("StartNodeStyle", new BOX_BRUSH("StartNode", FMargin(5.f / 138.f, 5.f / 56.f, 5.f / 138.f, 5.f / 56.f)));

		StyleSet->Set("EventIcon", new IMAGE_BRUSH("Event", FVector2D(14, 16)));
		StyleSet->Set("ConditionIcon", new IMAGE_BRUSH("Condition", FVector2D(16, 16)));
		StyleSet->Set("VoiceIcon", new IMAGE_BRUSH("Sound", FVector2D(17, 16)));

		StyleSet->Set(FName(TEXT("ClassThumbnail.Dialogue")), new IMAGE_BRUSH("Dialogue-64px", FVector2D(64, 64)));
		StyleSet->Set(FName(TEXT("ClassIcon.Dialogue")), new IMAGE_BRUSH("Dialogue-16px", FVector2D(16, 16)));
	}

	FSlateStyleRegistry::RegisterSlateStyle(*StyleSet.Get());
}

void FDialogueTreeEditorStyle::Shutdown()
{
	if (StyleSet.IsValid())
	{
		FSlateStyleRegistry::UnRegisterSlateStyle(*StyleSet.Get());
		ensure(StyleSet.IsUnique());
		StyleSet.Reset();
	}
}

TSharedPtr< FSlateStyleSet > FDialogueTreeEditorStyle::StyleSet = nullptr;
TSharedPtr< ISlateStyle > FDialogueTreeEditorStyle::Get() { return StyleSet; }

FName FDialogueTreeEditorStyle::GetStyleSetName()
{
	static FName DialogueTreeEditorStyleName(TEXT("DialogueTreeEditorStyle"));
	return DialogueTreeEditorStyleName;
}

FString FDialogueTreeEditorStyle::RootToContentDir(const ANSICHAR* RelativePath, const TCHAR* Extension)
{
	static FString ContentDir = FPaths::ProjectContentDir() + TEXT("Editor/Dialogue/");
	return (ContentDir / RelativePath) + Extension;
}
