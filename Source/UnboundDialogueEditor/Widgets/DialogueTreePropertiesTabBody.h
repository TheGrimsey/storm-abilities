// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "SSingleObjectDetailsPanel.h"

/**
 * 
 */
class UNBOUNDDIALOGUEEDITOR_API SDialogueTreePropertiesTabBody : public SSingleObjectDetailsPanel
{
	/*
	*	Variables
	*/
public:
	SLATE_BEGIN_ARGS(SDialogueTreePropertiesTabBody) {}
	SLATE_END_ARGS()

private:
	// Pointer back to owning Dialogue editor instance (the keeper of state)
	TWeakPtr<class FDialogueTreeEditor> DialogueTreeEditorPtr;

	/*
	*	Methods
	*/
public:
	void Construct(const FArguments& InArgs, TSharedPtr<class FDialogueTreeEditor> InDialogueEditor);

	virtual UObject* GetObjectToObserve() const override;

	virtual TSharedRef<SWidget> PopulateSlot(TSharedRef<SWidget> PropertyEditorWidget) override;

	virtual void Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime) override;

};
