// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "PropertyCustomizationHelpers.h"
#include "Runtime/Slate/Public/Widgets/Layout/SConstraintCanvas.h"

//#include "DialoguePluginEditorSettingsDetails.h"

enum class EDialogueRenderingLOD : uint8
{
	// Detail level that text starts being disabled because it is unreadable
	LowDetail,

	// Detail level at which text starts to get hard to read but is still drawn
	MediumDetail,

	// Detail level when zoomed in at 1:1
	DefaultDetail,
};

enum class ESelectionType : uint8
{
	WithShift,
	WithCtrl,
	Default
};

/**
 * 
 */
class UNBOUNDDIALOGUEEDITOR_API SUnboundDialogueViewportWidget : public SCompoundWidget
{
	/*
	*	Variables
	*/
public:
	ESelectionType CurrentSelection = ESelectionType::Default;

	//The Ids of the currently selected nodes.
	mutable TArray<int32> SelectedNodes;

	mutable bool bFlagForRefresh;

	/** Pointer back to owning Dialogue editor instance (the keeper of state) */
	TWeakPtr<class FDialogueTreeEditor> DialogueTreeEditor;

	//Index of the Node which we are currently dragging (-1 if none).
	int32 DraggedNodeIndex = -1;
	FVector2D draggingOffset;
	FVector2D panningOffset = FVector2D(0, 0);
	FVector2D CachedLocalSize = FVector2D(0, 0);

	TArray<FVector2D> CachedNodeSizes;
	TArray<TSharedPtr<class SUnboundDialogueNodeWidget>> NodeWidgets;
	TArray<SConstraintCanvas::FSlot*> NodeFSlots;
	TArray<bool> isNodeVisible;

	//Cached Id to Index conversion.
	TMap<int32, int32> NodeIdsToIndexes;

	//Id of the Node which we are currently breaking links from (-1 if none).
	int32 BreakingLinksFromNode;
	//Id of the Node which we are currently editing the text of (-1 if none).
	int32 EditingTextOfNode;

	bool bIsLinkingAndCapturing;
	bool bBreakingLinksMode;
	bool bIsSelectingMultipleNodes;
	bool bIsPanning;

	bool bShouldTickAllWidgets = false;

private:
	SLATE_BEGIN_ARGS(class SUnboundDialogueViewportWidget) : _ShowGraphStateOverlay(true) , _IsEditable(true) {}

	SLATE_ARGUMENT(class UDialogueTree*, DialogueTree)
	SLATE_ATTRIBUTE(bool, ShowGraphStateOverlay)
	SLATE_ATTRIBUTE(bool, IsEditable)
	SLATE_END_ARGS()

	/** How zoomed in/out we are. e.g. 0.25f results in quarter-sized nodes. */
	int32 CurrentZoomLevel;
	/** Previous Zoom Level */
	int32 PreviousZoomLevel;

	/** Current LOD level for nodes/pins */
	EDialogueRenderingLOD CurrentLOD;

	bool bFirstNodeRequiresRepositioning;
	/** Whether to draw decorations for graph state (PIE / ReadOnly etc.) */
	TAttribute<bool> ShowGraphStateOverlay;
	/** Is the graph editable (can nodes be moved, etc...)? */
	TAttribute<bool> IsEditable;

	mutable TArray<int32> m_OriginallySelectedNodes;
	mutable FVector2D MarqueePointOfOrigin;
	mutable FVector2D MarqueeSize;
	mutable FVector2D MarqueeEndPoint;

	/**	Whether the software cursor should be drawn in the viewport */
	bool bShowSoftwareCursor : 1;
	FVector2D SoftwareCursorPosition;

	/** Direction for panning when we're dragging a node or a link, and the cursor is near the screen border*/
	FVector2D AutoPanningDirection = FVector2D(0, 0);
	FVector2D clickUpCoords;
	FVector2D clickDownCoords;
	FVector2D dragMouseCoords;
	/** Panning offset during marquee selection*/
	FVector2D selectionPanningOffset;
	/** Background sprite position*/
	float bgLeft = -128;
	/** Background sprite position*/
	float bgTop = -128;

	class UDialogueTree* DialogueTree;
	TSharedPtr<SConstraintCanvas> CanvasPanel;

	uint16 fontSize;
	FSlateFontInfo NodeFont;

	float NodeTextWrapLength = 250;

	FVector2D NodeMinSize = FVector2D(144, 60);

	/** Curve that handles fading the 'Zoom +X' text */
	FCurveSequence ZoomLevelFade;

	FIntPoint CoordsForPasting;

	/*
	*	Methods
	*/
public:
	void Construct(const FArguments& InArgs, TSharedPtr<class FDialogueTreeEditor> InDialogueTreeEditor);
	virtual FCursorReply OnCursorQuery(const FGeometry& MyGeometry, const FPointerEvent& CursorEvent) const override;
	virtual int32 OnPaint(const FPaintArgs& Args, const FGeometry& AllottedGeometry, const FSlateRect& MyClippingRect, FSlateWindowElementList& OutDrawElements, int32 LayerId, const FWidgetStyle& InWidgetStyle, bool bParentEnabled) const override;
	virtual FReply OnMouseMove(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override;
	virtual FReply OnMouseButtonUp(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override;
	virtual FReply OnMouseButtonDown(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override;
	virtual void Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime) override;
	virtual FReply OnDragDetected(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override;
	virtual FReply OnKeyDown(const FGeometry& MyGeometry, const FKeyEvent& InKeyEvent) override;
	virtual void OnFocusLost(const FFocusEvent& InFocusEvent) override;
	virtual FReply OnFocusReceived(const FGeometry& MyGeometry, const FFocusEvent& InFocusEvent) override;
	virtual bool SupportsKeyboardFocus() const override { return true; }

	/** Clears the viewport canvas, then respawns all nodes and focuses the one passed as parameter. */
	void SpawnNodes(int32 IdToFocus = 0);
	void SelectNodes(TArray<int32> NodesIds) const;
	void SelectNodes(int32 NodeId) const;
	void SelectUnderCursor() const;
	void DeselectNode(int32 NodeId) const;
	void DeselectAllNodes() const;
	void StartDraggingIndex(int32 DraggedNodeIndex);
	void ForceRefresh() const;
	void DeleteSelected();
	void DeleteOneNode(int32 NodeId);
	EActiveTimerReturnType EmptyTimer(double InCurrentTime, float InDeltaTime);
	/** @retun the zoom amount; e.g. a value of 0.25f results in quarter-sized nodes */
	float GetZoomAmount() const;
	FText GetZoomText() const;
	FSlateColor GetZoomTextColorAndOpacity() const;
	/** @return the view offset in graph space */
	FVector2D GetViewOffset() const;
	/** Get the grid snap size */
	static float GetSnapGridSize() { return 16.f; }
	/** @return node text size based on current zoom level*/
	FSlateFontInfo GetNodeFont();
	/** Sets node font size*/
	void UpdateFontInfo();
	/** @return text wrap length for nodes based on zoom level*/
	float GetNodeTextWrapLength();
	/** @return node min size depending on zoom level. Default is (144, 60).*/
	FVector2D GetNodeMinSize();
	void ForceSlateToStayAwake();
	TOptional<FSlateRenderTransform> GetIconScale();
	FMargin GetLeftCornerPadding();
	/** @return zoom level; e.g. values between 0 and 16 */
	int32 GetZoomLevel() const;

	void DuplicateSelected();
	void CopySelected();
	void CutSelected();
	void PasteNodes();

private:
	// Paint the background as lines
	void PaintBackgroundAsLines(const FSlateBrush* BackgroundImage, const FGeometry& AllottedGeometry, const FSlateRect& MyClippingRect, FSlateWindowElementList& OutDrawElements, int32& DrawLayerId) const;
	
	/** Given a coordinate in graph space (e.g. a node's position), return the same coordinate in widget space while taking zoom and panning into account */
	FVector2D GraphCoordToPanelCoord(const FVector2D& GraphSpaceCoordinate) const;
	
	void OnAddNodeClicked();
	void OnIsResponseCommited(ECheckBoxState NewState);
	void UpdateAutoPanningDirection(FVector2D& currentMouseCoords, FSlateRect& PanelScreenSpaceRect);
	void OnSelectedNodeTextCommited(const FText& InText, ETextCommit::Type);
	
	FMargin GetOffset(int32 index);
	FText GetNodeText() const;
	ECheckBoxState GetIsResponse() const;
	FVector2D ComputeSplineTangent(const FVector2D& Start, const FVector2D& End) const;
	
	virtual FReply OnMouseWheel(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override;
	// Change zoom level by the specified zoom level delta, about the specified origin.
	void ChangeZoomLevel(int32 ZoomLevelDelta, const FVector2D& WidgetSpaceZoomOrigin, bool bOverrideZoomLimiting);
	// Should be called whenever the zoom level has changed
	void PostChangedZoom();
	void UpdateZoomAffectedValues();
	
	EVisibility IsSimulating() const;
	
	void SerializeEventsConditions(const struct FDialogueNode& Node, TSharedPtr<FJsonObject> JsonNode);
	void DeserializeEventsConditions(struct FDialogueNode& Node, TSharedPtr<FJsonObject> JsonNode);
};
