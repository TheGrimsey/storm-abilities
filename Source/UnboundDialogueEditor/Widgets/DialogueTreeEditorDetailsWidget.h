// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Editor/PropertyEditor/Public/IDetailCustomization.h"
#include "PropertyCustomizationHelpers.h"

/**
 * 
 */
class UNBOUNDDIALOGUEEDITOR_API FDialogueTreeEditorDetailsWidget : public IDetailCustomization
{
	/*
	*	Variables
	*/
private:
	/** Associated detail layout builder */
	IDetailLayoutBuilder* DetailLayoutBuilder;

public:
	/** Makes a new instance of this detail layout class for a specific detail view requesting it */
	static TSharedRef<IDetailCustomization> MakeInstance();
	/** IDetailCustomization interface */
	virtual void CustomizeDetails(IDetailLayoutBuilder& DetailLayout) override;
	void TextCommited(const FText& InText, ETextCommit::Type InCommitType, class UDialogueTree* DialogueTree, int32 id);

};
