// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class UnboundDialogueEditor : ModuleRules
{
	public UnboundDialogueEditor(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "UnboundDialogue" });

        PublicDependencyModuleNames.AddRange(
            new string[]
            {
                    "Core",
                    "CoreUObject",
                    "EditorStyle",
                    "Engine",
                    "InputCore",
                    "LevelEditor",
                    "Slate",
                    "AssetTools",
                    "KismetWidgets",
                    "WorkspaceMenuStructure",
                    "Projects",
                    "GraphEditor",
                    "AnimGraph"
            }
        );

        PrivateDependencyModuleNames.AddRange(
            new string[]
            {
                    "PropertyEditor",
                    "SlateCore",
                    "ApplicationCore",
                    "UnrealEd",
                    "Json",
                    "JsonUtilities"
            }
        );

    }
}
