// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "UnboundDialogueEditor.h"
#include "Modules/ModuleManager.h"

#include "PropertyEditorModule.h"
#include "DialogueTreeEditor.h"
#include "Widgets/DialogueTreeEditorStyle.h"
#include "Widgets/DialogueTreeEditorDetailsWidget.h"

IMPLEMENT_MODULE(FUnboundDialogueEditorModule, UnboundDialogueEditor );

void FUnboundDialogueEditorModule::StartupModule()
{
	FDialogueTreeEditorStyle::Initialize();

	IAssetTools& AssetTools = FModuleManager::LoadModuleChecked<FAssetToolsModule>("AssetTools").Get();
	AssetTools.RegisterAssetTypeActions(MakeShareable(new FDialogueTreeAssetTypeActions()));

	FPropertyEditorModule& PropertyModule = FModuleManager::LoadModuleChecked<FPropertyEditorModule>("PropertyEditor");
	PropertyModule.RegisterCustomClassLayout("DialogueTree", FOnGetDetailCustomizationInstance::CreateStatic(&FDialogueTreeEditorDetailsWidget::MakeInstance));
	PropertyModule.NotifyCustomizationModuleChanged();
}

void FUnboundDialogueEditorModule::ShutdownModule()
{
	FDialogueTreeEditorStyle::Shutdown();
}
