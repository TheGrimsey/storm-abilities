// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "AssetTypeActions_Base.h"
#include "Toolkits/AssetEditorToolkit.h"
#include "Toolkits/AssetEditorManager.h"
#include "Editor/EditorWidgets/Public/ITransportControl.h"
#include "EditorUndoClient.h"

class FDialogueTreeAssetTypeActions : public FAssetTypeActions_Base
{
public:
	// IAssetTypeActions interface
	virtual FText GetName() const override;
	virtual FColor GetTypeColor() const override;
	virtual UClass* GetSupportedClass() const override;
	virtual void OpenAssetEditor(const TArray<UObject*>& InObjects, TSharedPtr<class IToolkitHost> EditWithinLevelEditor = TSharedPtr<IToolkitHost>()) override;
	virtual uint32 GetCategories() override;
	// End of IAssetTypeActions interface
};

/**
 * 
 */
class UNBOUNDDIALOGUEEDITOR_API FDialogueTreeEditor : public FAssetEditorToolkit, public FEditorUndoClient, public FGCObject
{
	/*
	*	Variables
	*/
public:
	//If we should refresh the details panel on next tick.
	bool RefreshDetails;

	/*
	*	Used for linking nodes.
	*/
	bool IsLinking;

	int32 LinkingFromIndex;

	FVector2D LinkingCoords;


protected:
	//DialogueTree we are editing.
	class UDialogueTree* DialogueTree;

	TSharedPtr<class SUnboundDialogueViewportWidget> DialogueViewportWidget;

	TSharedPtr<class FUICommandList> DialogueEditorCommands;

	/*
	*	Methods
	*/
public:
	FDialogueTreeEditor();
	virtual ~FDialogueTreeEditor();

	// IToolkit interface
	virtual void RegisterTabSpawners(const TSharedRef<FTabManager>& InTabManager) override;
	virtual void UnregisterTabSpawners(const TSharedRef<FTabManager>& InTabManager) override;
	// End of IToolkit interface

	//~ Begin FEditorUndoClient Interface
	virtual void PostUndo(bool bSuccess) override;
	virtual void PostRedo(bool bSuccess) override;
	// End of FEditorUndoClient

	// FAssetEditorToolkit
	virtual FName GetToolkitFName() const override { return FName("DialogueTreeEditor"); }	// Must implement in derived class!
	virtual FText GetBaseToolkitName() const override {	return GetToolkitName(); }			// Must implement in derived class!
	virtual FText GetToolkitName() const override;
	virtual FText GetToolkitToolTipText() const override;

	virtual FLinearColor GetWorldCentricTabColorScale() const override { return FLinearColor::White; }
	virtual FString GetWorldCentricTabPrefix() const override { return TEXT("DialogueEditor"); }
	virtual bool IsAssetEditor() const override { return true; }
	virtual class FEdMode* GetEditorMode() const override { return nullptr;  }

	virtual bool ProcessCommandBindings(const FKeyEvent& InKeyEvent) const override;
	// End of FAssetEditorToolkit

	// FSerializableObject interface
	virtual void AddReferencedObjects(FReferenceCollector& Collector) override;
	// End of FSerializableObject interface

	void InitDialogueEditor(const EToolkitMode::Type Mode, const TSharedPtr< IToolkitHost >& InitToolkitHost, class UDialogueTree* InitDialogue);

	class UDialogueTree* GetDialogueTree() { return DialogueTree; }

	/*
	* Adds a new DialogueNode with the input text. Returns the Id.
	* @Param Text Text for the node.
	* @Param IsResponse
	* @Param Coordinates Position in editor grid.
	* @Param CreateTransaction Whether or not we want to add a transaction for undo & redo.
	*/
	int32 AddNode(const FText& Text, const bool IsPlayerResponse, const FVector2D& Coordinates, const bool CreateTransaction = true);

	//Removes an existing node by Id.
	void RemoveNode(const int32 Id, const bool CreateTransaction = true);

	//Breaks all links going to NodeId.
	void BreakLinksToNode(const int32 NodeId, const bool CreateTransaction = true);

	//Breaks all links going from NodeId
	void BreakLinksFromNode(const int32 NodeId, const bool CreateTransaction = true);

	//Sets the text of NodeId.
	void UpdateNodeText(const int32 NodeId, const FText& NewText);

	//Sets the coordinates of NodeId.
	void UpdateNodeCoordinates(const int32 NodeId, const FVector2D& NewCoordinates);

	//Sets the IsResponse property of a Node.
	void UpdateNodeIsResponse(const int32 NodeId, const bool NewIsResponse);

protected:
	TSharedRef<SDockTab> SpawnTab_Viewport(const FSpawnTabArgs& Args);
	TSharedRef<SDockTab> SpawnTab_Details(const FSpawnTabArgs& Args);

private:
	void BindCommands();

	void OnDuplicateSelected();
	void OnCopySelected();
	void OnCutSelected();
	void OnPasteNodes();
	void OnDelete();
};
