// Fill out your copyright notice in the Description page of Project Settings.


#include "DialogueTreeEditor.h"

#include "UnboundDialogue/DialogueTree.h"
#include "UnboundDialogue/DialogueTreeData.h"

#include "Widgets/UnboundDialogueViewportWidget.h"
#include "Widgets/DialogueTreePropertiesTabBody.h"

#include "Framework/Application/SlateApplication.h"
#include "AssetTypeCategories.h"
#include "SDockTab.h"
#include "GenericCommands.h"
#include "ScopedTransaction.h"

#define LOCTEXT_NAMESPACE "DialogueTreeEditor"

const FName DialogueTreeEditorAppName = FName(TEXT("DialogueTreeEditorApp"));

struct FDialogueTreeEditorTabs
{
	// Tab identifiers
	static const FName DetailsID;
	static const FName ViewportID;
};

const FName FDialogueTreeEditorTabs::DetailsID(TEXT("Details"));
const FName FDialogueTreeEditorTabs::ViewportID(TEXT("Viewport"));

FText FDialogueTreeAssetTypeActions::GetName() const
{
	return FText::FromString(TEXT("DialogueTree"));
}

FColor FDialogueTreeAssetTypeActions::GetTypeColor() const
{
	return FColor(255, 55, 220);
}

UClass* FDialogueTreeAssetTypeActions::GetSupportedClass() const
{
	return UDialogueTree::StaticClass();
}

void FDialogueTreeAssetTypeActions::OpenAssetEditor(const TArray<UObject*>& InObjects, TSharedPtr<class IToolkitHost> EditWithinLevelEditor)
{
	const EToolkitMode::Type Mode = EditWithinLevelEditor.IsValid() ? EToolkitMode::WorldCentric : EToolkitMode::Standalone;

	//Create DialogueTreeEditor window for each selected dialoguetree object.
	for(UObject* Object : InObjects)
	{
		if (UDialogueTree* DialogueTree = Cast<UDialogueTree>(Object))
		{
			TSharedRef<FDialogueTreeEditor> NewDialogueTreeEditor(new FDialogueTreeEditor());
			NewDialogueTreeEditor->InitDialogueEditor(Mode, EditWithinLevelEditor, DialogueTree);

		}
	}
}

uint32 FDialogueTreeAssetTypeActions::GetCategories()
{
	return EAssetTypeCategories::Gameplay;
}

FDialogueTreeEditor::FDialogueTreeEditor() : FAssetEditorToolkit()
{
	IsLinking = false;

	if (UEditorEngine* Editor = Cast<UEditorEngine>(GEngine))
	{
		Editor->RegisterForUndo(this);
	}
}

FDialogueTreeEditor::~FDialogueTreeEditor()
{
	if (UEditorEngine* Editor = Cast<UEditorEngine>(GEngine))
	{
		Editor->UnregisterForUndo(this);
	}
}

void FDialogueTreeEditor::RegisterTabSpawners(const TSharedRef<FTabManager>& InTabManager)
{
	FAssetEditorToolkit::RegisterTabSpawners(InTabManager);

	WorkspaceMenuCategory = InTabManager->AddLocalWorkspaceMenuCategory(LOCTEXT("WorkspaceMenu_DialogueEditor", "Dialogue Editor"));
	auto WorkspaceMenuCategoryRef = WorkspaceMenuCategory.ToSharedRef();

	InTabManager->RegisterTabSpawner(FDialogueTreeEditorTabs::ViewportID, FOnSpawnTab::CreateSP(this, &FDialogueTreeEditor::SpawnTab_Viewport))
		.SetDisplayName(LOCTEXT("ViewportTab", "Viewport"))
		.SetGroup(WorkspaceMenuCategoryRef)
		.SetIcon(FSlateIcon(FEditorStyle::GetStyleSetName(), "LevelEditor.Tabs.Viewports"));

	InTabManager->RegisterTabSpawner(FDialogueTreeEditorTabs::DetailsID, FOnSpawnTab::CreateSP(this, &FDialogueTreeEditor::SpawnTab_Details))
		.SetDisplayName(LOCTEXT("DetailsTabLabel", "Details"))
		.SetGroup(WorkspaceMenuCategoryRef)
		.SetIcon(FSlateIcon(FEditorStyle::GetStyleSetName(), "LevelEditor.Tabs.Details"));
}

void FDialogueTreeEditor::UnregisterTabSpawners(const TSharedRef<FTabManager>& InTabManager)
{
	FAssetEditorToolkit::UnregisterTabSpawners(InTabManager);

	InTabManager->UnregisterTabSpawner(FDialogueTreeEditorTabs::ViewportID);
	InTabManager->UnregisterTabSpawner(FDialogueTreeEditorTabs::DetailsID);
}

void FDialogueTreeEditor::PostUndo(bool bSuccess)
{
	if (bSuccess)
	{
		GetDialogueTree()->SelectedNodeId = -1;

		DialogueViewportWidget->SpawnNodes();
		DialogueViewportWidget->ForceRefresh();

		FSlateApplication::Get().DismissAllMenus();
	}
}

void FDialogueTreeEditor::PostRedo(bool bSuccess)
{
	if (bSuccess)
	{
		GetDialogueTree()->SelectedNodeId = -1;

		DialogueViewportWidget->SpawnNodes();
		DialogueViewportWidget->ForceRefresh();

		FSlateApplication::Get().DismissAllMenus();
	}
}

FText FDialogueTreeEditor::GetToolkitName() const
{
	const bool bDirtyState = DialogueTree->GetOutermost()->IsDirty();

	FFormatNamedArguments Args;
	Args.Add(TEXT("DialogueName"), FText::FromString(DialogueTree->GetName()));
	Args.Add(TEXT("DirtyState"), bDirtyState ? FText::FromString(TEXT("*")) : FText::GetEmpty());
	return FText::Format(LOCTEXT("DialogueTreeEditorAppLabel", "{DialogueName}{DirtyState}"), Args);
}

FText FDialogueTreeEditor::GetToolkitToolTipText() const
{
	const UObject* EditingObject = DialogueTree;

	check(EditingObject != NULL);

	return GetToolTipTextForObject(EditingObject);
}

bool FDialogueTreeEditor::ProcessCommandBindings(const FKeyEvent& InKeyEvent) const
{
	return DialogueEditorCommands->ProcessCommandBindings(InKeyEvent);
}

void FDialogueTreeEditor::AddReferencedObjects(FReferenceCollector& Collector)
{
	Collector.AddReferencedObject(DialogueTree);
}

void FDialogueTreeEditor::InitDialogueEditor(const EToolkitMode::Type Mode, const TSharedPtr< IToolkitHost >& InitToolkitHost, UDialogueTree* InitDialogue)
{
	RefreshDetails = true;
	DialogueTree = InitDialogue;

	TSharedPtr<FDialogueTreeEditor> DialogueTreeEditor = SharedThis(this);

	// Default layout
	const TSharedRef<FTabManager::FLayout> StandaloneDefaultLayout = FTabManager::NewLayout("Standalone_DialogueTreeEditor_Layout")
		->AddArea
		(
			FTabManager::NewPrimaryArea()
			->SetOrientation(Orient_Vertical)
			->Split
			(
				FTabManager::NewStack()
				->SetSizeCoefficient(0.1f)
				->SetHideTabWell(true)
				->AddTab(GetToolbarTabId(), ETabState::OpenedTab)
			)
			->Split
			(
				FTabManager::NewSplitter()
				->SetOrientation(Orient_Horizontal)
				->SetSizeCoefficient(0.9f)
				->Split
				(
					FTabManager::NewStack()
					->SetSizeCoefficient(0.8f)
					->SetHideTabWell(true)
					->AddTab(FDialogueTreeEditorTabs::ViewportID, ETabState::OpenedTab)
				)
				->Split
				(
					FTabManager::NewSplitter()
					->SetOrientation(Orient_Vertical)
					->SetSizeCoefficient(0.2f)
					->Split
					(
						FTabManager::NewStack()
						->SetSizeCoefficient(0.75f)
						->SetHideTabWell(true)
						->AddTab(FDialogueTreeEditorTabs::DetailsID, ETabState::OpenedTab)
					)
				)
			)
		);

	// Initialize the asset editor
	InitAssetEditor(Mode, InitToolkitHost, DialogueTreeEditorAppName, StandaloneDefaultLayout, /*bCreateDefaultStandaloneMenu=*/ true, /*bCreateDefaultToolbar=*/ true, InitDialogue);
	RegenerateMenusAndToolbars();
	BindCommands();
}

int32 FDialogueTreeEditor::AddNode(const FText& Text, const bool IsPlayerResponse, const FVector2D& Coordinates, const bool CreateTransaction)
{
	const FScopedTransaction Transaction(LOCTEXT("AddNode", "Node Added"), CreateTransaction);
	DialogueTree->Modify();
	DialogueTree->DialogueTreeData->Modify();

	//Create new node.
	FDialogueNode NewNode;
	NewNode.Id = DialogueTree->NextNodeId;
	NewNode.IsPlayerResponse = IsPlayerResponse;
	NewNode.Coordinates = Coordinates;

	FDialogueDataNode NewDataNode;
	NewDataNode.Id = NewNode.Id;
	NewDataNode.Text = Text;

	//Add new nodes
	DialogueTree->Nodes.Add(NewNode);
	DialogueTree->DialogueTreeData->DialogueDatas.Add(NewDataNode);

	//Increment Id for next node.
	DialogueTree->NextNodeId++;

	return NewNode.Id;
}

void FDialogueTreeEditor::RemoveNode(const int32 NodeId, const bool CreateTransaction)
{
	const FScopedTransaction Transaction(LOCTEXT("RemoveNode", "Node Added"), CreateTransaction);
	DialogueTree->Modify();
	DialogueTree->DialogueTreeData->Modify();

	//Break links without creating a transaction (since we might already have one here or dont want any).
	BreakLinksToNode(NodeId, false);

	int32 Index = DialogueTree->GetNodeIndexById(NodeId);
	if (Index != -1)
	{
		DialogueTree->Nodes.RemoveAt(Index);
		DialogueTree->DialogueTreeData->DialogueDatas.RemoveAt(Index);
	}
}

void FDialogueTreeEditor::BreakLinksToNode(const int32 NodeId, const bool CreateTransaction)
{
	const FScopedTransaction Transaction(LOCTEXT("BreakLinksToNode", "Broke links going to node."), CreateTransaction);
	DialogueTree->Modify();

	for (FDialogueNode& Node : DialogueTree->Nodes)
	{
		Node.LinkIds.RemoveAll([NodeId](const int32 LinkedId)
			{
				return (LinkedId == NodeId);
			});
	}
}

void FDialogueTreeEditor::BreakLinksFromNode(const int32 NodeId, const bool CreateTransaction)
{
	const FScopedTransaction Transaction(LOCTEXT("BreakLinksFromNode", "Broke links going from node."), CreateTransaction);
	DialogueTree->Modify();

	const int32 Index = DialogueTree->GetNodeIndexById(NodeId);
	if (Index != -1)
	{
		DialogueTree->Nodes[Index].LinkIds.Empty();
	}
}

void FDialogueTreeEditor::UpdateNodeText(const int32 NodeId, const FText& NewText)
{
	const FScopedTransaction Transaction(LOCTEXT("UpdateNodeText", "Updated Node Text"), true);
	DialogueTree->DialogueTreeData->Modify();

	const int32 Index = DialogueTree->GetNodeIndexById(NodeId);
	if (Index != -1)
	{
		DialogueTree->DialogueTreeData->DialogueDatas[Index].Text = NewText;
	}
}

void FDialogueTreeEditor::UpdateNodeCoordinates(const int32 NodeId, const FVector2D& NewCoordinates)
{
	const FScopedTransaction Transaction(LOCTEXT("UpdateNodeCoordinates", "Updated Node Coordinates"), true);
	DialogueTree->Modify();
	
	const int32 Index = DialogueTree->GetNodeIndexById(NodeId);
	if (Index != -1)
	{
		DialogueTree->Nodes[Index].Coordinates = NewCoordinates;
	}
}

void FDialogueTreeEditor::UpdateNodeIsResponse(const int32 NodeId, const bool NewIsResponse)
{
	const FScopedTransaction Transaction(LOCTEXT("UpdateNodeIsResponse", "Updated Node IsResponse"), true);
	DialogueTree->Modify();

	const int32 Index = DialogueTree->GetNodeIndexById(NodeId);
	if (Index != -1)
	{
		DialogueTree->Nodes[Index].IsPlayerResponse = NewIsResponse;
	}
}

TSharedRef<SDockTab> FDialogueTreeEditor::SpawnTab_Viewport(const FSpawnTabArgs& Args)
{
	TSharedPtr<FDialogueTreeEditor> DialogueTreeEditor = SharedThis(this);

	return SNew(SDockTab)
		.Label(LOCTEXT("ViewportTab_Title", "Viewport"))
		[
			SAssignNew(DialogueViewportWidget, SUnboundDialogueViewportWidget, DialogueTreeEditor)
			.DialogueTree(DialogueTree)
		]
	;
}

TSharedRef<SDockTab> FDialogueTreeEditor::SpawnTab_Details(const FSpawnTabArgs& Args)
{
	TSharedPtr<FDialogueTreeEditor> DialogueTreeEditor = SharedThis(this);

	// Spawn the tab
	return SNew(SDockTab)
		.Label(LOCTEXT("DetailsTab_Title", "Details"))
		[
			SNew(SDialogueTreePropertiesTabBody, DialogueTreeEditor)
		];
}

void FDialogueTreeEditor::BindCommands()
{
	// No need to regenerate the commands.
	if (!DialogueEditorCommands.IsValid())
	{
		DialogueEditorCommands = MakeShareable(new FUICommandList);
		{

			DialogueEditorCommands->MapAction(FGenericCommands::Get().Duplicate,
				FExecuteAction::CreateRaw(this, &FDialogueTreeEditor::OnDuplicateSelected),
				FCanExecuteAction()
			);

			DialogueEditorCommands->MapAction(FGenericCommands::Get().Copy,
				FExecuteAction::CreateRaw(this, &FDialogueTreeEditor::OnCopySelected),
				FCanExecuteAction()
			);

			DialogueEditorCommands->MapAction(FGenericCommands::Get().Cut,
				FExecuteAction::CreateRaw(this, &FDialogueTreeEditor::OnCutSelected),
				FCanExecuteAction()
			);

			DialogueEditorCommands->MapAction(FGenericCommands::Get().Paste,
				FExecuteAction::CreateRaw(this, &FDialogueTreeEditor::OnPasteNodes),
				FCanExecuteAction()
			);

			DialogueEditorCommands->MapAction(FGenericCommands::Get().Delete,
				FExecuteAction::CreateRaw(this, &FDialogueTreeEditor::OnDelete),
				FCanExecuteAction()
			);
		}
	}
}

void FDialogueTreeEditor::OnDuplicateSelected()
{
	DialogueViewportWidget->DuplicateSelected();
}

void FDialogueTreeEditor::OnCopySelected()
{
	DialogueViewportWidget->CopySelected();
}

void FDialogueTreeEditor::OnCutSelected()
{
	DialogueViewportWidget->CutSelected();
}

void FDialogueTreeEditor::OnPasteNodes()
{
	DialogueViewportWidget->PasteNodes();
}

void FDialogueTreeEditor::OnDelete()
{
	DialogueViewportWidget->DeleteSelected();
}

#undef LOCTEXT_NAMESPACE