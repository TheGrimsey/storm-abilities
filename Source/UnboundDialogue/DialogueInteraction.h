// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interaction.h"

#include "UnboundDialogueTypes.h"

#include "DialogueInteraction.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnCurrentNodeChanged);

/**
 * 
 */
UCLASS()
class UNBOUNDDIALOGUE_API UDialogueInteraction : public UInteraction
{
	GENERATED_BODY()
	
	/*
	*	Variables
	*/
public:
	//Called whenever the currentnode is replicated.
	UPROPERTY(BlueprintAssignable)
	FOnCurrentNodeChanged OnCurrentNodeChanged;

protected:
	//The Dialogue we are having
	UPROPERTY(VisibleAnywhere, Replicated, Category = "Dialogue")
	class UDialogueTree* DialogueTree;

	//The Index of the Dialouge Node we are currently at.
	UPROPERTY(VisibleAnywhere, ReplicatedUsing = OnRep_CurrentDialogueNode, Category = "Dialogue")
	int32 CurrentDialogueNodeIndex;

	/*
	*	Methods
	*/
public:
	/*
	*	Starts a new dialogue
	*	@Param InInteractionComponent InteractionComponent to start dialogue with.
	*	@Param InSpeaker Actor to speak to.
	*	@Param InDialogueTree DialogueTree to use in dialogue.
	*/
	UFUNCTION(BlueprintCallable)
	static UDialogueInteraction* StartDialogue(class UInteractionComponent* InInteractionComponent, class AActor* InSpeaker, class UDialogueTree* InDialogueTree);

	UDialogueInteraction();

	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const override;

	virtual void OnStartInteraction() override;

	//Returns the current DialogueNode
	UFUNCTION(BlueprintCallable)
	const FDialogueNode& GetCurrentNode() const;

	//Returns the current DialogueDataNode
	UFUNCTION(BlueprintCallable)
	const FDialogueDataNode& GetCurrentDataNode() const;

	//Returns the DialogueNode at the Index.
	UFUNCTION(BlueprintCallable)
	const FDialogueNode& GetNodeByIndex(const int32 Index) const;

	//Returns the DialogueDataNode at the Index.
	UFUNCTION(BlueprintCallable)
	const FDialogueDataNode& GetDataNodeByIndex(const int32 Index) const;

	//Returns true if we pass the conditions for the node at NodeIndex.
	UFUNCTION(BlueprintCallable)
	bool IsPassingNodeConditions(const int32 NodeIndex) const;

	/*
	*	Select a dialogue choice.
	*	@Param ChoiceIndex Index of the choice in the current dialogue node's LinkIndexes array.
	*/
	UFUNCTION(BlueprintCallable)
	void SelectDialogueChoice(int32 ChoiceIndex);

	/*
	*	Exits the conversation and ends the interaction.
	*/
	UFUNCTION(BlueprintCallable)
	void ExitConversation();
private:
	//Tries to go to the first node which conditions we pass linked to the "Start"-Node. If we don't pass conditions for any Node we exit the conversation.
	void GoToStartNode();

	//Sets our current node to input and executes events related to moving to the node.
	void GoToNodeByIndex(int32 Index);

	int32 FindFirstPassingLinkIndex(const FDialogueNode& Node);

	//RPC SelectDialogueChoice on server. Calls SelectDialogueChoice().
	UFUNCTION(Server, Reliable)
	void ServerSelectDialogueChoice(int32 ChoiceIndex);

	void InternalSelectDialogueChoice(int32 ChoiceIndex);

	UFUNCTION()
	void OnRep_CurrentDialogueNode();
};
