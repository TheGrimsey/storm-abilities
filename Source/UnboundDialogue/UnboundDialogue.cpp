// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "UnboundDialogue.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_GAME_MODULE( FDefaultGameModuleImpl, UnboundDialogue );

DEFINE_LOG_CATEGORY(LogUnboundDialogue);
