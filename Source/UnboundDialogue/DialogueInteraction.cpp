// Fill out your copyright notice in the Description page of Project Settings.


#include "DialogueInteraction.h"

#include "InteractionComponent.h"
#include "UnboundDialogue.h"

#include "DialogueTree.h"
#include "DialogueTreeData.h"
#include "DialogueCondition.h"
#include "DialogueEvent.h"

#include "Engine/ActorChannel.h"
#include "UnrealNetwork.h"

#include "UnboundPlayerController.h"

UDialogueInteraction* UDialogueInteraction::StartDialogue(UInteractionComponent* InInteractionComponent, AActor* InSpeaker, UDialogueTree* InDialogueTree)
{
	//Create new interaction with the interactioncomponent as outer.
	UDialogueInteraction* Interaction = NewObject<UDialogueInteraction>(InInteractionComponent);
	//Assign properties.
	Interaction->DialogueTree = InDialogueTree;

	//Init interaction.
	Interaction->Init(InInteractionComponent, InSpeaker);

	//Return interaction.
	return Interaction;
}

UDialogueInteraction::UDialogueInteraction()
{
	InteractionType = EInteractionType::Dialogue;
}

void UDialogueInteraction::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	
	DOREPLIFETIME(UDialogueInteraction, DialogueTree);
	DOREPLIFETIME_CONDITION_NOTIFY(UDialogueInteraction, CurrentDialogueNodeIndex, COND_None, REPNOTIFY_Always);
}

void UDialogueInteraction::OnStartInteraction()
{
	Super::OnStartInteraction();

	if (GetOwningActor()->HasAuthority())
	{
		GoToStartNode();
	}
}

const FDialogueNode& UDialogueInteraction::GetCurrentNode() const
{
	return DialogueTree->GetNodeByIndex(CurrentDialogueNodeIndex);
}

const FDialogueDataNode& UDialogueInteraction::GetCurrentDataNode() const
{
	return DialogueTree->DialogueTreeData->GetDataNodeByIndex(CurrentDialogueNodeIndex);
}

const FDialogueNode& UDialogueInteraction::GetNodeByIndex(const int32 Index) const
{
	return DialogueTree->GetNodeByIndex(Index);
}

const FDialogueDataNode& UDialogueInteraction::GetDataNodeByIndex(const int32 Index) const
{
	return DialogueTree->DialogueTreeData->GetDataNodeByIndex(Index);
}

bool UDialogueInteraction::IsPassingNodeConditions(const int32 NodeIndex) const
{
	//Make sure it is a valid node.
	if (DialogueTree->IsValidNodeIndex(NodeIndex))
	{
		//Grab node.
		const FDialogueNode& Node = GetNodeByIndex(NodeIndex);

		//Check all conditions. If we fail one then we aren't passing.
		for (const UDialogueCondition* Condition : Node.Conditions)
		{
			if (Condition->CheckCondition(Speaker, GetPlayerController()) == false)
			{
				return false;
			}
		}

		return true;
	}

	return false;
}

void UDialogueInteraction::SelectDialogueChoice(int32 ChoiceIndex)
{
	if (InteractionComponent->HasAuthority())
	{
		InternalSelectDialogueChoice(ChoiceIndex);
	}
	else
	{
		ServerSelectDialogueChoice(ChoiceIndex);
	}
}

void UDialogueInteraction::ExitConversation()
{
	if (InteractionComponent->GetInteraction() && InteractionComponent->GetInteraction() == this)
	{
		InteractionComponent->StopInteraction();
	}
}

void UDialogueInteraction::GoToStartNode()
{
	const FDialogueNode& StartNode = DialogueTree->GetStartNode();

	//Index of first node we pass all conditions for.
	int32 NodeIndex = -1;

	for (int32 StartNodeLink : StartNode.LinkIndexes)
	{
		if (IsPassingNodeConditions(StartNodeLink))
		{
			NodeIndex = StartNodeLink;

			break;
		}
	}

	if (NodeIndex != -1)
	{
		GoToNodeByIndex(NodeIndex);
	}
	else
	{
		ExitConversation();
	}
	
}

void UDialogueInteraction::GoToNodeByIndex(int32 Index)
{
	//Make sure the node is valid.
	if (DialogueTree->IsValidNodeIndex(Index))
	{
		//Grab node.
		const FDialogueNode& NewNode = GetNodeByIndex(Index);

		//Execute all events for node.
		for (UDialogueEvent* DialogueEvent : NewNode.Events)
		{
			DialogueEvent->ExecuteEvent(Speaker, GetPlayerController());
		}

		//Set currentdialoguenodeindex to the new one.
		CurrentDialogueNodeIndex = Index;
		//Notify that we have changed node.
		OnCurrentNodeChanged.Broadcast();
	}
	else
	{
		UE_LOG(LogUnboundDialogue, Warning, TEXT("Tried to go to node with invalid Index %d"), Index);
		ExitConversation();
	}
}

int32 UDialogueInteraction::FindFirstPassingLinkIndex(const FDialogueNode& Node)
{
	for (int32 LinkIndex : Node.LinkIndexes)
	{
		if (IsPassingNodeConditions(LinkIndex))
		{
			return LinkIndex;
		}
	}

	return -1;
}

void UDialogueInteraction::ServerSelectDialogueChoice_Implementation(int32 ChoiceIndex)
{
	SelectDialogueChoice(ChoiceIndex);
}

void UDialogueInteraction::InternalSelectDialogueChoice(int32 ChoiceIndex)
{
	//Make sure we have a valid index else let's exit the conversation. (The reason for this is partially to allow "Goodbye!" choices to work without much custom work)
	if (GetCurrentNode().LinkIndexes.IsValidIndex(ChoiceIndex))
	{
		int32 ChosenNodeIndex = GetCurrentNode().LinkIndexes[ChoiceIndex];

		if (IsPassingNodeConditions(ChosenNodeIndex))
		{
			int32 NewNodeIndex = FindFirstPassingLinkIndex(GetNodeByIndex(ChosenNodeIndex));

			GoToNodeByIndex(NewNodeIndex);
		}
	}
	else
	{
		ExitConversation();
	}
}

void UDialogueInteraction::OnRep_CurrentDialogueNode()
{
	//TODO Handle Client events for new node.

	OnCurrentNodeChanged.Broadcast();
}
