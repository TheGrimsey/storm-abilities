// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UnboundDialogueTypes.generated.h"

USTRUCT(BlueprintType)
struct FDialogueNode
{
	GENERATED_BODY()

	//The Id of this node.
	UPROPERTY()
	int32 Id = -1;

	//The Indexes of the Nodes we are linked to. Converted down from LinkIds when saving.
	UPROPERTY(BlueprintReadOnly, Category = "Dialogue Node")
	TArray<int32> LinkIndexes;

	//Conditions required to pass to choose the response (Player Node) / get the response (NPC Response)
	UPROPERTY(EditAnywhere, Instanced, BlueprintReadOnly, Category = "Dialogue Node")
	TArray<class UDialogueCondition*> Conditions;

	//Events executed when a node is entered / response is chosen. 
	UPROPERTY(EditAnywhere, Instanced, BlueprintReadOnly, Category = "Dialogue Node")
	TArray<class UDialogueEvent*> Events;

#if WITH_EDITORONLY_DATA
	//The Ids of the Nodes we are linked to. These are used in the editor to determine which nodes we are connected to.
	UPROPERTY()
	TArray<int32> LinkIds;

	//Whether or not this is a player's response to what an NPC said. Just used in the editor to hide some right click stuff. Not actually used for logic and may be removed in the future.
	UPROPERTY(EditAnywhere, Category = "Dialogue Node", NoClear)
	bool IsPlayerResponse;

	//Position on the editor viewport.
	UPROPERTY()
	FVector2D Coordinates;
#endif
};

USTRUCT(BlueprintType)
struct FDialogueDataNode
{
	GENERATED_BODY()

	UPROPERTY()
	int32 Id = -1;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Dialogue Node")
	FText Text;

	//TODO Sounds

	//TODO Client Events (Visual Only events)
};