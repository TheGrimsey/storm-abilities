// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Engine/DataAsset.h"
#include "UnboundDialogueTypes.h"
#include "DialogueTreeData.generated.h"

/**
 * 
 */
UCLASS()
class UNBOUNDDIALOGUE_API UDialogueTreeData : public UDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY()
	TArray<FDialogueDataNode> DialogueDatas;

	const FDialogueDataNode& GetDataNodeByIndex(int32 Index) const
	{
		return DialogueDatas[Index];
	}

	//Don't load DialogueTreeData on servers.
	virtual bool NeedsLoadForServer() const { return false; }

};
