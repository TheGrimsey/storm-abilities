// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "DialogueEvent.generated.h"

/**
 * A Dialogue event is executed when a player chooses a response and we enter a node. These are only run on the server.
 */
UCLASS(BlueprintType, EditInlineNew)
class UNBOUNDDIALOGUE_API UDialogueEvent : public UObject
{
	GENERATED_BODY()

public:
	/*
	*	Call to Execute the event.
	*	@Param Speaker The actor that the player is speaking to.
	*	@Param Player The player in the dialogue.
	*/
	virtual void ExecuteEvent(class AActor* Speaker, class AUnboundPlayerController* Player) { }
};
