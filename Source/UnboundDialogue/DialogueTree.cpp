// Fill out your copyright notice in the Description page of Project Settings.


#include "DialogueTree.h"
#include "DialogueTreeData.h"

#if WITH_EDITOR
#include "UnrealEd/Public/FileHelpers.h"

#include "AssetTools/Public/AssetToolsModule.h"
#include "AssetTools/Private/AssetRenameManager.h"
#endif


const FDialogueNode& UDialogueTree::GetStartNode() const
{
	return GetNodeByIndex(0);
}

#if WITH_EDITORONLY_DATA
int32 UDialogueTree::GetNodeIndexById(int32 Id)
{
	for (int i = 0; i < Nodes.Num(); i++)
	{
		if (Nodes[i].Id == Id) return i;
	}

	return -1;
}

void UDialogueTree::PreSave(const ITargetPlatform* TargetPlatform)
{
	//Save all Link Indexes from the Id.
	TMap<int32, int32> IdToIndex;
	IdToIndex.Reserve(Nodes.Num());

	//Get all Ids and their corresponding index.
	for (int i = 0; i < Nodes.Num(); i++)
	{
		IdToIndex.Add(Nodes[i].Id, i);
	}

	//Save Indexes for Nodes we refer to by Id.
	for (int i = 0; i < Nodes.Num(); i++)
	{
		//First empty the existing LinkIndexes array and rezise it to the new size (if needed).
		Nodes[i].LinkIndexes.Empty(Nodes[i].LinkIds.Num());

		//Go through all LinkIds.
		for (int LinkI = 0; LinkI < Nodes[i].LinkIds.Num(); LinkI++)
		{
			//Find link index from TMap.
			int LinkIndex = *IdToIndex.Find(Nodes[i].LinkIds[LinkI]);

			//Add index to Link list.
			Nodes[i].LinkIndexes.Add(LinkIndex);
		}
	}

	//Save DialogueTreeData when this is saved.
	TArray<UPackage*> PackageToSave { DialogueTreeData->GetOutermost() };

	const bool bOnlySaveDirty = true;
	bool Saved = UEditorLoadingAndSavingUtils::SavePackages(PackageToSave, bOnlySaveDirty);

	Super::PreSave(TargetPlatform);
}

void UDialogueTree::PostRename(UObject* OldOuter, const FName OldName)
{
	Super::PostRename(OldOuter, OldName);

	/*
	*	Rename Data assets to make sure the names always match.
	*/

	FSoftObjectPath ThisPath = FSoftObjectPath(this);
	FSoftObjectPath NewPath = ThisPath.GetLongPackageName() + TEXT("Data") + TEXT(".") + ThisPath.GetAssetName() + TEXT("Data");
	FAssetRenameData RenameData = FAssetRenameData(DialogueTreeData->GetPathName(), NewPath);

	TArray<FAssetRenameData> RenameDataArray;
	RenameDataArray.Add(RenameData);

	FAssetToolsModule& AssetToolsModule = FModuleManager::LoadModuleChecked<FAssetToolsModule>("AssetTools");
	AssetToolsModule.Get().RenameAssets(RenameDataArray);
}

#endif