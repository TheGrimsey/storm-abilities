// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class UnboundDialogue : ModuleRules
{
	public UnboundDialogue(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine"});

        PublicDependencyModuleNames.AddRange(new string[] { "Unbound", "UnboundInteraction" });

        if (Target.bBuildEditor)
        {
            PrivateDependencyModuleNames.Add("AssetTools");
            PrivateDependencyModuleNames.Add("UnrealEd");
        }
	}
}
