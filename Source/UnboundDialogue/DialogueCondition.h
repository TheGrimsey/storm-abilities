// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "DialogueCondition.generated.h"

/**
 * A Dialogue Condition determines if a Player can choose a response or an NPC will choose a node when responding to the player.
 */
UCLASS(BlueprintType, EditInlineNew)
class UNBOUNDDIALOGUE_API UDialogueCondition : public UObject
{
	GENERATED_BODY()

public:
	/*
	*	Returns true if condition passes.
	*	@Param Speaker The actor that the player is speaking to.
	*	@Param Player The player in the dialogue.
	*/
	virtual bool CheckCondition(class AActor* Speaker, class AUnboundPlayerController* Player)  const { return true; }
};
