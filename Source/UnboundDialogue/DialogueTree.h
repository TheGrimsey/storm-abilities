// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Engine/DataAsset.h"
#include "UnboundDialogueTypes.h"
#include "DialogueTree.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class UNBOUNDDIALOGUE_API UDialogueTree : public UDataAsset
{
	GENERATED_BODY()

	/*
	*	Variables
	*/
public:
#if WITH_EDITORONLY_DATA
	//Currently selected node in editor. Here because it is used in the details widget which only knows about this object.
	UPROPERTY(Transient)
	int32 SelectedNodeId = 0;

	UPROPERTY(EditAnywhere, NoClear)
	bool DisplayIdleSplines = true;

	//Used to determine what id newly created nodes should have. Only needed for Editor.
	UPROPERTY()
	int32 NextNodeId = 0;
#endif

	UPROPERTY(EditAnywhere)
	TArray<FDialogueNode> Nodes;

	//Holds all the client relevant data (such as text, sounds, client only events, etc).
	UPROPERTY(VisibleAnywhere)
	class UDialogueTreeData* DialogueTreeData;

	/*
	*	Methods
	*/
public:
	bool IsValidNodeIndex(int32 Index) const
	{
		return Nodes.IsValidIndex(Index);
	}

	const FDialogueNode& GetStartNode() const;

	const FDialogueNode& GetNodeByIndex(int32 Index) const
	{
		return Nodes[Index];
	}

#if WITH_EDITORONLY_DATA
	FDialogueNode& GetNodeByIndex_Mutable(int32 Index)
	{
		return Nodes[Index];
	}

	int32 GetNodeIndexById(int32 Id);
	
	virtual void PreSave(const class ITargetPlatform* TargetPlatform) override;

	virtual void PostRename(UObject* OldOuter, const FName OldName) override;
#endif

	virtual bool IsNameStableForNetworking() const override { return true; }
};
